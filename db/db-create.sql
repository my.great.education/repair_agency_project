DROP database IF EXISTS `repair_agency_db`;
CREATE database IF NOT EXISTS `repair_agency_db` DEFAULT CHARACTER SET utf8 ;
USE `repair_agency_db` ;

DROP TABLE IF EXISTS `repair_agency_db`.`role` ;
DROP TABLE IF EXISTS `repair_agency_db`.`account_status` ;
DROP TABLE IF EXISTS `repair_agency_db`.`account` ;
DROP TABLE IF EXISTS `repair_agency_db`.`request_status` ;
DROP TABLE IF EXISTS `repair_agency_db`.`category` ;
DROP TABLE IF EXISTS `repair_agency_db`.`service` ;
DROP TABLE IF EXISTS `repair_agency_db`.`request` ;
DROP TABLE IF EXISTS `repair_agency_db`.`request_has_service` ;
DROP TABLE IF EXISTS `repair_agency_db`.`status_flow` ;
DROP TABLE IF EXISTS `repair_agency_db`.`account_balance_history` ;
DROP TABLE IF EXISTS `repair_agency_db`.`request_executor` ;

CREATE TABLE IF NOT EXISTS `repair_agency_db`.`role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(16) NOT NULL,
  `access_level` INT UNSIGNED NULL DEFAULT 10,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `repair_agency_db`.`account_status` (
	`id` INT NOT NULL AUTO_INCREMENT,
    `status` VARCHAR(16) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `status_UNIQUE` (`status` ASC) VISIBLE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `repair_agency_db`.`account` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(16) NOT NULL UNIQUE,
  `pass` VARCHAR(32) NOT NULL,
  `balance` DECIMAL(10,2) UNSIGNED NULL DEFAULT 0,
  `firstname` VARCHAR(32) NOT NULL,
  `lastname` VARCHAR(32) NOT NULL,
  `email` VARCHAR(32) NOT NULL,
  `phone_number` VARCHAR(9) NOT NULL UNIQUE,
  `status_id` INT NOT NULL,
  `role_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE,
  UNIQUE INDEX `phone_number_UNIQUE` (`phone_number` ASC) VISIBLE,
  CONSTRAINT `fk_account_role`
    FOREIGN KEY (`role_id`)
    REFERENCES `repair_agency_db`.`role` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_account_account_status`
    FOREIGN KEY (`status_id`)
    REFERENCES `repair_agency_db`.`account_status` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `repair_agency_db`.`request_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `condition` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `condition_UNIQUE` (`condition` ASC) VISIBLE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `repair_agency_db`.`category` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `parent_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_category_category1_idx` (`parent_id` ASC) VISIBLE,
  CONSTRAINT `fk_category_category1`
    FOREIGN KEY (`parent_id`)
    REFERENCES `repair_agency_db`.`category` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);


CREATE TABLE IF NOT EXISTS `repair_agency_db`.`service` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(64) NOT NULL,
  `price` DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT 1,
  `category_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_service_category_idx` (`category_id` ASC) VISIBLE,
  CONSTRAINT `fk_service_category`
    FOREIGN KEY (`category_id`)
    REFERENCES `repair_agency_db`.`category` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `repair_agency_db`.`request` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `price` DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT 1,
  `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `account_id` INT NOT NULL,
  `status_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_request_account_idx` (`account_id` ASC) VISIBLE,
  INDEX `fk_request_status_status_idx` (`status_id` ASC) VISIBLE,
  CONSTRAINT `fk_request_account`
    FOREIGN KEY (`account_id`)
    REFERENCES `repair_agency_db`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_request_status_status`
    FOREIGN KEY (`status_id`)
    REFERENCES `repair_agency_db`.`request_status` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `repair_agency_db`.`request_has_service` (
  `request_id` INT NOT NULL,
  `service_id` INT NOT NULL,
  `amount` INT UNSIGNED NOT NULL,
  `price` DECIMAL(10,2) UNSIGNED NOT NULL,
  `description` VARCHAR(300) NULL,
  `edit_time` TIMESTAMP NULL,
  PRIMARY KEY (`request_id`, `service_id`),
  INDEX `fk_request_has_service_service1_idx` (`service_id` ASC) VISIBLE,
  INDEX `fk_request_has_service_request1_idx` (`request_id` ASC) VISIBLE,
  CONSTRAINT `fk_request_has_service_request1`
    FOREIGN KEY (`request_id`)
    REFERENCES `repair_agency_db`.`request` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_request_has_service_service1`
    FOREIGN KEY (`service_id`)
    REFERENCES `repair_agency_db`.`service` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `repair_agency_db`.`status_flow` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `from` INT NOT NULL,
  `to` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_status_flow_request_status1_idx` (`from` ASC) VISIBLE,
  INDEX `fk_status_flow_request_status2_idx` (`to` ASC) VISIBLE,
  CONSTRAINT `fk_status_flow_request_status1`
    FOREIGN KEY (`from`)
    REFERENCES `repair_agency_db`.`request_status` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_status_flow_request_status2`
    FOREIGN KEY (`to`)
    REFERENCES `repair_agency_db`.`request_status` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `repair_agency_db`.`account_balance_history` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `account_id` INT NOT NULL,
  `payment` DECIMAL NOT NULL,
  `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `account_id`),
  CONSTRAINT `fk_account_balance_history_account1`
    FOREIGN KEY (`account_id`)
    REFERENCES `repair_agency_db`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `repair_agency_db`.`request_executor` (
  `request_id` INT NOT NULL,
  `account_id` INT NOT NULL,
  `review` VARCHAR(128) NULL,
  `review_time` TIMESTAMP NULL,
  `grade` TINYINT(1) NULL,
  PRIMARY KEY (`request_id`),
  INDEX `fk_executor_account1_idx` (`account_id` ASC) VISIBLE,
  CONSTRAINT `fk_executor_request1`
    FOREIGN KEY (`request_id`)
    REFERENCES `repair_agency_db`.`request` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_executor_account1`
    FOREIGN KEY (`account_id`)
    REFERENCES `repair_agency_db`.`account` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;

DROP VIEW IF EXISTS `category_for_request`;
CREATE VIEW `category_for_request` AS
SELECT `category`.`name` AS `category_name`,
	   `service`.`id` AS `service_id`,
	   `request_has_service`.`request_id`
FROM `category` 
INNER JOIN `service` ON `category`.`id` = `service`.`category_id`
INNER JOIN `request_has_service` ON `request_has_service`.`service_id` = `service`.`id`;

DROP VIEW IF EXISTS `master_for_request_transit`;
CREATE VIEW `master_for_request_transit` AS
SELECT `account`.`firstname` AS `master_name`,
	   `account`.`lastname` AS `master_lastname`,
       `account`.`id` AS `master_id`,
       `account`.`email` AS `master_email`,
       `account`.`phone_number` AS `master_phone`,
       `request_executor`.`review` AS `review`,
       `request_executor`.`review_time` AS `review_time`,
	   `request_executor`.`request_id`,
       `request_executor`.`grade`,
	   `request`.`account_id` AS `account_id`
FROM `account` 
INNER JOIN `request_executor` ON `request_executor`.`account_id` = `account`.`id`
INNER JOIN `request` ON `request`.`id` = `request_executor`.`request_id`;

DROP VIEW IF EXISTS `master_has_feedback`;
CREATE VIEW `master_has_feedback` AS
SELECT 
`master_for_request_transit`.`review`, 
`master_for_request_transit`.`grade`,
`master_for_request_transit`.`review_time`, 
`account`.`firstname` AS `user_name`,
`master_for_request_transit`.`master_id`
FROM `master_for_request_transit`
INNER JOIN `account` ON `master_for_request_transit`.`account_id` = `account`.`id`
WHERE `master_for_request_transit`.`review` IS NOT NULL;

DROP VIEW IF EXISTS `order_has_user`;
CREATE VIEW `order_has_user` AS
SELECT 
`account`.`id` as `user_id`, 
`account`.`lastname` as `user_name`, 
`account`.`balance` as `user_balance`, 
`account`.`phone_number` as `user_phone`,
`request`.`id` as `request_id`,
`request`.`price`,
`request`.`create_time` as `order_date`,
`request_status`.`condition` as `status`,
`request_status`.`id` as `status_id`,
`request_has_service`.`amount`,
`request_has_service`.`description` as `order_body`,
`category_for_request`.`category_name` as `service`,
`request_executor`.`account_id` as `master_id`
FROM `request`
INNER JOIN `account` ON `account`.`id` = `request`.`account_id`
INNER JOIN `request_has_service` ON `request_has_service`.`request_id` = `request`.`id`
INNER JOIN `category_for_request` ON `category_for_request`.`request_id` = `request`.`id`
INNER JOIN `service` ON `request_has_service`.`service_id` = `service`.`id`
INNER JOIN `request_status` ON `request_status`.`id` = `request`.`status_id`
LEFT JOIN `request_executor` ON `request_executor`.`request_id` = `request`.`id`
WHERE `request`.`account_id` = `account`.`id`;