USE `repair_agency_db`;

INSERT INTO `role` (`id`, `name`, `access_level`)
	VALUES 
		(1, "admin", 0),
		(2, "manager", 5),
		(3, "craftsman", 8),
        (4, "client", 9),
		(5, "guest", DEFAULT); 

INSERT INTO `request_status` (`id`, `condition`)
	VALUES 
		(1, "awaiting processing"),
		(2, "pending payment"),
        (3, "paid"), 
		(4, "in progress"),
		(5, "completed"), 
		(6, "canceled"); 

SET @id_1 = (SELECT `id` FROM `request_status` WHERE `condition` = "awaiting processing");
SET @id_2 = (SELECT `id` FROM `request_status` WHERE `condition` = "pending payment");
SET @id_3 = (SELECT `id` FROM `request_status` WHERE `condition` = "paid");
SET @id_4 = (SELECT `id` FROM `request_status` WHERE `condition` = "in progress");
SET @id_5 = (SELECT `id` FROM `request_status` WHERE `condition` = "completed");
SET @id_6 = (SELECT `id` FROM `request_status` WHERE `condition` = "canceled");
INSERT INTO `status_flow` (`id`, `from`, `to`)
	VALUES
		(1, @id_1, @id_2),
        (DEFAULT, @id_2, @id_3),
        (DEFAULT, @id_3, @id_4),
        (DEFAULT, @id_4, @id_5);
        
INSERT INTO `account_status` (`id`, `status`)
	VALUES
		(1, "ACTIVE"),
        (2, "BLOCKED"),
        (3, "DELETED");

SET @fk_status_id = (SELECT `id` FROM `account_status` WHERE `status` = "ACTIVE");
SET @fk_client_role_id = (SELECT `id` FROM `role` WHERE `name` = "client");
SET @fk_manager_role_id = (SELECT `id` FROM `role` WHERE `name` = "manager");
SET @fk_crfatsman_role_id = (SELECT `id` FROM `role` WHERE `name` = "craftsman");
SET @fk_admin_role_id = (SELECT `id` FROM `role` WHERE `name` = "admin");
INSERT INTO `account` (`id`, `login`, `pass`, `firstname`, `lastname`, `email`, `phone_number`,`balance`, `status_id`, `role_id`) 
	VALUES
		(DEFAULT, "admin", "admin", "adminFirstName", "adminLastName", "johndoe420pm@gmail.com", "981234567", DEFAULT, @fk_status_id, @fk_admin_role_id),
		(DEFAULT, "user1", "user1", "user1FirstName", "user1LastName", "johndoe420pm@gmail.com", "981234568", DEFAULT, @fk_status_id, @fk_client_role_id),
        (DEFAULT, "user2", "user2", "user2FirstName", "user2LastName", "johndoe420pm@gmail.com", "981234569", DEFAULT, @fk_status_id, @fk_client_role_id),
        (DEFAULT, "manager1", "manager1", "manager1FirstName", "manager1LastName", "johndoe420pm@gmail.com", "982234560", DEFAULT, @fk_status_id, @fk_manager_role_id),
        (DEFAULT, "manager2", "manager2", "manager2FirstName", "manager2LastName", "johndoe420pm@gmail.com", "982234561", DEFAULT, @fk_status_id, @fk_manager_role_id),
        (DEFAULT, "craftsman1", "craftsman1", "Anton", "Antonov", "johndoe420pm@gmail.com", "982234562", DEFAULT, @fk_status_id, @fk_crfatsman_role_id),
        (DEFAULT, "craftsman2", "craftsman2", "Sergey", "Sergeev", "johndoe420pm@gmail.com", "982234563", DEFAULT, @fk_status_id, @fk_crfatsman_role_id);
        
   
INSERT INTO `category` (`id`, `name`, `parent_id`) VALUES (1, "Custom", NULL);   
INSERT INTO `category` (`id`, `name`, `parent_id`) VALUES (2, "Repair of washing machines", NULL);
INSERT INTO `category` (`id`, `name`, `parent_id`) VALUES (3, "Refrigerator repair", NULL);
INSERT INTO `category` (`id`, `name`, `parent_id`) VALUES (4, "Gas stove repair", NULL);
INSERT INTO `category` (`id`, `name`, `parent_id`) VALUES (5, "Repair of electric stoves", NULL);

INSERT INTO `service` (`id`, `name`, `price`, `category_id`)
	VALUES
		(DEFAULT, "CUSTOM", DEFAULT, (SELECT `id` FROM `category` WHERE `name` = "Custom")),
		(DEFAULT, "Replacing the heating element", 260, (SELECT `id` FROM `category` WHERE `name` = "Repair of washing machines")),
        (DEFAULT, "Repair of the board", 450, (SELECT `id` FROM `category` WHERE `name` = "Repair of washing machines")),
        (DEFAULT, "Installing a new drum", 800, (SELECT `id` FROM `category` WHERE `name` = "Repair of washing machines")),
        (DEFAULT, "Replacing the bearing block", 320, (SELECT `id` FROM `category` WHERE `name` = "Repair of washing machines")),
		(DEFAULT, "Замена компрессора", 950, (SELECT `id` FROM `category` WHERE `name` = "Refrigerator repair")),
		(DEFAULT, "Заправка фреоном", 300, (SELECT `id` FROM `category` WHERE `name` = "Refrigerator repair")),
        (DEFAULT, "Замена пускового реле", 180, (SELECT `id` FROM `category` WHERE `name` = "Refrigerator repair")),
        (DEFAULT, "Замена терморегулятора", 220, (SELECT `id` FROM `category` WHERE `name` = "Refrigerator repair")),
        (DEFAULT, "Установка газовой плиты", 270, (SELECT `id` FROM `category` WHERE `name` = "Gas stove repair")),
        (DEFAULT, "Замена жиклеров", 160, (SELECT `id` FROM `category` WHERE `name` = "Gas stove repair")),
        (DEFAULT, "Замена шланга", 170, (SELECT `id` FROM `category` WHERE `name` = "Gas stove repair")),
        (DEFAULT, "Замена термопары", 350, (SELECT `id` FROM `category` WHERE `name` = "Gas stove repair")),
        (DEFAULT, "Установка электроплиты", 240, (SELECT `id` FROM `category` WHERE `name` = "Repair of electric stoves")),
        (DEFAULT, "Замена конфорок", 215, (SELECT `id` FROM `category` WHERE `name` = "Repair of electric stoves")),
        (DEFAULT, "Замена ТЭНа", 280, (SELECT `id` FROM `category` WHERE `name` = "Repair of electric stoves")),
        (DEFAULT, "Замена переключателя режимов конфорки", 255, (SELECT `id` FROM `category` WHERE `name` = "Repair of electric stoves"));