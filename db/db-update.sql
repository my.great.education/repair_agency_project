USE `repair_agency_db`;

INSERT INTO `account_balance_history` (`account_id`, `payment`, `time`) 
	VALUES 
		((SELECT `id` FROM `account` WHERE `login` = "user1"), 10000, DEFAULT); 

SET @id = (SELECT `id` FROM `account` WHERE `login` = "user1");
SET @payment = (SELECT `payment` FROM `account_balance_history` WHERE `account_id` = @id);
UPDATE `account` 
SET balance = balance + @payment
WHERE `login` = "user1";

UPDATE request_executor SET review = "Все хорошо сделали. Спасибо", review_time = current_timestamp(), grade = TRUE WHERE `request_id` = 6;
update request_executor SET review = "В Бахчисараї фельд'єґер зумів одягнути ящірці жовтий капюшон!", review_time = current_timestamp(), grade = FALSE WHERE request_id = 10;
update request_executor SET review = "Грішний джиґіт, що хотів у Францію, позбувався цієї думки з'їдаючи трюфель", review_time = current_timestamp(), grade = TRUE WHERE request_id = 1;
update request_executor SET review = "Щастям б'єш жук їх глицю в фон й ґедзь пріч", review_time = current_timestamp(), grade = TRUE WHERE request_id = 7;
update request_executor SET review = "Юнкерський джинґл, що при безхліб'ї чує фашист, це ловця гімн", review_time = current_timestamp(), grade = FALSE WHERE request_id = 5;