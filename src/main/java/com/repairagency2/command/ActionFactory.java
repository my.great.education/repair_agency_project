package com.repairagency2.command;

import javax.servlet.http.HttpServletRequest;

import com.repairagency2.command.ipml.EmptyCommand;
import com.repairagency2.command.manager.MessageManager;

public class ActionFactory {
    private CommandEnum currentEnum;
    
    public ActionCommand defineCommand(HttpServletRequest request) {
	ActionCommand current = new EmptyCommand();
	String action = request.getParameter("command").toUpperCase();
	if (action == null || action.isEmpty()) {
	    return current;
	}
	try {
	    currentEnum = CommandEnum.valueOf(action);
	    current = currentEnum.getCurrentCommand();
	} catch (IllegalArgumentException e) {
	    //log
	    request.setAttribute("error", action + MessageManager.getProperty("message.error.wrongAction"));
	}
	return current;
    }
    
    public CommandEnum getAction() {
	return currentEnum;
    }
}
