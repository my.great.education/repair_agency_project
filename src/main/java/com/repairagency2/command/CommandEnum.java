package com.repairagency2.command;

import com.repairagency2.command.ipml.*;
import com.repairagency2.command.ipml.manager.*;
import com.repairagency2.command.ipml.user.*;

public enum CommandEnum {
    LOGIN {
	{
	    this.command = new LoginCommand();
	}
    },
    REGISTER {
	{
	    this.command = new RegisterCommand();
	}
    },
    TUBALANCE {
	{
	    this.command = new TopUpBalanceCommand();
	}
    },
    CHANGE_PROFILE {
	{
	    this.command = new ChangeProfileCommand();
	}
    },
    CHANGE_PASSWORD {
	{
	    this.command = new ChangePasswordCommand();
	}
    },
    PAYMENT_HISTORY {
	{
	    this.command = new PaymentHistoryCommand();
	}
    },
    CREATE_ORDER_CUSTOM {
	{
	    this.command = new CreateOrderCustomCommand();
	}
    },
    UPDATE_ORDER_STATUS {
	{
	    this.command = new UpdateOrderStatusCommand();
	}
    },
    UPDATE_ORDER_CUSTOM_TEXT {
	{
	    this.command = new UpdateCustomOrderTextCommand();
	}
    },
    ORDER_HISTORY {
	{
	    this.command = new OrderHistoryCommand();
	}
    },
    ABOUT_MASTER {
	{
	    this.command = new FeedbackCommand();
	}
    },
    PAY_ORDER {
	{
	    this.command = new PayOrderCommand();
	}
    },
    PUBLISH_FEEDBACK {
	{
	    this.command = new PublishFeedbackCommand();
	}
    },
    ORDER_FULL {
	{
	    this.command = new OrderFullCommand();
	}
    },
    TUBALANCE_MANAGER {
	{
	    this.command = new TopUpBalanceManagerCommand();
	}
    },
    CHANGE_PRICE {
	{
	    this.command = new ChangePriceCommand();
	}
    },
    CHANGE_MASTER {
	{
	    this.command = new ChangeMasterCommand();
	}
    },
    UPDATE_STATUS {
	{
	    this.command = new UpdateStatusCommand();
	}
    };

    ActionCommand command;

    public ActionCommand getCurrentCommand() {
	return command;
    }
}
