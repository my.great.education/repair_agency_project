package com.repairagency2.command.ipml;

import javax.servlet.http.HttpServletRequest;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;

public class EmptyCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
	String page = ConfManager.getProperty("path.page.index");
	return page;
    }
}
