package com.repairagency2.command.ipml;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.AccountService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.field.RequestParamsUtil.LoginParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class LoginCommand implements ActionCommand {
    private String lpPattern = "^[a-zA-Z0-9]{5,8}$";
    private AccountService accountService;

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	Account currentUser = null;
	String login = request.getParameter(LoginParams.LOGIN);
	String password = request.getParameter(LoginParams.PASSWORD);

	if (checkCredentials(login, password)) {
	    accountService = new AccountService();
	    try {
		// log
		currentUser = accountService.find(login, password);
	    } catch (DAOException e) {
		// log
		e.printStackTrace();
		return page;
	    } catch (DBException e) {		
		// log
		e.printStackTrace();
		return page;
	    }
	}
	
	if (currentUser == null) {	   
	    request.setAttribute("error", MessageManager.getProperty("message.error.login"));
	    page = ConfManager.getProperty("path.notice");
	    // log
	    return page;
	}

	if (accountService.isValidUserPassword(currentUser, password)) {
	    HttpSession session = request.getSession();
	    int roleId = currentUser.getRoleId();
	    int statusId = currentUser.getStatusId();
	    
	    switch (statusId) {
	    case 2:
		request.setAttribute("error", MessageManager.getProperty("message.error.login.status.blocked"));
		page = ConfManager.getProperty("path.notice");
		return page;
	    case 3:
		request.setAttribute("error", MessageManager.getProperty("message.error.login.status.deleted"));
		page = ConfManager.getProperty("path.notice");
		return page;
	    default:
		break;
	    }
	    
	    session.setAttribute("user", currentUser);
	    session.setAttribute("user_id", currentUser.getId());
	    session.setAttribute("role_id", currentUser.getRoleId());

	    // log
	    switch (roleId) {
	    case 2:
		page = ConfManager.getProperty("path.controller.manager.index");
		break;
	    case 4:
		page = ConfManager.getProperty("path.page.index");
		break;
	    default:
		break;
	    }
	} else {
	    request.setAttribute("error", MessageManager.getProperty("message.error.login"));
	    page = ConfManager.getProperty("path.notice");
	}
	// log
	return page;
    }

    private boolean checkCredentials(String login, String password) {
	if (login == null || login.isEmpty() || !login.matches(lpPattern)) {
	    // log
	    return false;
	}
	if (password == null || password.isEmpty() || !password.matches(lpPattern)) {
	    // log
	    return false;
	}
	return true;
    }
}
