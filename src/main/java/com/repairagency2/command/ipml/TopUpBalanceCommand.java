package com.repairagency2.command.ipml;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.AccountService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.field.RequestParamsUtil.ToUpBalanceParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class TopUpBalanceCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}
	
	Account user = (Account) session.getAttribute("user");	
	String payment = request.getParameter(ToUpBalanceParams.TO_UP_BALANCE);
	
	if (payment.isBlank() || payment == null) {
	    request.setAttribute("error", MessageManager.getProperty("message.error.topUp.value"));
	    page = ConfManager.getProperty("path.notice");
	    //log
	    return page;
	}
	
	BigDecimal paymentBigDecimal = new BigDecimal(payment).setScale(2, RoundingMode.HALF_DOWN);
	
	if ((paymentBigDecimal.signum() == 0) || (paymentBigDecimal.signum() == -1)) {
	    request.setAttribute("error", MessageManager.getProperty("message.error.topUp.value"));
	    page = ConfManager.getProperty("path.notice");
	    //log
	    return page;
	}
	
	try {
	    AccountService.updateBalanceAndHistory(user, paymentBigDecimal);
	    request.setAttribute("success", MessageManager.getProperty("message.success.topUp"));    
	} catch (DAOException e) {
	    //log
	    e.printStackTrace();
	    request.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    request.setAttribute("error", MessageManager.getProperty("message.error.topUp"));
	}
	
	page = ConfManager.getProperty("path.notice");
	return page;
    }
}
