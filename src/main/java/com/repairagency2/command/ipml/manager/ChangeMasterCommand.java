package com.repairagency2.command.ipml.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.OrderService;
import com.repairagency2.entity.field.RequestParamsUtil.ChangeMasterParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class ChangeMasterCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}
	
	String orderIdRaw = request.getParameter(ChangeMasterParams.ORDER_ID);
	String masterIdRaw = request.getParameter(ChangeMasterParams.MASTER_ID);
	String cMasterIdRaw = request.getParameter(ChangeMasterParams.CURRENT_MASTER_ID);
	String changeFlagRaw = request.getParameter(ChangeMasterParams.CHANGE_FLAG);
	
	if (isNotValidParam(orderIdRaw, masterIdRaw, cMasterIdRaw)) {
	    // log
	    session.setAttribute("errorManager", MessageManager.getProperty("message.error.changeMaster.param"));
	    session.setAttribute("orderIdCurrent", orderIdRaw);
	    page = ConfManager.getProperty("path.controller.manager.index");
	    return page;
	}
	
	int orderId = Integer.parseInt(orderIdRaw);
	int masterId = Integer.parseInt(masterIdRaw);
	int changeFlag = Integer.parseInt(changeFlagRaw);
	
	try {
	    if (changeFlag == 1) {
		OrderService.changeMasterForOrder(orderId, masterId);
	    } else {
		OrderService.updateMasterForOrder(orderId, masterId);
	    }
	    session.setAttribute("successManager", MessageManager.getProperty("message.success.changeMaster"));
	    session.setAttribute("orderIdCurrent", orderId);
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    request.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    session.setAttribute("errorManager", MessageManager.getProperty("message.error.changeMaster"));
	    session.setAttribute("orderIdCurrent", orderId);
	}

	// log
	page = ConfManager.getProperty("path.controller.manager.index");
	return page;
    }
    
    private boolean isNotValidParam(String orderIdRaw, String masterIdRaw, String cMasterIdRaw) {
	if (orderIdRaw == null || masterIdRaw == null || orderIdRaw.isBlank() || masterIdRaw.isBlank()) {
	    return true;
	}
	try {
	    int orderId = Integer.parseInt(orderIdRaw);
	    int masterId = Integer.parseInt(masterIdRaw);
	    int cMasterId = Integer.parseInt(cMasterIdRaw);
	    if ((masterId < 0) || (orderId < 1) || (masterId == cMasterId)) {
		return true;
	    }
	} catch (NumberFormatException e) {
	    return true;
	}
	return false;
    }
}
