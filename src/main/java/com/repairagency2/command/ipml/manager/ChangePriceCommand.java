package com.repairagency2.command.ipml.manager;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.OrderService;
import com.repairagency2.entity.field.RequestParamsUtil.ChangePriceParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class ChangePriceCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}

	String orderIdRaw = request.getParameter(ChangePriceParams.ORDER_ID);
	String sumRaw = request.getParameter(ChangePriceParams.SUM);

	if (isNotValidParam(orderIdRaw, sumRaw)) {
	    // log
	    session.setAttribute("errorManager", MessageManager.getProperty("message.error.changePrice.param"));
	    session.setAttribute("orderIdCurrent", orderIdRaw);
	    page = ConfManager.getProperty("path.controller.manager.index");
	    return page;
	}

	int orderId = Integer.parseInt(orderIdRaw.trim());
	BigDecimal sum = new BigDecimal(sumRaw.trim()).setScale(2, RoundingMode.HALF_DOWN);

	try {
	    OrderService.changePriceForOrder(orderId, sum);
	    session.setAttribute("successManager", MessageManager.getProperty("message.success.changePrice"));
	    session.setAttribute("orderIdCurrent", orderId);
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    request.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    session.setAttribute("errorManager", MessageManager.getProperty("message.error.changePrice"));
	}

	// log
	page = ConfManager.getProperty("path.controller.manager.index");
	return page;
    }

    private boolean isNotValidParam(String orderIdRaw, String sumRaw) {
	if (orderIdRaw == null || sumRaw == null || orderIdRaw.isBlank() || sumRaw.isBlank()) {
	    return true;
	}
	try {
	    int orderId = Integer.parseInt(orderIdRaw);
	    BigDecimal sum = new BigDecimal(sumRaw.trim()).setScale(2, RoundingMode.HALF_DOWN);
	    if (sum.compareTo(BigDecimal.ONE) <= 0 || orderId < 1) {
		return true;
	    }
	} catch (NumberFormatException e) {
	    return true;
	}
	return false;
    }
}
