package com.repairagency2.command.ipml.manager;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.OrderService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.ManagerOrder;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class OrderFullCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}

	List<ManagerOrder> orders = new ArrayList<>();
	List<Account> masters = new ArrayList<>();
	Map<Integer, String> statuses = new LinkedHashMap<>();
	
	try {
	    orders = OrderService.getOrdersForManager();
	    masters = OrderService.getMasters();
	    statuses = OrderService.getStatuses();
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    return page;
	} catch (DBException e) {
	    // log
	    session.setAttribute("errorManager", MessageManager.getProperty("message.error.getOrders"));
	    page = ConfManager.getProperty("path.page.manager.index");
	    return page;
	}
	session.setAttribute("orders", orders);
	session.setAttribute("masters", masters);
	session.setAttribute("statuses", statuses);
	page = ConfManager.getProperty("path.page.manager.index");
	
	return page;
    }
}
