package com.repairagency2.command.ipml.manager;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.AccountService;
import com.repairagency2.entity.field.RequestParamsUtil.TopUpBalanceManagerParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class TopUpBalanceManagerCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}

	String userIdRaw = request.getParameter(TopUpBalanceManagerParams.USER_ID);
	String valueRaw = request.getParameter(TopUpBalanceManagerParams.SUM);
	
	if (isNotValidParam(userIdRaw, valueRaw)) {
	    // log
	    session.setAttribute("errorManager", MessageManager.getProperty("message.error.topUpBalance.param"));
	    session.setAttribute("userIdCurrent", userIdRaw);
	    page = ConfManager.getProperty("path.controller.manager.index");
	    return page;
	}
	
	int userId = Integer.parseInt(userIdRaw);
	BigDecimal value = new BigDecimal(valueRaw.trim()).setScale(2, RoundingMode.HALF_DOWN);
	
	try {
	    AccountService.updateBalanceAndHistoryManager(userId, value);
	    session.setAttribute("successManager", MessageManager.getProperty("message.success.topUpBalance"));
	    session.setAttribute("userIdCurrent", userId);
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    request.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    session.setAttribute("errorManager", MessageManager.getProperty("message.error.topUpBalance"));
	}
	
	// log
	page = ConfManager.getProperty("path.controller.manager.index");
	return page;
    }
    
    private boolean isNotValidParam(String userIdRaw, String valueRaw) {
	if (userIdRaw == null || valueRaw == null || userIdRaw.isBlank() || valueRaw.isBlank()) {
	    return true;
	}
	try {
	    int userId = Integer.parseInt(userIdRaw);
	    BigDecimal value = new BigDecimal(valueRaw.trim()).setScale(2, RoundingMode.HALF_DOWN);
	    if (value.compareTo(BigDecimal.ZERO) <= 0 || userId < 1) {
		return true;
	    }
	} catch (NumberFormatException e) {
	    return true;
	}
	return false;
    }
}
