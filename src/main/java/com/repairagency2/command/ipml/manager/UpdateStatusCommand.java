package com.repairagency2.command.ipml.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.OrderService;
import com.repairagency2.entity.field.RequestParamsUtil.UpdateStatusParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class UpdateStatusCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}
	String orderIdRaw = request.getParameter(UpdateStatusParams.ORDER_ID);
	String statusIdRaw = request.getParameter(UpdateStatusParams.STATUS_ID_NEW);
	String cStatusIdRaw = request.getParameter(UpdateStatusParams.CURRENT_STATUS_ID);
	
	if (isNotValidParam(orderIdRaw, statusIdRaw, cStatusIdRaw)) {
	    // log
	    session.setAttribute("errorManager", MessageManager.getProperty("message.error.updateStatus.param"));
	    session.setAttribute("orderIdCurrent", orderIdRaw);
	    page = ConfManager.getProperty("path.controller.manager.index");
	    return page;
	}
	
	int orderId = Integer.parseInt(orderIdRaw);
	int statusId = Integer.parseInt(statusIdRaw);
	
	try {
	    OrderService.updateStatusForOrder(orderId, statusId);
	    session.setAttribute("successManager", MessageManager.getProperty("message.success.updateStatus"));
	    session.setAttribute("orderIdCurrent", orderId);
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    request.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    session.setAttribute("errorManager", MessageManager.getProperty("message.error.updateStatus"));
	    session.setAttribute("orderIdCurrent", orderId);
	}
	
	// log
	page = ConfManager.getProperty("path.controller.manager.index");
	return page;
    }
    
    private boolean isNotValidParam(String orderIdRaw, String statusIdRaw, String cStatusIdRaw) {
	if (orderIdRaw == null || statusIdRaw == null || orderIdRaw.isBlank() || statusIdRaw.isBlank()) {
	    return true;
	}
	try {
	    int orderId = Integer.parseInt(orderIdRaw);
	    int masterId = Integer.parseInt(statusIdRaw);
	    int cMasterId = Integer.parseInt(cStatusIdRaw);
	    if ((masterId < 0) || (orderId < 1) || (masterId == cMasterId)) {
		return true;
	    }
	} catch (NumberFormatException e) {
	    return true;
	}
	return false;
    }

}
