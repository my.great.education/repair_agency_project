package com.repairagency2.command.ipml.user;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.AccountService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.field.RequestParamsUtil.ChangePasswordParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class ChangePasswordCommand implements ActionCommand {
    private static final Map<String, String> requestParams = new HashMap<>();
    
    static {
	requestParams.put(ChangePasswordParams.OLD_PASSWORD, 
		"^[a-zA-Z0-9]{5,8}$");
	requestParams.put(ChangePasswordParams.NEW_PASSWORD, 
		"^[a-zA-Z0-9]{5,8}$");
	requestParams.put(ChangePasswordParams.RP_NEW_PASSWORD, 
		"^[a-zA-Z0-9]{5,8}$");
    }
    
    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	
	if (session == null) {
	    // log
	    return page;
	}
	
	if (!isValidParam(request, session)) {
	    // log
	    page = ConfManager.getProperty("path.page.acc.profile");
	    return page;
	}
	
	String newPassword = request.getParameter(ChangePasswordParams.NEW_PASSWORD);	
	String oldPassword = request.getParameter(ChangePasswordParams.OLD_PASSWORD);	
	Account currentAccount = (Account) session.getAttribute("user");
	
	try {
	    AccountService.updatePassword(currentAccount, newPassword, oldPassword);
	    session.setAttribute("successProfile", MessageManager.getProperty("message.success.changePass")); 
	    // log
	} catch (DAOException e) {
	    //log
	    e.printStackTrace();
	    request.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    session.setAttribute("errorProfile", e.getMessage());
	}
	
	page = ConfManager.getProperty("path.page.acc.profile");
	return page;
    }
    
    private boolean isValidParam(HttpServletRequest req, HttpSession session) {
	String newPass = "";
	String newPassRP = "";
	
	for (Map.Entry<String, String> entry : requestParams.entrySet()) {
	    String param = entry.getKey();
	    String regex = entry.getValue();
	    String paramValueFromReq = req.getParameter(param);
	    	    
	    if (paramValueFromReq == null || (!paramValueFromReq.matches(regex))) {
		session.setAttribute("errorProfile", MessageManager.getProperty("message.error.changePass.param"));
		return false;
	    } 
	    if (param.equals(ChangePasswordParams.NEW_PASSWORD)) {
		newPass = paramValueFromReq;
	    }
	    if (param.equals(ChangePasswordParams.RP_NEW_PASSWORD)) {
		newPassRP = paramValueFromReq;
	    }
	}
	
	if (!newPass.equals(newPassRP)) {
	    session.setAttribute("errorProfile", MessageManager.getProperty("message.error.changePass.repeatPassword"));
	    return false;
	}
	
	return true;	
    }
}
