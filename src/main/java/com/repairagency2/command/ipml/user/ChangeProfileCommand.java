package com.repairagency2.command.ipml.user;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.AccountService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.field.RequestParamsUtil.ChangeProfileParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class ChangeProfileCommand implements ActionCommand {
    private static final Map<String, String> requestParams = new HashMap<>();
    
    static {
	requestParams.put(ChangeProfileParams.FIRSTNAME, 
		"^[a-zA-Zа-яА-ЯЇїЄєІі0-9]{3,16}$");
	requestParams.put(ChangeProfileParams.LASTNAME, 
		"^[a-zA-Zа-яА-ЯЇїЄєІі0-9]{3,16}$");
	requestParams.put(ChangeProfileParams.EMAIL, 
		"(\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6})");
    }
    
    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	Map<String, String> dataMap = new HashMap<>();
	
	if (session == null) {
	    // log
	    return page;
	}
	
	if (!isValidParam(request)) {
	    // log
	    session.setAttribute("errorProfile", MessageManager.getProperty("message.error.changeProfile.param"));
	    page = ConfManager.getProperty("path.page.acc.profile");
	    return page;
	}
	
	String newFirstname = request.getParameter(ChangeProfileParams.FIRSTNAME);
	String newLastname = request.getParameter(ChangeProfileParams.LASTNAME);
	String newEmail = request.getParameter(ChangeProfileParams.EMAIL);
		
	Account currentAccount = (Account) session.getAttribute("user");	
	dataMap = dataToChange(currentAccount, newFirstname, newLastname, newEmail);
	
	if (dataMap.isEmpty()) {
	    // log
	    page = ConfManager.getProperty("path.page.acc.profile");
	    return page;
	}
	
	try {
	    AccountService.updateAccountInfo(currentAccount, dataMap);
	    session.setAttribute("successProfile", MessageManager.getProperty("message.success.changeProfile")); 
	} catch (DAOException e) {
	    //log
	    e.printStackTrace();
	    request.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    session.setAttribute("errorProfile", MessageManager.getProperty("message.error.changeProfile"));
	}
	
	page = ConfManager.getProperty("path.page.acc.profile");
	return page;
    }
    
    private boolean isValidParam(HttpServletRequest req) {
	for (Map.Entry<String, String> entry : requestParams.entrySet()) {
	    String param = entry.getKey();
	    String regex = entry.getValue();
	    String paramValueFromReq = req.getParameter(param);
	    	    
	    if (paramValueFromReq == null || (!paramValueFromReq.matches(regex))) {
		return false;
	    }
	}	
	return true;	
    }
    
    private Map<String, String> dataToChange(Account account, String fName, String lName, String email) {
	Map<String, String> map = new HashMap<>();
	
	if (!fName.equals(account.getFirstname())) {
	    map.put(ChangeProfileParams.FIRSTNAME, fName);
	}
	if (!lName.equals(account.getLastname())) {
	    map.put(ChangeProfileParams.LASTNAME, lName);
	}
	if (!email.equals(account.getEmail()) ) {
	    map.put(ChangeProfileParams.EMAIL, email);
	}
	
	return map;
    }
}
