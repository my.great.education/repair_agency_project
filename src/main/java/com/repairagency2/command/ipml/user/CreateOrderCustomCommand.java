package com.repairagency2.command.ipml.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.OrderService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.Order;
import com.repairagency2.entity.field.RequestParamsUtil.CustomOrderParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class CreateOrderCustomCommand implements ActionCommand {
    private final String SPACES_PATTERN = "\\s{2,}";

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);	
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}	
	
	String text = request.getParameter(CustomOrderParams.TEXT)
		.trim()
		.replaceAll(SPACES_PATTERN, " ");
	
	if (!isValidParam(text)) {
	    // log
	    request.setAttribute("error", MessageManager.getProperty("message.error.customOrder.text"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	}
	
	Account account = (Account) session.getAttribute("user");
	Order newOrder = Order.getNewOrder();
	try {
	    OrderService.insertCustom(account, newOrder, text);
	    request.setAttribute("success", MessageManager.getProperty("message.success.order"));
	    page = ConfManager.getProperty("path.notice");
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    return page;
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    request.setAttribute("error", MessageManager.getProperty("message.error.order"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	}
	
	return page;
    }
    
    private boolean isValidParam(String text) {
	return !((text.isBlank()) || (text.length() < 20) || (text.length() > 300));
    }
}
