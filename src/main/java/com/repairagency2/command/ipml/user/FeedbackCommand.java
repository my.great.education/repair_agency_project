package com.repairagency2.command.ipml.user;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.mysql.cj.x.protobuf.MysqlxCrud.Order;
import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.AccountService;
import com.repairagency2.db.service.OrderService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.Feedback;
import com.repairagency2.entity.field.RequestParamsUtil.FeedbackParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class FeedbackCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}
	clearSession(session);
	
	String mastertIdParam = request.getParameter(FeedbackParams.MASTER_ID);
	String requestIdParam = request.getParameter(FeedbackParams.REQUEST_ID);
	String isFeedback = request.getParameter(FeedbackParams.FEEDBACK_FLAG);
	
	if (!isValidParam(mastertIdParam)) {
	    // log
	    session.setAttribute("feedbackError", MessageManager.getProperty("message.error.feedback"));
	    page = ConfManager.getProperty("path.page.feedback");
	    return page;
	}
	List<Feedback> feedbackList = new ArrayList<>();
	Account master = new Account();
	
	try {        
	    int mastertId = Integer.parseInt(mastertIdParam);
	    feedbackList = OrderService.getFeedbacksAndCustomizeMaster(mastertId, master);
	    
	    if (!feedbackList.isEmpty()) {
	    	session.setAttribute(FeedbackParams.FEEDBACK_LIST, feedbackList);
	    }
	    session.setAttribute("masterAccount", master);
	    
	    if (isFeedback != null) {
		if (!isValidParam(requestIdParam)) {
		    // log
		    session.setAttribute("feedbackError", MessageManager.getProperty("message.error.feedback"));
		    page = ConfManager.getProperty("path.page.feedback");
		    return page;
		}
		int requestId = Integer.parseInt(requestIdParam);
		session.setAttribute(FeedbackParams.FEEDBACK_FLAG, 1);
		session.setAttribute(FeedbackParams.REQUEST_ID, requestId);
	    }
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    return page;
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    session.setAttribute("error", MessageManager.getProperty("message.error.feedback"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	}
	// log
	page = ConfManager.getProperty("path.page.feedback");
	return page;
    }
    
    private boolean isValidParam(String param) {
	if( (param == null) || (param.isBlank()) ) {
	    return false;
	}
	try {
	    Integer.parseInt(param);
	} catch (NumberFormatException e) {
	    return false;
	}
	
	return true;
    }
    
    private void clearSession(HttpSession session) {
	session.removeAttribute(FeedbackParams.FEEDBACK_LIST);
	session.removeAttribute(FeedbackParams.FEEDBACK_FLAG);
	session.removeAttribute(FeedbackParams.REQUEST_ID);
    }
}
