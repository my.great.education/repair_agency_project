package com.repairagency2.command.ipml.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.db.service.OrderService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.AccountOrderHistory;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class OrderHistoryCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}

	List<AccountOrderHistory> orders = null;
	Account currentAccount = (Account) session.getAttribute("user");

	try {
	    orders = OrderService.getOrderHistory(currentAccount);
	    session.setAttribute("ordersHistory", orders);
	    page = ConfManager.getProperty("path.page.orders");
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    return page;
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    return page;
	}
	return page;
    }
}
