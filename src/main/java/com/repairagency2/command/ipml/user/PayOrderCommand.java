package com.repairagency2.command.ipml.user;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.OrderService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.field.RequestParamsUtil.PayOrderParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class PayOrderCommand implements ActionCommand{

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}
	
	Account currentAccount = (Account) session.getAttribute("user");
	String orderIdRaw = request.getParameter(PayOrderParams.ORDER_ID);
	String paymentRaw = request.getParameter(PayOrderParams.PAYMENT);
	
	if (isNotValidParam(orderIdRaw, paymentRaw)) {
	    // log
	    session.setAttribute("error", MessageManager.getProperty("message.error.order.pay"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	}
	
	int orderId = Integer.parseInt(orderIdRaw.trim());
	BigDecimal payment = new BigDecimal(paymentRaw.trim()).setScale(2, RoundingMode.HALF_DOWN);
	
	try {
	    int flag = OrderService.payOrderAndUpdateOrderHistory(currentAccount, orderId, payment);
	    if (flag == 0) {
		session.setAttribute("error", MessageManager.getProperty("message.error.order.payMoney"));
		page = ConfManager.getProperty("path.notice");
		return page;
	    }
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    return page;
	} catch (DBException e) {
	    // log
	    session.setAttribute("error", MessageManager.getProperty("message.error.order.pay"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	}
	
	session.setAttribute("error", MessageManager.getProperty("message.success.order.pay"));
	page = ConfManager.getProperty("path.notice");
	return page;
    }

    private boolean isNotValidParam(String orderIdRaw, String paymentRaw) {
	if (orderIdRaw == null || paymentRaw == null || orderIdRaw.isBlank() || paymentRaw.isBlank()) {
	    return true;
	}
	try {
	    int orderId = Integer.parseInt(orderIdRaw.trim());
	    BigDecimal payment = new BigDecimal(paymentRaw.trim());
	    if (orderId < 1) {
		 return true;
	    }
	} catch (NumberFormatException e) {
	    // log
	    e.printStackTrace();
	    return true;
	}
	
	return false;
    }
}
