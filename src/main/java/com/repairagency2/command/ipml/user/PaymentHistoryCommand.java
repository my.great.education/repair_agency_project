package com.repairagency2.command.ipml.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.AccountService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.AccountBalanceHistory;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class PaymentHistoryCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}

	List<AccountBalanceHistory> balanceHistory = null;
	Account currentAccount = (Account) session.getAttribute("user");

	try {
	    balanceHistory = AccountService.getBalanceHistory(currentAccount);
	    session.setAttribute("balanceHistory", balanceHistory);
	    page = ConfManager.getProperty("path.page.acc.paymentHistory");
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    return page;
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    request.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	}

	return page;
    }
}
