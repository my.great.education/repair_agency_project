package com.repairagency2.command.ipml.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.AccountService;
import com.repairagency2.entity.field.RequestParamsUtil.FeedbackParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class PublishFeedbackCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}
	
	String reviewText = request.getParameter(FeedbackParams.REVIEW_TEXT).trim();
	String gradeParam = request.getParameter(FeedbackParams.GRADE);
	String requestIdParam = request.getParameter(FeedbackParams.REQUEST_ID);
	String masterIdParam = request.getParameter(FeedbackParams.MASTER_ID);
	
	if (!isValidParam(reviewText, gradeParam, requestIdParam)) {
	    // log
	    session.setAttribute("feedbackError", MessageManager.getProperty("message.error.feedback"));
	    page = ConfManager.getProperty("path.page.feedback");
	    return page;
	}
	
	try {
	    byte grade = Byte.parseByte(gradeParam);
	    int requestId = Integer.parseInt(requestIdParam);
	    AccountService.insertFeedback(reviewText, grade, requestId);
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    return page;
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    session.setAttribute("error", MessageManager.getProperty("message.error.feedback"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	}
	
	session.removeAttribute(FeedbackParams.FEEDBACK_FLAG);
//	session.removeAttribute(FeedbackParams.MASTER_ID);
	session.removeAttribute(FeedbackParams.REQUEST_ID);
	
	int masterId = Integer.parseInt(masterIdParam); 
	page = ConfManager.getProperty("path.controller.aboutMaster") + "&id=" + masterId;
	return page;
    }
    
    private boolean isValidParam(String text, String grade, String requestId) {
	if( (text == null) || (text.isBlank()) || (text.length() < 3 || text.length() > 120) ) {
	    return false;
	}
	if( (grade == null) || (grade.isBlank()) || !(grade.equals("0") || grade.equals("1")) ) {
	    return false;
	}
	if( (requestId == null) || (requestId.isBlank()) ) {
	    return false;
	}
	
	try {
	    Integer.parseInt(requestId);
	} catch (NumberFormatException e) {
	    return false;
	}
	
	return true;
    }

}
