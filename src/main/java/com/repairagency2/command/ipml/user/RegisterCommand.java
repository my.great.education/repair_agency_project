package com.repairagency2.command.ipml.user;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.AccountService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.builder.AccountBuilder;
import com.repairagency2.entity.field.RequestParamsUtil.RegisterParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class RegisterCommand implements ActionCommand {
    private static final Map<String, String> requestParams = new HashMap<>();

    static {
	requestParams.put(RegisterParams.PHONE_NUMBER,
		"^\\s[(](67|68|97|98|63|73|93|50|66|99)[)]\\s\\d{3}[-]\\d{2}[-]\\d{2}$");
	requestParams.put(RegisterParams.FIRSTNAME, 
		"^[a-zA-Zа-яА-ЯЇїЄєІі0-9]{3,16}$");
	requestParams.put(RegisterParams.LASTNAME, 
		"^[a-zA-Zа-яА-ЯЇїЄєІі0-9]{3,16}$");
	requestParams.put(RegisterParams.EMAIL, 
		"(\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6})");
	requestParams.put(RegisterParams.LOGIN, 
		"^[a-zA-Z0-9]{5,8}$");
	requestParams.put(RegisterParams.PASSWORD, 
		"^[a-zA-Z0-9]{5,8}$");
	requestParams.put(RegisterParams.RP_PASSWORD, 
		"^[a-zA-Z0-9]{5,8}$");
    }

    @Override
    public String execute(HttpServletRequest req) {
	String page = null;
	
	try {
	    if (!isValidParam(req)) {
	        // log
	        page = ConfManager.getProperty("path.notice");
	        return page;
	    }
	} catch (IOException e1) {
	    // log
	    e1.printStackTrace();
	}
	
	Account newAccount = new Account();
	AccountBuilder ab = new AccountBuilder();
	ab.registerBuild(req, newAccount);	
	try {
	    AccountService.insert(newAccount);
	    // log
	    req.setAttribute("success", MessageManager.getProperty("message.success.register"));
	    page = ConfManager.getProperty("path.notice");
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    return page;
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    req.setAttribute("error", MessageManager.getProperty("message.error.register.already"));
	    page = ConfManager.getProperty("path.notice");
	    return page;
	}
	return page;
    }
    
    private boolean isValidParam(HttpServletRequest req) throws IOException {
	String pass = "";
	String RPpass = "";
	
	for (Map.Entry<String, String> entry : requestParams.entrySet()) {
	    String param = entry.getKey();
	    String regex = entry.getValue();
	    String paramValueFromReq = req.getParameter(param).trim();
	    	    
	    if (paramValueFromReq == null || (!paramValueFromReq.matches(regex))) {
		req.setAttribute("error", MessageManager.getProperty("message.error.register.param"));
		return false;
	    }
	    
	    if (param.equals(RegisterParams.PASSWORD)) {
		pass = paramValueFromReq;
	    }
	    
	    if (param.equals(RegisterParams.RP_PASSWORD)) {
		RPpass = paramValueFromReq;
	    }
	}
	
	if (!pass.equals(RPpass)) {
	    req.setAttribute("error", MessageManager.getProperty("message.error.register.repeatPassword"));
	    return false;
	}
	
	return true;
    }
}
