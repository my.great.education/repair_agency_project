package com.repairagency2.command.ipml.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.OrderService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.field.RequestParamsUtil.CustomOrderParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class UpdateCustomOrderTextCommand implements ActionCommand {
    private final String SPACES_PATTERN = "\\s{2,}";
    
    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}
	
	Account currentAccount = (Account) session.getAttribute("user");
	int orderId = Integer.parseInt(request.getParameter(CustomOrderParams.ORDER_ID));	
	String srcText = request.getParameter(CustomOrderParams.SRC_TEXT);
	String text = request.getParameter(CustomOrderParams.TEXT)
		.trim()
		.replaceAll(SPACES_PATTERN, " ");
		
	if (isTextMatches(text, srcText)) {
	    page = ConfManager.getProperty("path.controller.orderHistory");
	    return page;
	}
	
	if (isNotValid(text)) {
	    session.setAttribute("errorUpdate", MessageManager.getProperty("message.error.order.updateText"));
	    page = ConfManager.getProperty("path.controller.orderHistory");
	    return page;
	}
	
	try {
	    OrderService.updateCustomOrderText(currentAccount, text, orderId);
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    return page;
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    session.setAttribute("errorUpdate", MessageManager.getProperty("message.error.order.cancel"));
	    page = ConfManager.getProperty("path.controller.orderHistory");
	    return page;
	}
	//log 
	session.setAttribute("successUpdate", MessageManager.getProperty("message.success.order.updateText"));
	page = ConfManager.getProperty("path.controller.orderHistory");
	return page;
    }
    
    private boolean isTextMatches(String text, String srcText) {
	return (srcText != null && srcText.equals(text)) ;
    }
    
    private boolean isNotValid(String text) {
	return text == null || text.isEmpty() || text.length() < 20 || text.length() > 300;
    }
}
