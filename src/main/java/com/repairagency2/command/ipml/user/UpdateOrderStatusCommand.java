package com.repairagency2.command.ipml.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.OrderService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.field.RequestParamsUtil;
import com.repairagency2.entity.field.RequestParamsUtil.UpdateOrderStatusParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class UpdateOrderStatusCommand implements ActionCommand{

    @Override
    public String execute(HttpServletRequest request) {
	String page = null;
	HttpSession session = request.getSession(false);
	if (session == null || session.getAttribute("user") == null) {
	    // log
	    return page;
	}
	
	Account currentAccount = (Account) session.getAttribute("user");
	int orderId = Integer.parseInt(request.getParameter(UpdateOrderStatusParams.ORDER_ID));
	int newStatusId = Integer.parseInt(request.getParameter(UpdateOrderStatusParams.STATE));
	
	try {
	    OrderService.updateStatus(currentAccount, orderId, newStatusId);
	} catch (DAOException e) {
	    // log
	    e.printStackTrace();
	    return page;
	} catch (DBException e) {
	    session.setAttribute("errorUpdate", MessageManager.getProperty("message.error.order.status"));
	    page = ConfManager.getProperty("path.controller.orderHistory");
	    return page;
	}
	// log
	session.setAttribute("successUpdate", MessageManager.getProperty("message.success.order.status"));
	page = ConfManager.getProperty("path.controller.orderHistory");
	return page;
    }
}
