package com.repairagency2.command.manager;

import java.util.ResourceBundle;

public class ConfManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("com.repairagency2.resources.config");

    private ConfManager() {
    }

    public static String getProperty(String key) {
	return resourceBundle.getString(key);
    }
}
