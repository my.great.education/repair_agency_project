package com.repairagency2.command.manager;

import java.util.ResourceBundle;

public class DAOManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("com.repairagency2.resources.dao");

    private DAOManager() {
    }

    public static String getProperty(String key) {
	return resourceBundle.getString(key);
    }
}
