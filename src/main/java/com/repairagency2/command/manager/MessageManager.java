package com.repairagency2.command.manager;

import java.util.ResourceBundle;

public class MessageManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("com.repairagency2.resources.message");

    private MessageManager() {
    }

    public static String getProperty(String key) {
	return resourceBundle.getString(key);
    }
}
