package com.repairagency2.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.ActionCommand;
import com.repairagency2.command.ActionFactory;
import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;

@WebServlet("/controller")
public class ControllerServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	HttpSession session = req.getSession(false);
	
	if(req.getParameter("command") == null) {
	    if (session == null) {
		String page = req.getContextPath() + ConfManager.getProperty("path.notice");
		req.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
		req.getRequestDispatcher(page).forward(req, resp);
		return;
	    }
	    String page = req.getContextPath() + ConfManager.getProperty("path.notice");
	    session.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
	    resp.sendRedirect(page);
	} else {
	    processRequest(req, resp);
	}	
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	String page = null;
	HttpSession session = req.getSession(false);
	ActionFactory client = new ActionFactory();
	ActionCommand command = client.defineCommand(req);
	
	page = command.execute(req);

	if (page != null) {
	    if (isNotice(page)) {
		getServletContext().getRequestDispatcher(page).forward(req, resp);	
	    } else {
		resp.sendRedirect(req.getContextPath() + page);
	    }
	} else {
	    page = ConfManager.getProperty("path.notice");
	    req.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
	    req.getRequestDispatcher(page).forward(req, resp);
	}
    }

    private boolean isNotice(String page) {
	return page.equals(ConfManager.getProperty("path.notice"));
    }
}
