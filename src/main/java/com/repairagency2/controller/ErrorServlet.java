package com.repairagency2.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;

@WebServlet("/notice")
public class ErrorServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	if (req.getParameter("404") != null) {
	    req.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
	    req.getRequestDispatcher(ConfManager.getProperty("path.page.notice")).forward(req, resp);
	    return;
	}
	req.getRequestDispatcher(ConfManager.getProperty("path.page.notice")).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	Object notice = req.getAttribute("error");
	HttpSession session = req.getSession(false);

	if (notice == null) {
	    notice = req.getAttribute("success");
	    if (session != null) {
		session.setAttribute("success", notice);
	    }
	} else {
	    if (session != null) {
		session.setAttribute("error", notice);
	    }
	}

	String uri = req.getContextPath() + ConfManager.getProperty("path.notice");
	resp.sendRedirect(uri);
    }
}
