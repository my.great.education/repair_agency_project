package com.repairagency2.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import com.mysql.cj.jdbc.MysqlDataSource;

public class DBConnectorMySQL {
    private static volatile DataSource dataSourceInstance;

    private DBConnectorMySQL() {
    }

    public static DataSource getDataSourceInstance() {
	DataSource result = dataSourceInstance;
	if (result != null) {
	    return result;
	}
	synchronized (DBConnectorMySQL.class) {
	    if (dataSourceInstance == null) {
		result = getCustomizedDataSource();
		dataSourceInstance = result;
	    }
	    return dataSourceInstance;
	}
    }

    private static DataSource getCustomizedDataSource() {
	DataSource ds = null;
	try {
	    Context initContext = new InitialContext();
	    Context envContext = (Context) initContext.lookup("java:/comp/env");
	    ds = (DataSource) envContext.lookup("jdbc/RepairAgencyDB");
	} catch (NamingException e) {
	    System.err.println("Error get Context to DataSource");
	    e.printStackTrace();
	}
	return ds;
    }

    public static Connection getPooledConnection() throws SQLException {
	return getDataSourceInstance().getConnection();
    }

    public static void close(Connection con, Statement... stmts) throws SQLException {
	if (stmts.length > 0) {
	    for (Statement stmt : stmts) {
		if (stmt != null)
		    stmt.close();
	    }
	}
	if (con != null) {
	    con.close();
	}
    }

    public static Connection getLOCALPooledConnection() throws SQLException {
	MysqlDataSource ds = new MysqlConnectionPoolDataSource();
	ds.setUrl("jdbc:mysql://localhost:3306/repair_agency_db?user=repairagency.user&password=1234");

	return ds.getConnection();
    }
}
