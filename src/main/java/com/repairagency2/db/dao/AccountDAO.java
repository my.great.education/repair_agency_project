package com.repairagency2.db.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;

import com.repairagency2.entity.Account;
import com.repairagency2.entity.AccountBalanceHistory;
import com.repairagency2.exception.DBException;

public interface AccountDAO {
	List<Account> getAll() throws DBException;
	Account getByID(Connection con, int id) throws DBException;
	Account getByLogin(String login) throws DBException;
	List<Account> getByRoleID(Connection con, int roleID) throws DBException;
	String getPassByID(Connection con, int inputId) throws DBException;
	List<AccountBalanceHistory> getBalanceHistory(Connection con, int inputId) throws DBException;
	String getFirstname(Connection con, int inputId) throws DBException;
	String getLastname(Connection con, int inputId) throws DBException;
	int getRoleIdByName(Connection con, String string) throws DBException;
	
	void insert(Connection con, Account... accounts) throws DBException;
	void insertBalanceHistory(Connection con, Account account, BigDecimal payment) throws DBException;
	void insertFeedback(Connection con, String reviewText, byte grade, int requestId) throws DBException;
	
	void delete(int id) throws DBException;
	void update(Account account) throws DBException;
	void updateBalance(Connection con, Account account, BigDecimal newBalance) throws DBException;
	
	void updateFirstname(Connection con, Account account, String fName) throws DBException;
	void updateLastname(Connection con, Account account, String lName) throws DBException;
	void updateEmail(Connection con, Account account, String email) throws DBException;
	void updatePassword(Connection con, Account account, String pass) throws DBException;
	
		
}
