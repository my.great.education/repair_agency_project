package com.repairagency2.db.dao;

import com.repairagency2.exception.DAOException;

public abstract class DAOFactory {
	private static DAOFactory instance;
	private static String daoFactoryFQN;

	protected DAOFactory() {}

	public static synchronized DAOFactory getInstance() throws DAOException {
		if (instance == null) {
			try {
				Class<?> clazz = Class.forName(daoFactoryFQN);
				instance = (DAOFactory) clazz.getDeclaredConstructor().newInstance();
			} catch (Exception e) {
				e.printStackTrace();
				throw new DAOException("Error in DAOFactory#getInstance()", e);
				// log
			}
		}
		return instance;
	}

	public static void setDaoFactoryFQN(String FCN) {
		instance = null;
		DAOFactory.daoFactoryFQN = FCN;
	}

	public abstract AccountDAO getAccountDAO();
	public abstract OrderDAO getOrderDAO();
}
