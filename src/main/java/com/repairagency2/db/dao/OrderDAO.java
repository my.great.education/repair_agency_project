package com.repairagency2.db.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

import com.repairagency2.entity.Account;
import com.repairagency2.entity.AccountOrderHistory;
import com.repairagency2.entity.Feedback;
import com.repairagency2.entity.ManagerOrder;
import com.repairagency2.entity.Order;
import com.repairagency2.exception.DBException;

public interface OrderDAO {
    void insert(Connection con, Order newOrder) throws DBException;
    void insertHasService(Connection con, Order newOrder, int serviceId, String text) throws DBException;
    void insertMasterForOrder(Connection con, int orderId, int masterId) throws DBException;
    
    List<Order> selectOrders(Connection con, Account account) throws DBException;
    int getServiceId(Connection con, String name) throws DBException;
    String getStatusName(Connection con, int statusId) throws DBException;
    int getStatusId(Connection con, int orderId) throws DBException;
    int getStatusId(Connection con, String string) throws DBException;
    String getStatusNextName(Connection con, int statusId) throws DBException;
    int getStatusNextId(Connection con, int statusId) throws DBException;    
    String getCategoryName(Connection con, int oredrId) throws DBException;  
    String getDescription(Connection con, int orderId) throws DBException;
    Map<Integer, String> getAllStatuses(Connection con) throws DBException;
    
    void updateOrderText(Connection con, int orderId, String text) throws DBException;
    void updateOrderStatus(Connection con, int orderId, int newStatusId) throws DBException;
    void updatePriceForRequest(Connection con, int orderId, BigDecimal sum) throws DBException;
    void updatePriceForRequestHasService(Connection con, int orderId, BigDecimal sum) throws DBException;
    void updateMasterForOrder(Connection con, int orderId, int masterId) throws DBException;
  
    List<Feedback> getFeedbacksByMasterId(Connection con, int masterId) throws DBException;
    int getMasterIdByOrderId(Connection con, int orderId) throws DBException;
    String getMasterName(Connection con, int orderId) throws DBException;
    String getMasterLastName(Connection con, int orderId) throws DBException;
    String getFeedbackByOrderId(Connection con, int orderId) throws DBException;
    List<ManagerOrder> getOrdersForManager(Connection con) throws DBException;
    
    void deleteMasterForOrder(Connection con, int orderId) throws DBException;
    
       
}
