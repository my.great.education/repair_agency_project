package com.repairagency2.db.dao.mysql;

import com.repairagency2.db.dao.AccountDAO;
import com.repairagency2.db.dao.DAOFactory;
import com.repairagency2.db.dao.OrderDAO;

public class MysqDAOFactoryImpl extends DAOFactory {
    private AccountDAO userDAO;
    private OrderDAO orderDAO;

    @Override
    public AccountDAO getAccountDAO() {
	if (userDAO == null)
	    return new MysqlAccountDAOImpl();
	return userDAO;
    }

    @Override
    public OrderDAO getOrderDAO() {
	if (orderDAO == null)
	    return new MysqlOrderDAOImpl();
	return orderDAO;
    }

}
