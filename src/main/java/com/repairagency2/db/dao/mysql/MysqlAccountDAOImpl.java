package com.repairagency2.db.dao.mysql;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.repairagency2.db.DBConnectorMySQL;
import com.repairagency2.db.dao.AccountDAO;
import com.repairagency2.db.sql.MysqlAccountQuery;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.AccountBalanceHistory;
import com.repairagency2.entity.builder.*;
import com.repairagency2.exception.DBException;

public class MysqlAccountDAOImpl implements AccountDAO {
    AccountBuilder accountBuilder;

    public MysqlAccountDAOImpl() {
	accountBuilder = new AccountBuilder();
    }

    @Override
    public List<Account> getAll() throws DBException {
	List<Account> accounts = new ArrayList<>();

	try (Connection con = DBConnectorMySQL.getPooledConnection()) {
	    try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.SELECT_ALL)) {
		try (ResultSet rs = ps.executeQuery()) {
		    while (rs.next()) {
			Account account = accountBuilder.build(rs);
			accounts.add(account);
		    }
		}
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("Can't find all accounts", e);
	}
	return accounts;
    }

    @Override
    public Account getByID(Connection con, int inputId) throws DBException {
	Account account = null;

	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.SELECT_BY_ID)) {
	    ps.setInt(1, inputId);
	    try (ResultSet rs = ps.executeQuery()) {
		while (rs.next()) {
		    account = accountBuilder.build(rs);
		}
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("Can't find account by ID", e);
	}
	return account;
    }
    
    @Override
    public String getFirstname(Connection con, int inputId) throws DBException {
	String firstname = null;
	
	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.SELECT_FIRSTNAME)) {
	    ps.setInt(1, inputId);
	    try (ResultSet rs = ps.executeQuery()) {
		while (rs.next()) {
		    firstname = rs.getString(1);
		}
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("Can't find firstname by ID", e);
	}
	return firstname;
    }
    
    @Override
    public String getLastname(Connection con, int inputId) throws DBException {
	String lastname = null;
	
	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.SELECT_LASTNAME)) {
	    ps.setInt(1, inputId);
	    try (ResultSet rs = ps.executeQuery()) {
		while (rs.next()) {
		    lastname = rs.getString(1);
		}
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("Can't find lastname by ID", e);
	}
	return lastname;
    }

    @Override
    public String getPassByID(Connection con, int inputId) throws DBException {
	String pass = null;

	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.SELECT_PASS_BY_ID)) {
	    ps.setInt(1, inputId);
	    try (ResultSet rs = ps.executeQuery()) {
		while (rs.next()) {
		    pass = rs.getString(1);
		}
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("Can't find pass by ID", e);
	}
	return pass;
    }

    @Override
    public Account getByLogin(String login) throws DBException {
	Account account = null;

	try (Connection con = DBConnectorMySQL.getPooledConnection()) {
	    try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.SELECT_BY_LOGIN)) {
		ps.setString(1, login);
		try (ResultSet rs = ps.executeQuery()) {
		    while (rs.next()) {
			account = accountBuilder.build(rs);
		    }
		}
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("ERROR! Can't find account by LOGIN", e);
	}
	return account;
    }
    
    @Override
    public List<Account> getByRoleID(Connection con, int roleID) throws DBException {
	List<Account> accounts = new ArrayList<>();

	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.SELECT_BY_ROLE_ID)) {
	    ps.setInt(1, roleID);
	    try (ResultSet rs = ps.executeQuery()) {
		while (rs.next()) {
		    Account account = accountBuilder.build(rs);
		    accounts.add(account);
		}
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("Can't find account by ROLE_ID", e);
	}
	return accounts;
    }

    @Override
    public List<AccountBalanceHistory> getBalanceHistory(Connection con, int inputId) throws DBException {
	List<AccountBalanceHistory> result = new ArrayList<>();

	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.SELECT_BALANCE_HISTORY)) {
	    ps.setInt(1, inputId);
	    try (ResultSet rs = ps.executeQuery()) {
		while (rs.next()) {
		    AccountBalanceHistory value = AccountBalanceHistoryBuilder.build(rs);
		    result.add(value);
		}
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("Can't find balance history", e);
	}
	return result;
    }

    @Override
    public int getRoleIdByName(Connection con, String name) throws DBException {
	int id = 0;	
	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.SELECT_ROLE_ID_BY_NAME)) {
	    ps.setString(1, name);
	    try (ResultSet rs = ps.executeQuery()) {
		while (rs.next()) {
		    id = rs.getInt(1);
		}
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("Can't find role id by name", e);
	}
	
	return id;
    }

    @Override
    public void insert(Connection con, Account... accounts) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.INSERT)) {
	    for (Account account : accounts) {
		int k = 1;
		ps.setInt(k++, account.getId());
		ps.setString(k++, account.getLogin());
		ps.setString(k++, account.getPassword());
		ps.setString(k++, account.getFirstname());
		ps.setString(k++, account.getLastname());
		ps.setString(k++, account.getEmail());
		ps.setString(k++, account.getPhoneNumber());
		ps.setBigDecimal(k++, account.getBalance());
		ps.executeUpdate();
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("MysqlAccountDAOImpl#insert() -> Can't insert new user", e);
	}
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void update(Account account) {

    }

    @Override
    public void updateBalance(Connection con, Account account, BigDecimal newBalance) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.UPDATE_BALANCE)) {
	    int k = 1;
	    ps.setBigDecimal(k++, newBalance);
	    ps.setInt(k++, account.getId());

	    ps.executeUpdate();
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("MysqlAccountDAOImpl#updateBalance() -> Can't update balance for user", e);
	}
    }

    @Override
    public void insertBalanceHistory(Connection con, Account account, BigDecimal payment) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.INSERT_BALANCE_HISTORY)) {
	    int k = 1;
	    ps.setInt(k++, account.getId());
	    ps.setBigDecimal(k++, payment);

	    ps.executeUpdate();
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("MysqlAccountDAOImpl#insertBalanceHistory() -> Can't update balance for user", e);
	}
    }

    @Override
    public void updateFirstname(Connection con, Account account, String fName) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.UPDATE_FIRSTNAME)) {
	    int k = 1;
	    ps.setString(k++, fName);
	    ps.setInt(k++, account.getId());

	    ps.executeUpdate();
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("MysqlAccountDAOImpl#updateFirstname() -> Can't update firstname for user", e);
	}
    }

    @Override
    public void updateLastname(Connection con, Account account, String lName) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.UPDATE_LASTNAME)) {
	    int k = 1;
	    ps.setString(k++, lName);
	    ps.setInt(k++, account.getId());

	    ps.executeUpdate();
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("MysqlAccountDAOImpl#updateLastname() -> Can't update lastname for user", e);
	}
    }

    @Override
    public void updateEmail(Connection con, Account account, String email) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.UPDATE_EMAIL)) {
	    int k = 1;
	    ps.setString(k++, email);
	    ps.setInt(k++, account.getId());

	    ps.executeUpdate();
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("MysqlAccountDAOImpl#updateEmail() -> Can't update email for user", e);
	}
    }

    @Override
    public void updatePassword(Connection con, Account account, String pass) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.UPDATE_PASSWORD)) {
	    int k = 1;
	    ps.setString(k++, pass);
	    ps.setInt(k++, account.getId());

	    ps.executeUpdate();
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("500 Can't update password for user", e);
	}
    }

    @Override
    public void insertFeedback(Connection con, String reviewText, byte grade, int requestId) throws DBException {
	Timestamp date = new Timestamp(new Date().getTime());
	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.INSERT_FEEDBACK)) {
	    int k = 1;
	    ps.setString(k++, reviewText);
	    ps.setTimestamp(k++, date);
	    ps.setByte(k++, grade);
	    ps.setInt(k++, requestId);

	    ps.executeUpdate();
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new DBException("500 Can't update review", e);
	}
    }
}
