package com.repairagency2.db.dao.mysql;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.repairagency2.db.dao.OrderDAO;
import com.repairagency2.db.sql.MysqlAccountQuery;
import com.repairagency2.db.sql.MysqlOrderQuery;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.AccountBalanceHistory;
import com.repairagency2.entity.AccountOrderHistory;
import com.repairagency2.entity.Feedback;
import com.repairagency2.entity.ManagerOrder;
import com.repairagency2.entity.Order;
import com.repairagency2.entity.builder.AccountBalanceHistoryBuilder;
import com.repairagency2.entity.builder.OrderBuilder;
import com.repairagency2.exception.DBException;

public class MysqlOrderDAOImpl implements OrderDAO {

    @Override
    public void insert(Connection con, Order newOrder) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.INSERT, PreparedStatement.RETURN_GENERATED_KEYS)) {
	    int k = 1;
	    ps.setBigDecimal(k++, newOrder.getPrice());
	    ps.setInt(k++, newOrder.getAccountId());
	    ps.executeUpdate();

	    try (ResultSet rs = ps.getGeneratedKeys()) {
		if (rs.next()) {
		    newOrder.setId(rs.getInt(1));
		}
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't insert order", e);
	}
    }

    @Override
    public void insertHasService(Connection con, Order newOrder, int serviceId, String text) throws DBException {
	int amount = 1;
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.INSERT_HAS_SERVICE)) {
	    int k = 1;
	    ps.setInt(k++, newOrder.getId());
	    ps.setInt(k++, serviceId);
	    ps.setInt(k++, amount);
	    ps.setBigDecimal(k++, newOrder.getPrice());
	    ps.setString(k++, text);
	    ps.executeUpdate();

	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't insert has service by custom", e);
	}
    }
    

    @Override
    public void insertMasterForOrder(Connection con, int orderId, int masterId) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.INSERT_REQUEST_EXECUTOR)) {
	    int k = 1;
	    ps.setInt(k++, orderId);
	    ps.setInt(k++, masterId);
	    ps.executeUpdate();
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't insert master for order id: " + orderId, e);
	}
    }

    @Override
    public int getServiceId(Connection con, String name) throws DBException {
	int serviceId = 0;
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_SERVICE_ID)) {
	    ps.setString(1, name);
	    
	    try (ResultSet rs = ps.executeQuery()) {
		if (rs.next()) {
		    serviceId = rs.getInt(1);
		}
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get service id", e);
	}
	return serviceId;
    }

    @Override
    public List<Order> selectOrders(Connection con, Account account) throws DBException {
	List<Order> list = new ArrayList<>();
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.SELECT_BY_ACCOUNT_ID)) {
	    ps.setInt(1, account.getId());
	    
	    try (ResultSet rs = ps.executeQuery()) {
		while (rs.next()) {
		    Order order = OrderBuilder.build(rs);
		    list.add(order);
		}
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get service id", e);
	}
	return list;
    }

    @Override
    public String getStatusName(Connection con, int statusId) throws DBException {
	String status = "";
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_STATUS_NAME)) {
	    ps.setInt(1, statusId);
	    
	    try (ResultSet rs = ps.executeQuery()) {
		if (rs.next()) {
		    status = rs.getString(1);
		}
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get status name", e);
	}
	return status;
    }
    
    @Override
    public String getStatusNextName(Connection con, int statusId) throws DBException {
	String statusTo = "";
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_STATUS_NAME_TO)) {
	    ps.setInt(1, statusId);
	    
	    try (ResultSet rs = ps.executeQuery()) {
		if (rs.next()) {
		    statusTo = rs.getString(1);
		}
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get status name to", e);
	}
	return statusTo;
    }
    
    @Override
    public int getStatusNextId(Connection con, int statusId) throws DBException {
	int statusTo = 0;
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_STATUS_TO_ID)) {
	    ps.setInt(1, statusId);
	    
	    try (ResultSet rs = ps.executeQuery()) {
		if (rs.next()) {
		    statusTo = rs.getInt(1);
		}
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get status next id", e);
	}
	return statusTo;
    }
    
    @Override
    public int getStatusId(Connection con, int orderId) throws DBException {
	int status = 1;
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_STATUS_ID_FOR_REQUEST)) {
	    ps.setInt(1, orderId);
	    
	    try (ResultSet rs = ps.executeQuery()) {
		if (rs.next()) {
		    status = rs.getInt(1);
		}
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get status id", e);
	}
	return status;
    }
    
    @Override
    public int getStatusId(Connection con, String string) throws DBException {
	int status = 1;
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_REQUEST_STATUS)) {
	    ps.setString(1, string);
	    
	    try (ResultSet rs = ps.executeQuery()) {
		if (rs.next()) {
		    status = rs.getInt(1);
		}
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get status id for Canceled", e);
	}
	return status;
    }

    @Override
    public String getCategoryName(Connection con, int oredrId) throws DBException {
	String category = "";
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_CATEGORY_FOR_REQUEST)) {
	    ps.setInt(1, oredrId);
	    
	    try (ResultSet rs = ps.executeQuery()) {
		if (rs.next()) {
		    category = rs.getString(1);
		}
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get service id", e);
	}
	return category;
    }

    @Override
    public String getMasterName(Connection con, int orderId) throws DBException {
	String masterName = "Not assigned";
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_MASTER_NAME_FOR_REQUEST)) {
	    ps.setInt(1, orderId);  
	    
	    try (ResultSet rs = ps.executeQuery()) {
		if (rs.next()) {
		    masterName = rs.getString(1);
		}
	    }    
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get master name", e);
	}
	return masterName;
    }
    
    @Override
    public String getMasterLastName(Connection con, int orderId) throws DBException {
	String masterLastName = "Not assigned";
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_MASTER_LASTNAME_FOR_REQUEST)) {
	    ps.setInt(1, orderId);  
	    
	    try (ResultSet rs = ps.executeQuery()) {
		if (rs.next()) {
		    masterLastName = rs.getString(1);
		}
	    }    
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get master lastname", e);
	}
	return masterLastName;
    }
    
    @Override
    public List<Feedback> getFeedbacksByMasterId(Connection con, int masterId) throws DBException {
	List<Feedback> feedbackList = new ArrayList<>();
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_FEEDBACKS_BY_MASTER_ID)) {
	    ps.setInt(1, masterId);  
	    
	    try (ResultSet rs = ps.executeQuery()) {
		while (rs.next()) {
		    Feedback feedback = OrderBuilder.buildFeedback(rs);
		    feedbackList.add(feedback);
		}
	    }    
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get feedback entity", e);
	}
	return feedbackList;
    }

    @Override
    public String getDescription(Connection con, int orderId) throws DBException {
	String desc = "";
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_DESCRIPTION)) {
	    ps.setInt(1, orderId);
	    
	    try (ResultSet rs = ps.executeQuery()) {
		if (rs.next()) {
		    desc = rs.getString(1);
		}
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get description", e);
	}
	return desc;
    }

    @Override
    public void updateOrderText(Connection con, int orderId, String text) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.UPDATE_DESCRIPTION)) {
	    int k = 1;
	    ps.setString(k++, text);
	    ps.setInt(k++, orderId);
	    ps.executeUpdate();
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't udate order text", e);
	}
    }

    @Override
    public void updateOrderStatus(Connection con, int orderId, int newStatusId) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.UPDATE_STATUS)) {
	    int k = 1;
	    ps.setInt(k++, newStatusId);
	    ps.setInt(k++, orderId);
	    ps.executeUpdate();
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't udate order status", e);
	}
    }

    @Override
    public int getMasterIdByOrderId(Connection con, int orderId) throws DBException {
	int masterId = 0;
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_MASTER_ID_BY_REQUEST_ID)) {
	    ps.setInt(1, orderId);
	    
	    try (ResultSet rs = ps.executeQuery()) {
		if (rs.next()) {
		    masterId = rs.getInt(1);
		}
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get master id", e);
	}
	return masterId;
    }

    @Override
    public String getFeedbackByOrderId(Connection con, int orderId) throws DBException {
	String review = "";
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_REVIEW_BY_ORDER_ID)) {
	    ps.setInt(1, orderId);
	    
	    try (ResultSet rs = ps.executeQuery()) {
		if (rs.next()) {
		    review = rs.getString(1);
		}
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get review by order id", e);
	}
	return review;
    }

    @Override
    public List<ManagerOrder> getOrdersForManager(Connection con) throws DBException {
	List<ManagerOrder> orders = new ArrayList<>();
	
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.GET_ORDERS_FOR_MANAGER)) {
	    try (ResultSet rs = ps.executeQuery()) {
		while (rs.next()) {
		    ManagerOrder order = OrderBuilder.buildOrderForManager(rs);
		    orders.add(order);
		}
	    }    
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get orders for manager", e);
	}
	
	return orders;
    }

    @Override
    public void updatePriceForRequest(Connection con, int orderId, BigDecimal sum) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.UPDATE_PRICE_FOR_REQUEST)) {
	    int k = 1;
	    ps.setBigDecimal(k++, sum);
	    ps.setInt(k++, orderId);
	    ps.executeUpdate();
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't udate order price for request", e);
	}
    }
    @Override
    public void updatePriceForRequestHasService(Connection con, int orderId, BigDecimal sum) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.UPDATE_PRICE_FOR_REQUEST_HAS_SERVICE)) {
	    int k = 1;
	    ps.setBigDecimal(k++, sum);
	    ps.setInt(k++, orderId);
	    ps.executeUpdate();
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't udate order price for request has service", e);
	}
    }

    @Override
    public void updateMasterForOrder(Connection con, int orderId, int masterId) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.UPDATE_MASTER_FOR_REQUEST)) {
	    int k = 1;
	    ps.setInt(k++, masterId);
	    ps.setInt(k++, orderId);
	    ps.executeUpdate();
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't udate master for request has executor", e);
	}
    }

    @Override
    public void deleteMasterForOrder(Connection con, int orderId) throws DBException {
	try (PreparedStatement ps = con.prepareStatement(MysqlOrderQuery.DELETE_MASTER_FOR_REQUEST)) {
	    ps.setInt(1, orderId);
	    ps.executeUpdate();
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't delete master for request executor. Order id: " + orderId, e);
	}
    }

    @Override
    public Map<Integer, String> getAllStatuses(Connection con) throws DBException {
	Map<Integer, String> statuses = new LinkedHashMap<>();
	
	try (PreparedStatement ps = con.prepareStatement(MysqlAccountQuery.SELECT_ALL_STATUSES)) {
	    try (ResultSet rs = ps.executeQuery()) {
		while (rs.next()) {
		    statuses.put(rs.getInt(1), rs.getString(2));
		}
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DBException("Can't get all status", e);
	}
	return statuses;
    }

}
