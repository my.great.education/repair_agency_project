package com.repairagency2.db.service;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateful;

import com.repairagency2.command.manager.DAOManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.DBConnectorMySQL;
import com.repairagency2.db.dao.AccountDAO;
import com.repairagency2.db.dao.DAOFactory;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.AccountBalanceHistory;
import com.repairagency2.entity.field.RequestParamsUtil.ChangeProfileParams;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

@Stateful
public class AccountService {

    public Account find(String login, String pass) throws DAOException, DBException {
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	AccountDAO accDao = DAOFactory.getInstance().getAccountDAO();
	Account account = accDao.getByLogin(login);
	return account;
    }
    
    public Account find(int id) throws DAOException, DBException {
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	AccountDAO accDao = DAOFactory.getInstance().getAccountDAO();
	Account account = null;
	
	try (Connection con = DBConnectorMySQL.getPooledConnection()) {
	    account = accDao.getByID(con, id);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	}
	 
	return account;
    }

    public boolean isValidUserPassword(Account user, String password) {
	return user.getPassword().equals(password) ? true : false;
    }

    public static void insert(Account newAccount) throws DAOException, DBException {
	Connection con = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	AccountDAO accDao = DAOFactory.getInstance().getAccountDAO();

	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);

	    accDao.insert(con, newAccount);

	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    e.printStackTrace();
	    // log
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    e.printStackTrace();
	    // log
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException("AccountService#insert() -> Can't close connection for insert user!", e);
	    }
	}
    }

    public static void updateBalanceAndHistory(Account account, BigDecimal payment) throws DAOException, DBException {
	Connection con = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	AccountDAO accDao = DAOFactory.getInstance().getAccountDAO();

	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);

	    Account accountFromDB = accDao.getByID(con, account.getId());
	    BigDecimal newBalance = accountFromDB.getBalance().add(payment);
	    accountFromDB.setBalance(newBalance);

	    accDao.updateBalance(con, accountFromDB, newBalance);
	    accDao.insertBalanceHistory(con, accountFromDB, payment);
	    account.setBalance(accountFromDB.getBalance());

	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException("AccountService#updateBalanceAndHistory() -> Can't close connection update balance for user: " + account.getId(), e);
	    }
	}
    }
    
    public static void updateBalanceAndHistoryManager(int userId, BigDecimal payment) throws DAOException, DBException {
	Connection con = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	AccountDAO accDao = DAOFactory.getInstance().getAccountDAO();
	
	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);
	    
	    Account accountFromDB = accDao.getByID(con, userId);
	    BigDecimal newBalance = accountFromDB.getBalance().add(payment);
	    accountFromDB.setBalance(newBalance);
	    
	    accDao.updateBalance(con, accountFromDB, newBalance);
	    accDao.insertBalanceHistory(con, accountFromDB, payment);
	    
	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException("AccountService#updateBalanceAndHistory() -> Can't close connection update balance for user: " + userId, e);
	    }
	}
    }
    
    public static void updatePassword(Account account, String newPassword, String oldPassword) throws DAOException, DBException {
	Connection con = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	AccountDAO accDao = DAOFactory.getInstance().getAccountDAO();
	
	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);

	    String passFromDB = accDao.getPassByID(con, account.getId());
	    if (!passFromDB.equals(oldPassword)) {
		// log
		throw new DBException(MessageManager.getProperty("message.error.changePass.oldPass"));
	    }
	    
	    accDao.updatePassword(con, account, newPassword);

	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException(
			"AccountService#updatePassword() -> Can't close connection update password for user!", e);
	    }
	}
    }
    
    public static void updateAccountInfo(Account account, Map<String, String> updateInfo)
	    throws DAOException, DBException {
	Connection con = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	AccountDAO accDao = DAOFactory.getInstance().getAccountDAO();

	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);

	    Account accountFromDB = accDao.getByID(con, account.getId());

	    for (var entry : updateInfo.entrySet()) {
		String param = entry.getKey();
		String value = entry.getValue();

		switch (param) {
		case ChangeProfileParams.FIRSTNAME:
		    accDao.updateFirstname(con, accountFromDB, value);
		    account.setFirstname(value);
		    break;
		case ChangeProfileParams.LASTNAME:
		    accDao.updateLastname(con, accountFromDB, value);
		    account.setLastname(value);
		    break;
		case ChangeProfileParams.EMAIL:
		    accDao.updateEmail(con, accountFromDB, value);
		    account.setEmail(value);
		    break;
		}
	    }

	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    // log
	    e.printStackTrace();
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException(
			"AccountService#updateChangeProfile() -> Can't close connection update balance for user!", e);
	    }
	}
    }
    
    public static List<AccountBalanceHistory> getBalanceHistory(Account account) throws DAOException, DBException {
	List<AccountBalanceHistory> list = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	AccountDAO accDao = DAOFactory.getInstance().getAccountDAO();
	
	try (Connection con = DBConnectorMySQL.getPooledConnection()) {
	    list = accDao.getBalanceHistory(con, account.getId());
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	}
	
	return list.stream()
		.sorted((o1, o2) -> o2.getTime().compareTo(o1.getTime()))
		.toList();
    }

    public static void insertFeedback(String reviewText, byte grade, int requestId) throws DAOException, DBException {
	Connection con = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	AccountDAO accDao = DAOFactory.getInstance().getAccountDAO();

	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);
	    
	    accDao.insertFeedback(con, reviewText, grade, requestId);

	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    e.printStackTrace();
	    // log
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    e.printStackTrace();
	    // log
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException("AccountService#insertFeedback() -> Can't close connection for insert feedback!", e);
	    }
	}
    }
}
