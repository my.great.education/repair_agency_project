package com.repairagency2.db.service;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.repairagency2.command.manager.DAOManager;
import com.repairagency2.db.DBConnectorMySQL;
import com.repairagency2.db.dao.AccountDAO;
import com.repairagency2.db.dao.DAOFactory;
import com.repairagency2.db.dao.OrderDAO;
import com.repairagency2.entity.*;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

public class OrderService {

    public static void insertCustom(Account account, Order newOrder, String text) throws DAOException, DBException {
	Connection con = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	OrderDAO orderDao = DAOFactory.getInstance().getOrderDAO();

	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);

	    int serviceId = orderDao.getServiceId(con, "CUSTOM");
	    newOrder.setAccountId(account.getId());

	    orderDao.insert(con, newOrder);
	    orderDao.insertHasService(con, newOrder, serviceId, text);

	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    e.printStackTrace();
	    // log
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    e.printStackTrace();
	    // log
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException("OrderService#insertCustom() -> Can't close connection for insert custom order!",
			e);
	    }
	}
    }

    public static List<AccountOrderHistory> getOrderHistory(Account account) throws DAOException, DBException {
	List<AccountOrderHistory> list = new ArrayList<>();
	List<Order> orders = new ArrayList<>();
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	OrderDAO orderDao = DAOFactory.getInstance().getOrderDAO();

	try (Connection con = DBConnectorMySQL.getPooledConnection()) {
	    orders = orderDao.selectOrders(con, account);
	    for (Order order : orders) {
		AccountOrderHistory val = new AccountOrderHistory();
		String status = orderDao.getStatusName(con, order.getStatusId());
		String category = orderDao.getCategoryName(con, order.getId());
		String masterName = orderDao.getMasterName(con, order.getId());
		String description = orderDao.getDescription(con, order.getId());
		int masterId = orderDao.getMasterIdByOrderId(con, order.getId());
		String review = orderDao.getFeedbackByOrderId(con, order.getId());
		boolean isThereReview = true;

		if (review == null || review.isEmpty()) {
		    isThereReview = false;
		}

		val.setRequestId(order.getId());
		val.setDate(order.getCreateTime());
		val.setPayment(order.getPrice());
		val.setStatus(status);
		val.setCategory(category);
		val.setMaster(masterName);
		val.setDescription(description);
		val.setMasterId(masterId);
		val.setThereReview(isThereReview);
		list.add(val);
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	}
	list.sort(new Comparator<AccountOrderHistory>() {
	    @Override
	    public int compare(AccountOrderHistory o1, AccountOrderHistory o2) {
		return o2.getDate().compareTo(o1.getDate());
	    }
	});
	return list;
    }

    public static void updateCustomOrderText(Account currentAccount, String text, int orderId)
	    throws DAOException, DBException {
	Connection con = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	OrderDAO orderDao = DAOFactory.getInstance().getOrderDAO();

	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);

	    orderDao.updateOrderText(con, orderId, text);

	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    e.printStackTrace();
	    // log
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException(
			"OrderService#updateCustomOrderText() " + "-> Can't close connection for update order text!",
			e);
	    }
	}
    }
    
    public static void updateMasterForOrder(int orderId, int masterId) throws DAOException, DBException {
	Connection con = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	OrderDAO orderDao = DAOFactory.getInstance().getOrderDAO();
	
	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);
	    
	    if (masterId == 0) {
		orderDao.deleteMasterForOrder(con, orderId);
	    } else {
		orderDao.updateMasterForOrder(con, orderId, masterId);
	    }
	    
	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    e.printStackTrace();
	    // log
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException("OrderService#updateMasterForOrder() "
			+ "-> Can't close connection for update master for order id: " + orderId, e);
	    }
	}	
    }

    public static void updateStatus(Account currentAccount, int orderId, int newStatusId)
	    throws DAOException, DBException {
	Connection con = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	OrderDAO orderDao = DAOFactory.getInstance().getOrderDAO();

	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);

	    orderDao.updateOrderStatus(con, orderId, newStatusId);

	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    e.printStackTrace();
	    // log
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException(
			"OrderService#updateStatus() " + "-> Can't close connection for update order status!", e);
	    }
	}
    }

    public static int payOrderAndUpdateOrderHistory(Account currentAccount, int orderId, BigDecimal payment)
	    throws DAOException, DBException {
	Connection con = null;
	int flag = 1;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	OrderDAO orderDao = DAOFactory.getInstance().getOrderDAO();

	if (currentAccount.getBalance().compareTo(payment) == -1) {
	    flag = 0;
	    return flag;
	}

	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);

	    int newOrderStatus = 3;
	    BigDecimal negativePayment = new BigDecimal(payment.toString());
	    negativePayment = negativePayment.negate();

	    orderDao.updateOrderStatus(con, orderId, newOrderStatus);
	    AccountService.updateBalanceAndHistory(currentAccount, negativePayment);

	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    e.printStackTrace();
	    // log
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException("OrderService#payOrderAndUpdateOrderHistory() "
			+ "-> Can't close connection for pay order № " + orderId, e);
	    }
	}
	return flag;
    }

    public static List<Feedback> getFeedbacksAndCustomizeMaster(int mastertId, Account newAccForMaster)
	    throws DBException, DAOException {
	List<Feedback> list = new ArrayList<>();
	Account accFromDB = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	OrderDAO orderDao = DAOFactory.getInstance().getOrderDAO();
	AccountDAO accDao = DAOFactory.getInstance().getAccountDAO();

	try (Connection con = DBConnectorMySQL.getPooledConnection()) {
	    accFromDB = accDao.getByID(con, mastertId);

	    newAccForMaster.setId(accFromDB.getId());
	    newAccForMaster.setLastname(accFromDB.getLastname());
	    newAccForMaster.setFirstname(accFromDB.getFirstname());
	    newAccForMaster.setEmail(accFromDB.getEmail());
	    newAccForMaster.setPhoneNumber(accFromDB.getPhoneNumber());

	    list = orderDao.getFeedbacksByMasterId(con, mastertId);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	}
	list.sort(new Comparator<Feedback>() {
	    @Override
	    public int compare(Feedback o1, Feedback o2) {
		return o2.getReviewDate().compareTo(o1.getReviewDate());
	    }
	});
	return list;
    }

    public static List<ManagerOrder> getOrdersForManager() throws DAOException, DBException {
	List<ManagerOrder> orders = new ArrayList<>();
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	OrderDAO orderDao = DAOFactory.getInstance().getOrderDAO();
	AccountDAO accountDao = DAOFactory.getInstance().getAccountDAO();

	try (Connection con = DBConnectorMySQL.getPooledConnection()) {
	    orders = orderDao.getOrdersForManager(con);
	    for (ManagerOrder order : orders) {
		order.setStatusNextId(orderDao.getStatusNextId(con, order.getStatusId()));
		order.setStatusNext(orderDao.getStatusNextName(con, order.getStatusId()));
		order.setMasterName(accountDao.getFirstname(con, order.getMasterId()));
		order.setMasterLastname(accountDao.getLastname(con, order.getMasterId()));
	    }
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	}

	return orders;
    }

    public static void changePriceForOrder(int orderId, BigDecimal sum) throws DAOException, DBException {
	Connection con = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	OrderDAO orderDao = DAOFactory.getInstance().getOrderDAO();

	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);

	    orderDao.updatePriceForRequestHasService(con, orderId, sum);
	    orderDao.updatePriceForRequest(con, orderId, sum);

	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    e.printStackTrace();
	    // log
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException("OrderService#changePriceForOrder() "
			+ "-> Can't close connection for change order price id: " + orderId, e);
	    }
	}
    }
    
    public static void changeMasterForOrder(int orderId, int masterId) throws DAOException, DBException {
	Connection con = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	OrderDAO orderDao = DAOFactory.getInstance().getOrderDAO();
	
	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);
	    
	    orderDao.insertMasterForOrder(con, orderId, masterId);
	    
	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    e.printStackTrace();
	    // log
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException("OrderService#changeMasterForOrder() "
			+ "-> Can't close connection for change master for order id: " + orderId, e);
	    }
	}
    }

    public static List<Account> getMasters() throws DAOException, DBException {
	List<Account> masters = new ArrayList<>();
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	AccountDAO accountDao = DAOFactory.getInstance().getAccountDAO();

	try (Connection con = DBConnectorMySQL.getPooledConnection()) {
	    int crfatsmanId = accountDao.getRoleIdByName(con, "craftsman");
	    masters = accountDao.getByRoleID(con, crfatsmanId);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	}

	return masters;
    }

    public static void updateStatusForOrder(int orderId, int statusId) throws DAOException, DBException {
	Connection con = null;
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	OrderDAO orderDao = DAOFactory.getInstance().getOrderDAO();

	try {
	    con = DBConnectorMySQL.getPooledConnection();
	    int transactionIsolation = con.getTransactionIsolation();
	    boolean autoCommit = con.getAutoCommit();
	    con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    con.setAutoCommit(false);
	    
	    int statusCanceled = orderDao.getStatusId(con, "canceled");
	    int statusIdFromDB = orderDao.getStatusId(con, orderId);
	    int statusNextFromDB = orderDao.getStatusNextId(con, statusIdFromDB);
	    
	    if ((statusNextFromDB != statusId) && (statusId != statusCanceled)) {
		throw new DBException("The incoming status value does not match the possible value");
	    }
	    if (statusId == statusCanceled) {
		orderDao.updateOrderStatus(con, orderId, statusCanceled);
	    } else {
		orderDao.updateOrderStatus(con, orderId, statusNextFromDB);

	    }	    

	    con.commit();
	    con.setTransactionIsolation(transactionIsolation);
	    con.setAutoCommit(autoCommit);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	} catch (DBException e) {
	    e.printStackTrace();
	    // log
	    if (con != null) {
		try {
		    con.rollback();
		} catch (SQLException e1) {
		    // log
		    e1.printStackTrace();
		}
	    }
	    throw new DBException(e.getMessage(), e);
	} finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    } catch (SQLException e) {
		// log
		e.printStackTrace();
		throw new DBException(
			"OrderService#updateStatusForOrder() " + "-> Can't close connection for update order status!",
			e);
	    }
	}
    }

    public static Map<Integer, String> getStatuses() throws DAOException, DBException{
	Map<Integer, String> statuses = new LinkedHashMap<>();
	DAOFactory.setDaoFactoryFQN(DAOManager.getProperty("mysql"));
	OrderDAO orderDao = DAOFactory.getInstance().getOrderDAO();

	try (Connection con = DBConnectorMySQL.getPooledConnection()) {
	    statuses = orderDao.getAllStatuses(con);
	} catch (SQLException e) {
	    // log
	    e.printStackTrace();
	    throw new DAOException("Can't get connection", e);
	}
	
	return statuses;
    }
}
