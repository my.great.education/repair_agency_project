package com.repairagency2.db.sql;

public class MysqlAccountQuery {
    public static final String SELECT_ALL = "SELECT * FROM `account`";
    public static final String SELECT_ALL_STATUSES = "SELECT * FROM `request_status`";
    public static final String SELECT_BY_ID = "SELECT * FROM `account` WHERE id = ?";
    public static final String SELECT_BY_LOGIN = "SELECT * FROM `account` WHERE login = ?";
    public static final String SELECT_BY_ROLE_ID = "SELECT * FROM `account` WHERE role_id = ?";
    public static final String SELECT_ID_BY_NAME = "SELECT `id` FROM `account` WHERE `name` = ?";
    public static final String SELECT_PASS_BY_ID = "SELECT `pass` FROM `account` WHERE `id` = ?";
    public static final String SELECT_FIRSTNAME = "SELECT `firstname` FROM `account` WHERE `id` = ?";
    public static final String SELECT_LASTNAME = "SELECT `lastname` FROM `account` WHERE `id` = ?";
    public static final String SELECT_ROLE_ID_BY_NAME = "SELECT `id` FROM `role` WHERE `name` = ?";

    private static final String DEFAULT_STATUS_ID = "(SELECT `id` FROM `account_status` WHERE `status` = \"ACTIVE\")";
    private static final String DEFAULT_ROLE = "(SELECT `id` FROM `role` WHERE `name` = \"client\")";
    public static final String INSERT = "INSERT INTO `account` (`id`, `login`, `pass`, `firstname`, `lastname`, `email`, `phone_number`,`balance`, `status_id`, `role_id`) "
	    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, " + DEFAULT_STATUS_ID + ", " + DEFAULT_ROLE + ")";
    public static final String INSERT_FEEDBACK = "UPDATE `request_executor` SET `review` = ?, `review_time` = ?, `grade` = ? WHERE `request_id` = ?";

    public static final String UPDATE_BALANCE = "UPDATE `account` SET `balance` = ? WHERE id = ?";
    public static final String INSERT_BALANCE_HISTORY = "INSERT INTO `account_balance_history` (`account_id`, `payment`) "
    	+ "VALUES (?, ?)";
    
    public static final String UPDATE_FIRSTNAME = "UPDATE `account` SET `firstname` = ? WHERE id = ?";
    public static final String UPDATE_LASTNAME = "UPDATE `account` SET `lastname` = ? WHERE id = ?";
    public static final String UPDATE_EMAIL = "UPDATE `account` SET `email` = ? WHERE id = ?";
    public static final String UPDATE_PASSWORD = "UPDATE `account` SET `pass` = ? WHERE id = ?";
    
    public static final String SELECT_BALANCE_HISTORY = "SELECT * FROM `account_balance_history` WHERE `account_id` = ?";

}
