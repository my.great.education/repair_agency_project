package com.repairagency2.db.sql;

public class MysqlOrderQuery {
    public static final String SELECT_ALL = "SELECT * FROM `request`";
    public static final String SELECT_BY_ID = "SELECT * FROM `request` WHERE `id` = ?";
    public static final String SELECT_BY_ACCOUNT_ID = "SELECT * FROM `request` WHERE `account_id` = ?";
    public static final String SELECT_BY_STATUS_ID = "SELECT * FROM `request` WHERE `status_id` = ?";

    private static final String GET_STATUS_ID_NEW = "(SELECT `id` FROM `request_status` WHERE `condition` = \"awaiting processing\")";
    public static final String GET_PRICE = "(SELECT `price` FROM `service` WHERE `name` = ?)";
    public static final String GET_SERVICE_ID = "SELECT `id` FROM `service` WHERE `name` = ?";
    public static final String GET_SERVICE_NAME = "(SELECT `name` FROM `service` WHERE `id` = ?)";
    public static final String GET_STATUS_NAME = "(SELECT `condition` FROM `request_status` WHERE `id` = ?)";    
    public static final String GET_STATUS_NAME_TO = "SELECT `request_status`.`condition` FROM `request_status` "
    	+ "WHERE `request_status`.`id` = "
    	+ "(SELECT `status_flow`.`to` FROM `status_flow` WHERE `status_flow`.`from` = ?)";
    public static final String GET_STATUS_ID_FOR_REQUEST = "SELECT `status_id` FROM `request` WHERE `id` = ?";
    public static final String GET_REQUEST_STATUS = "SELECT `id` FROM `request_status` WHERE `condition` = ?";
    public static final String GET_STATUS_TO_ID = "SELECT `status_flow`.`to` FROM `status_flow` WHERE `from` = ?";
    public static final String GET_ORDER_ID = "SELECT `id` FROM `request` WHERE `account_id` = ?";

    public static final String GET_SERV_ID_FROM_RHS = "(SELECT `service_id` FROM `request_has_service` WHERE `request_id` = ?)";
    public static final String GET_CAT_ID_FROM_SERVICE = "(SELECT `category_id` FROM `service` WHERE `id` = ?)";
    public static final String GET_CATEGORY_NAME = "(SELECT `name` FROM `category` WHERE `id` = ?)";

    public static final String GET_MASTER_ID_BY_REQUEST_ID = "(SELECT `account_id` FROM `request_executor` WHERE `request_id` = ?)";

    public static final String GET_DESCRIPTION = "(SELECT `description` FROM `request_has_service` WHERE `request_id` = ?)";

    public static final String INSERT = "INSERT INTO `request` (`price`, `account_id`, `status_id`) VALUES (?, ?, "
	    + GET_STATUS_ID_NEW + ")";
    public static final String INSERT_REQUEST_EXECUTOR = "INSERT INTO `request_executor` (`request_id`, `account_id`, `grade`) VALUES (?, ?, 0)";
    public static final String INSERT_HAS_SERVICE = "INSERT INTO `request_has_service` (`request_id`, `service_id`, `amount`, `price`, `description`) "
	    + "VALUES (?, ?, ?, ?, ?)";

    public static final String UPDATE_STATUS = "UPDATE `request` SET `status_id` = ? WHERE id = ?";
    public static final String UPDATE_MASTER_FOR_REQUEST = "UPDATE `request_executor` SET `account_id` = ? WHERE request_id = ?";
    public static final String UPDATE_PRICE = "UPDATE `request` SET `price` = ? WHERE id = ?";
    public static final String UPDATE_DESCRIPTION = "UPDATE `request_has_service` SET `description` = ? WHERE request_id = ?";
    public static final String UPDATE_PRICE_FOR_REQUEST = "UPDATE `request` SET `price` = ? WHERE id = ?";
    public static final String UPDATE_PRICE_FOR_REQUEST_HAS_SERVICE = "UPDATE `request_has_service` SET `price` = ? WHERE request_id = ?";
    
    public static final String DELETE_MASTER_FOR_REQUEST = "DELETE FROM `request_executor` WHERE request_id = ?";

    // VIEW
    public static final String GET_CATEGORY_FOR_REQUEST = "SELECT `category_name` FROM `category_for_request` WHERE `request_id` = ?";
    public static final String GET_MASTER_NAME_FOR_REQUEST = "SELECT `master_name` FROM `master_for_request_transit` WHERE `request_id` = ?";
    public static final String GET_MASTER_LASTNAME_FOR_REQUEST = "SELECT `master_lastname` FROM `master_for_request_transit` WHERE `request_id` = ?";
    public static final String GET_FEEDBACK_BY_REQUEST_ID = "SELECT `master_for_request_transit`.`master_name`, `master_for_request_transit`.`master_lastname`, `master_for_request_transit`.`master_id`, `master_for_request_transit`.`master_email`"
	    + ", `master_for_request_transit`.`master_phone`, `master_for_request_transit`.`review`, `master_for_request_transit`.`review_time`, `master_for_request_transit`.`request_id`"
	    + ",`account`.`firstname` " + "FROM `master_for_request_transit` "
	    + "INNER JOIN `account` ON `master_for_request_transit`.`account_id` = `account`.`id`"
	    + "WHERE `request_id` = ?";

    public static final String GET_FEEDBACKS_BY_MASTER_ID = "SELECT `review`, `review_time`, `user_name`, `grade` FROM `master_has_feedback` WHERE master_id = ?";
    public static final String GET_REVIEW_BY_ORDER_ID = "SELECT `review` FROM `request_executor` WHERE request_id = ?";
    
    public static final String GET_ORDERS_FOR_MANAGER = "SELECT * FROM `order_has_user` ORDER BY `order_date` DESC";
}
