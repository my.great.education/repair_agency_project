package com.repairagency2.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.repairagency2.entity.field.EntityFieldsUtil;

public class Account implements Entity {
    private static final long serialVersionUID = 1L;
    
	private int id;
	private String login;
	private transient String password;
	private String lastname;
	private String firstname;
	private String email;
	private String phoneNumber;
	private BigDecimal balance;
	private int roleId;
	private int statusId;

	public Account() {
	    this.id = 0;
	    this.balance = new BigDecimal("0").setScale(2, RoundingMode.HALF_DOWN);
	    this.roleId = EntityFieldsUtil.RoleField.DEFAULT_ID;
	    this.statusId = EntityFieldsUtil.StatusField.DEFAULT_ID;
	}

	public Account(int id, String login, String password, String firstname, String lastname, String email,
			String phoneNumber, BigDecimal balance, int roleId, int statusId) {
		this.id = id;
		this.login = login;
		this.password = password;
		this.lastname = lastname;
		this.firstname = firstname;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.balance = balance.setScale(2, RoundingMode.HALF_DOWN);
		this.roleId = roleId;
		this.statusId = statusId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

//	public static Account getNewAccount() {
//		return new Account();
//	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((balance == null) ? 0 : balance.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + id;
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + roleId;
		result = prime * result + statusId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (balance == null) {
			if (other.balance != null)
				return false;
		} else if (!balance.equals(other.balance))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (id != other.id)
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (roleId != other.roleId)
			return false;
		if (statusId != other.statusId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", login=" + login + ", lastname=" + lastname + ", firstname=" + firstname
				+ "phoneNumber=" + phoneNumber + ", email=" + email + ", balance=" + balance + ", roleId=" + roleId + ", statusId=" + statusId + "]";
	}
}
