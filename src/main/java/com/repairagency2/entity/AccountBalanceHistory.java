package com.repairagency2.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

public class AccountBalanceHistory implements Entity {
    private static final long serialVersionUID = 1L;

    private int id;
    private int accountId;
    private BigDecimal payment;
    private LocalDateTime time;
    
    public static AccountBalanceHistory getNewAccountBalanceHistory() {
	return new AccountBalanceHistory();
    }
    
    private AccountBalanceHistory() {
	this.payment = new BigDecimal("0").setScale(2, RoundingMode.HALF_DOWN);
    }
    
    public AccountBalanceHistory(int id, int accountId, BigDecimal payment, LocalDateTime time) {
	this.id = id;
	this.accountId = accountId;
	this.payment = payment;
	this.time = time;
    }
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getAccountId() {
        return accountId;
    }
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }
    public BigDecimal getPayment() {
        return payment;
    }
    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }
    public LocalDateTime getTime() {
        return time;
    }
    public void setTime(LocalDateTime time) {
        this.time = time;
    }
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + accountId;
	result = prime * result + id;
	result = prime * result + ((payment == null) ? 0 : payment.hashCode());
	result = prime * result + ((time == null) ? 0 : time.hashCode());
	return result;
    }
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	AccountBalanceHistory other = (AccountBalanceHistory) obj;
	if (accountId != other.accountId)
	    return false;
	if (id != other.id)
	    return false;
	if (payment == null) {
	    if (other.payment != null)
		return false;
	} else if (!payment.equals(other.payment))
	    return false;
	if (time == null) {
	    if (other.time != null)
		return false;
	} else if (!time.equals(other.time))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "AccountBalanceHistory [id=" + id + ", accountId=" + accountId + ", payment=" + payment + ", time="
		+ time + "]";
    }
}
