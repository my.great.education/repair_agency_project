package com.repairagency2.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class AccountOrderHistory implements Entity {
    private static final long serialVersionUID = 1L;
    
    private int requestId;
    private String status;
    private LocalDateTime date;
    private String category;
    private BigDecimal payment;
    private String master;
    private int masterId;
    private String description;
    private boolean thereReview;
    
    public AccountOrderHistory() {
	this.master = "not assigned";
    }
       
    public boolean isThereReview() {
        return thereReview;
    }

    public void setThereReview(boolean thereReview) {
        this.thereReview = thereReview;
    }

    public int getMasterId() {
        return masterId;
    }

    public void setMasterId(int masterId) {
        this.masterId = masterId;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public int getRequestId() {
        return requestId;
    }
    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public LocalDateTime getDate() {
        return date;
    }
    public void setDate(LocalDateTime date) {
        this.date = date;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public BigDecimal getPayment() {
        return payment;
    }
    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }
    public String getMaster() {
        return master;
    }
    public void setMaster(String master) {
        this.master = master;
    }
    
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((category == null) ? 0 : category.hashCode());
	result = prime * result + ((date == null) ? 0 : date.hashCode());
	result = prime * result + ((description == null) ? 0 : description.hashCode());
	result = prime * result + (thereReview ? 1231 : 1237);
	result = prime * result + ((master == null) ? 0 : master.hashCode());
	result = prime * result + masterId;
	result = prime * result + ((payment == null) ? 0 : payment.hashCode());
	result = prime * result + requestId;
	result = prime * result + ((status == null) ? 0 : status.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	AccountOrderHistory other = (AccountOrderHistory) obj;
	if (category == null) {
	    if (other.category != null)
		return false;
	} else if (!category.equals(other.category))
	    return false;
	if (date == null) {
	    if (other.date != null)
		return false;
	} else if (!date.equals(other.date))
	    return false;
	if (description == null) {
	    if (other.description != null)
		return false;
	} else if (!description.equals(other.description))
	    return false;
	if (thereReview != other.thereReview)
	    return false;
	if (master == null) {
	    if (other.master != null)
		return false;
	} else if (!master.equals(other.master))
	    return false;
	if (masterId != other.masterId)
	    return false;
	if (payment == null) {
	    if (other.payment != null)
		return false;
	} else if (!payment.equals(other.payment))
	    return false;
	if (requestId != other.requestId)
	    return false;
	if (status == null) {
	    if (other.status != null)
		return false;
	} else if (!status.equals(other.status))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "AccountOrderHistory [requestId=" + requestId + ", status=" + status + ", date=" + date + ", category="
		+ category + ", payment=" + payment + ", master=" + master + ", masterId=" + masterId + ", description="
		+ description + ", ifThereReview=" + thereReview + "]";
    }
    
}
