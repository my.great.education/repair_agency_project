package com.repairagency2.entity;

import java.time.LocalDateTime;

public class Feedback implements Entity {
    private static final long serialVersionUID = 1L;
    
    private String firstnameUser;
    private LocalDateTime reviewDate;
    private String review;
    private boolean grade;
    
    
    public boolean isGrade() {
        return grade;
    }
    public void setGrade(boolean grade) {
        this.grade = grade;
    }
    public String getReview() {
        return review;
    }
    public void setReview(String review) {
        this.review = review;
    }
    public LocalDateTime getReviewDate() {
        return reviewDate;
    }
    public void setReviewDate(LocalDateTime reviewDate) {
        this.reviewDate = reviewDate;
    }
    public String getFirstnameUser() {
        return firstnameUser;
    }
    public void setFirstnameUser(String firstnameUser) {
        this.firstnameUser = firstnameUser;
    }
    
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((firstnameUser == null) ? 0 : firstnameUser.hashCode());
	result = prime * result + (grade ? 1231 : 1237);
	result = prime * result + ((review == null) ? 0 : review.hashCode());
	result = prime * result + ((reviewDate == null) ? 0 : reviewDate.hashCode());
	return result;
    }
    
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Feedback other = (Feedback) obj;
	if (firstnameUser == null) {
	    if (other.firstnameUser != null)
		return false;
	} else if (!firstnameUser.equals(other.firstnameUser))
	    return false;
	if (grade != other.grade)
	    return false;
	if (review == null) {
	    if (other.review != null)
		return false;
	} else if (!review.equals(other.review))
	    return false;
	if (reviewDate == null) {
	    if (other.reviewDate != null)
		return false;
	} else if (!reviewDate.equals(other.reviewDate))
	    return false;
	return true;
    }
    
    @Override
    public String toString() {
	return "Feedback [firstnameUser=" + firstnameUser + ", reviewDate=" + reviewDate + ", review=" + review
		+ ", grade=" + grade + "]";
    }
}
