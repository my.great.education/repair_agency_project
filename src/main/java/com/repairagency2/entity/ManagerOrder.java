package com.repairagency2.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ManagerOrder implements Entity {
    private static final long serialVersionUID = 1L;
    
    private String userName;
    private int userId;
    private BigDecimal userBalance;
    private String userPhone;
    private int orderId;
    private BigDecimal price;
    private int amount;
    private LocalDateTime date;
    private String service;
    private String status;
    private int statusId;
    private String statusNext;
    private int statusNextId;
    private int masterId;
    private String masterName;
    private String masterLastname;
    private String description;
    
    
    public int getUserId() {
        return userId;
    }
    public void setUserId(int userId) {
        this.userId = userId;
    }
    public int getStatusId() {
        return statusId;
    }
    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
    public int getStatusNextId() {
        return statusNextId;
    }
    public void setStatusNextId(int statusNextId) {
        this.statusNextId = statusNextId;
    }
    public int getMasterId() {
        return masterId;
    }
    public void setMasterId(int masterId) {
        this.masterId = masterId;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public BigDecimal getUserBalance() {
        return userBalance;
    }
    public void setUserBalance(BigDecimal userBalance) {
        this.userBalance = userBalance;        
    }
    public String getUserPhone() {
        return userPhone;
    }
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }
    public int getOrderId() {
        return orderId;
    }
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
    public BigDecimal getPrice() {
        return price;
    }
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }
    public LocalDateTime getDate() {
        return date;
    }
    public void setDate(LocalDateTime date) {
        this.date = date;
    }
    public String getService() {
        return service;
    }
    public void setService(String service) {
        this.service = service;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getStatusNext() {
        return statusNext;
    }
    public void setStatusNext(String statusNext) {
        this.statusNext = statusNext;
    }
    public String getMasterName() {
        return masterName;
    }
    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }
    public String getMasterLastname() {
        return masterLastname;
    }
    public void setMasterLastname(String masterLastname) {
        this.masterLastname = masterLastname;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + amount;
	result = prime * result + ((date == null) ? 0 : date.hashCode());
	result = prime * result + ((description == null) ? 0 : description.hashCode());
	result = prime * result + masterId;
	result = prime * result + orderId;
	result = prime * result + ((price == null) ? 0 : price.hashCode());
	result = prime * result + ((service == null) ? 0 : service.hashCode());
	result = prime * result + statusId;
	result = prime * result + statusNextId;
	result = prime * result + userId;
	return result;
    }
    
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ManagerOrder other = (ManagerOrder) obj;
	if (amount != other.amount)
	    return false;
	if (date == null) {
	    if (other.date != null)
		return false;
	} else if (!date.equals(other.date))
	    return false;
	if (description == null) {
	    if (other.description != null)
		return false;
	} else if (!description.equals(other.description))
	    return false;
	if (masterId != other.masterId)
	    return false;
	if (orderId != other.orderId)
	    return false;
	if (price == null) {
	    if (other.price != null)
		return false;
	} else if (!price.equals(other.price))
	    return false;
	if (service == null) {
	    if (other.service != null)
		return false;
	} else if (!service.equals(other.service))
	    return false;
	if (statusId != other.statusId)
	    return false;
	if (statusNextId != other.statusNextId)
	    return false;
	if (userId != other.userId)
	    return false;
	return true;
    }
    
    @Override
    public String toString() {
	return "ManagerOrder [userName=" + userName + ", userId=" + userId + ", userBalance=" + userBalance
		+ ", userPhone=" + userPhone + ", orderId=" + orderId + ", price=" + price + ", amount=" + amount
		+ ", date=" + date + ", service=" + service + ", status=" + status + ", statusId=" + statusId
		+ ", statusNext=" + statusNext + ", statusNextId=" + statusNextId + ", masterId=" + masterId
		+ ", masterName=" + masterName + ", masterLastname=" + masterLastname + ", description=" + description
		+ "]";
    }  
}
