package com.repairagency2.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

public class Order implements Entity {
    private static final long serialVersionUID = 1L;

    private int id;
    private BigDecimal price;
    private LocalDateTime createTime;
    private LocalDateTime lastUpdateTime;
    private int accountId;
    private int statusId;

    private Order() {
	this.price = new BigDecimal(1).setScale(2, RoundingMode.HALF_DOWN);
    }

    public Order(int id, BigDecimal price, LocalDateTime createTime, LocalDateTime lastUpdateTime, int accountId,
	    int statusId) {
	this.id = id;
	this.price = price;
	this.createTime = createTime;
	this.lastUpdateTime = lastUpdateTime;
	this.accountId = accountId;
	this.statusId = statusId;
    }

    public static Order getNewOrder() {
	return new Order();
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public BigDecimal getPrice() {
	return price;
    }

    public void setPrice(BigDecimal price) {
	this.price = price;
    }

    public LocalDateTime getCreateTime() {
	return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
	this.createTime = createTime;
    }

    public LocalDateTime getLastUpdateTime() {
	return lastUpdateTime;
    }

    public void setLastUpdateTime(LocalDateTime lastUpdateTime) {
	this.lastUpdateTime = lastUpdateTime;
    }

    public int getAccountId() {
	return accountId;
    }

    public void setAccountId(int accountId) {
	this.accountId = accountId;
    }

    public int getStatusId() {
	return statusId;
    }

    public void setStatusId(int statusId) {
	this.statusId = statusId;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + accountId;
	result = prime * result + id;
	result = prime * result + statusId;
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Order other = (Order) obj;
	if (accountId != other.accountId)
	    return false;
	if (id != other.id)
	    return false;
	if (statusId != other.statusId)
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "Request [id=" + id + ", price=" + price + ", createTime=" + createTime + ", lastUpdateTime="
		+ lastUpdateTime + ", accountId=" + accountId + ", statusId=" + statusId + "]";
    }
}
