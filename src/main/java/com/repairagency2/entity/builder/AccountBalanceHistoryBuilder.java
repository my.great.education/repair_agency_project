package com.repairagency2.entity.builder;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import com.repairagency2.entity.AccountBalanceHistory;
import com.repairagency2.entity.field.EntityFieldsUtil.AccountBalanceHistoryFiled;

public class AccountBalanceHistoryBuilder {

    public static AccountBalanceHistory build(ResultSet rs) throws SQLException {
	AccountBalanceHistory result = null;
	
	int id = rs.getInt(AccountBalanceHistoryFiled.ID);
	int accountId = rs.getInt(AccountBalanceHistoryFiled.ACCOUNT_ID);
	BigDecimal payment = rs.getBigDecimal(AccountBalanceHistoryFiled.PPAYMENT);
	Timestamp time = rs.getTimestamp(AccountBalanceHistoryFiled.TIME);
	LocalDateTime lTime = convertToLocalDateTimeViaInstant(time);
	
	result = AccountBalanceHistory.getNewAccountBalanceHistory();
	result.setId(id);
	result.setAccountId(accountId);
	result.setPayment(payment);
	result.setTime(lTime);
	
	return result;
    }

    private static LocalDateTime convertToLocalDateTimeViaInstant(Timestamp dateToConvert) {
	LocalDateTime timeInUTC = dateToConvert.toLocalDateTime();
	return timeInUTC;
    }
}
