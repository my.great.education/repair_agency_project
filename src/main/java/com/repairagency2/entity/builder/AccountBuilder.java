package com.repairagency2.entity.builder;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import com.repairagency2.entity.Account;
import com.repairagency2.entity.field.EntityFieldsUtil.AccountField;
import com.repairagency2.entity.field.RequestParamsUtil.RegisterParams;

public class AccountBuilder  {

    public Account build(ResultSet rs) throws SQLException {
	Account account = null;

	int id = rs.getInt(AccountField.ID);
	String login = rs.getString(AccountField.LOGIN);
	String pass = rs.getString(AccountField.PASSWORD);
	String firstName = rs.getString(AccountField.FIRSTNAME);
	String lastName = rs.getString(AccountField.LASTNAME);
	String email = rs.getString(AccountField.EMAIL);
	String phoneNumber = rs.getString(AccountField.PHONE_NUMBER);
	BigDecimal balance = rs.getBigDecimal(AccountField.BALANCE);
	int role_id = rs.getInt(AccountField.ROLE_ID);
	int status_id = rs.getInt(AccountField.STATUS_ID);

	account = new Account(id, login, pass, firstName, lastName, email, phoneNumber, balance, role_id, status_id);

	return account;
    }
    
    public Account registerBuild(HttpServletRequest req, Account account) {
	String login = req.getParameter(RegisterParams.LOGIN);
	String pass = req.getParameter(RegisterParams.PASSWORD);
	String firstName = req.getParameter(RegisterParams.FIRSTNAME);
	String lastName = req.getParameter(RegisterParams.LASTNAME);
	String email = req.getParameter(RegisterParams.EMAIL);
	String phoneNumber = req.getParameter(RegisterParams.PHONE_NUMBER).replaceAll("[\\s()-]+", "");
	
	account.setLogin(login);
	account.setPassword(pass);
	account.setFirstname(firstName);
	account.setLastname(lastName);
	account.setEmail(email);
	account.setPhoneNumber(phoneNumber);
	
	return account;
    }
}
