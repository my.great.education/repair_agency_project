package com.repairagency2.entity.builder;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import com.repairagency2.entity.Feedback;
import com.repairagency2.entity.ManagerOrder;
import com.repairagency2.entity.Order;
import com.repairagency2.entity.field.EntityFieldsUtil.FeedbackField;
import com.repairagency2.entity.field.EntityFieldsUtil.ManagerOrdersFiled;
import com.repairagency2.entity.field.OrderFieldsUtil.OrderField;

public class OrderBuilder {

    public static Order build(ResultSet rs) throws SQLException {
	Order order = Order.getNewOrder();

	int id = rs.getInt(OrderField.ID);
	BigDecimal price = rs.getBigDecimal(OrderField.PRICE);
	Timestamp createTime = rs.getTimestamp(OrderField.CREATE_TIME);
	Timestamp updateTime = rs.getTimestamp(OrderField.LAST_UPDATE_TIME);
	int accountId = rs.getInt(OrderField.ACCOUNT_ID);
	int statusId = rs.getInt(OrderField.STATUS_ID);

	order.setId(id);
	order.setPrice(price);
	order.setCreateTime(convertToLocalDateTime(createTime));
	order.setLastUpdateTime(convertToLocalDateTime(updateTime));
	order.setAccountId(accountId);
	order.setStatusId(statusId);

	return order;
    }

    public static Feedback buildFeedback(ResultSet rs) throws SQLException {
	Feedback feedback = new Feedback();
	
	feedback.setReview(rs.getString(FeedbackField.REVIEW));
	feedback.setFirstnameUser(rs.getString(FeedbackField.USER_NAME));
	feedback.setGrade(rs.getBoolean(FeedbackField.GRADE));
	
	Timestamp reciewTimestamp = rs.getTimestamp(FeedbackField.REVIEW_TIME);
	if (reciewTimestamp != null) {
	    feedback.setReviewDate(reciewTimestamp.toLocalDateTime());
	}
	
	return feedback;
    }

    private static LocalDateTime convertToLocalDateTime(Timestamp dateToConvert) {
	LocalDateTime timeInUTC = dateToConvert.toLocalDateTime();
	return timeInUTC;
    }

    public static ManagerOrder buildOrderForManager(ResultSet rs) throws SQLException {
	ManagerOrder order = new ManagerOrder();
	Timestamp orderDate = rs.getTimestamp(ManagerOrdersFiled.ORDER_DATE);

	order.setUserName(rs.getString(ManagerOrdersFiled.USER_NAME));
	order.setUserId(rs.getInt(ManagerOrdersFiled.USER_ID));
	order.setUserBalance(rs.getBigDecimal(ManagerOrdersFiled.USER_BALANCE));
	order.setUserPhone(rs.getString(ManagerOrdersFiled.USER_PHONE));
	order.setOrderId(rs.getInt(ManagerOrdersFiled.REQUEST_ID));
	order.setPrice(rs.getBigDecimal(ManagerOrdersFiled.PRICE));
	order.setDate(convertToLocalDateTime(orderDate));
	order.setStatus(rs.getString(ManagerOrdersFiled.STATUS));
	order.setStatusId(rs.getInt(ManagerOrdersFiled.STATUS_ID));
	order.setAmount(rs.getInt(ManagerOrdersFiled.AMOUNT));
	order.setDescription(rs.getString(ManagerOrdersFiled.ORDER_BODY));
	order.setService(rs.getString(ManagerOrdersFiled.SERVICE));
	order.setMasterId(rs.getInt(ManagerOrdersFiled.MASTER_ID));
	
	return order;
    }
}
