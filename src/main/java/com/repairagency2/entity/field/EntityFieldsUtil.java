package com.repairagency2.entity.field;

public class EntityFieldsUtil {
	
	public static class AccountField {
		public static final String ID = "id";
		public static final String LOGIN = "login";
		public static final String PASSWORD = "pass";
		public static final String LASTNAME = "lastname";
		public static final String FIRSTNAME = "firstname";
		public static final String EMAIL = "email";
		public static final String PHONE_NUMBER = "phone_number";
		public static final String BALANCE = "balance";
		public static final String STATUS_ID = "status_id";
		public static final String ROLE_ID = "role_id";
	}
	
	public static class AccountBalanceHistoryFiled {
		public static final String ID = "id";
		public static final String ACCOUNT_ID = "account_id";
		public static final String PPAYMENT = "payment";
		public static final String TIME = "time";
		public static final String SERIAL = "serial";
	}
	
	public static class RoleField {
		public static final String ID = "id";
		public static final String NAME = "name";
		public static final String ACCESS_LEVEL = "access_level";
		public static final int DEFAULT_ID = 4;
	}
	
	public static class StatusField {
	    public static final String ID = "id";
	    public static final String STATUS = "status";
	    public static final int DEFAULT_ID = 1;
	}
	
	public static class FeedbackField {
	    public static final String REVIEW = "review";
	    public static final String REVIEW_TIME = "review_time";
	    public static final String USER_NAME = "user_name";
	    public static final String GRADE = "grade";
	}
	
	public static class ManagerOrdersFiled {
	    public static final String USER_NAME = "user_name";
	    public static final String USER_ID = "user_id";
	    public static final String USER_BALANCE = "user_balance";
	    public static final String USER_PHONE = "user_phone";
	    public static final String REQUEST_ID = "request_id";
	    public static final String PRICE = "price";
	    public static final String ORDER_DATE = "order_date";
	    public static final String STATUS = "status";
	    public static final String STATUS_ID = "status_id";
	    public static final String AMOUNT = "amount";
	    public static final String ORDER_BODY = "order_body";
	    public static final String SERVICE = "service";
	    public static final String MASTER_ID = "master_id";
	    public static final String MASTER_NAME = "master_name";
	    public static final String MASTER_LASTNAME = "master_lastname";
	}
}
