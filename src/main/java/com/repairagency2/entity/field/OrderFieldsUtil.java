package com.repairagency2.entity.field;

public class OrderFieldsUtil {

    public static class OrderField {
	public static final String ID = "id";
	public static final String PRICE = "price";
	public static final String CREATE_TIME = "create_time";
	public static final String LAST_UPDATE_TIME = "last_update_time";
	public static final String ACCOUNT_ID = "account_id";
	public static final String STATUS_ID = "status_id";
    }

    public static class StatusField {
	public static final String ID = "id";
	public static final String CONDITION = "condition";
    }

    public static class StatusFlowField {
	public static final String ID = "id";
	public static final String FROM = "from";
	public static final String TO = "to";
    }
}
