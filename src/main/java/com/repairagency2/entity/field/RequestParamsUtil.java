package com.repairagency2.entity.field;

public class RequestParamsUtil {

    public static class NoticeParams {
	public static final String ERROR = "error";
	public static final String SUCCESS = "success";
    }

    public static class RegisterParams {
	public static final String PHONE_NUMBER = "regPhoneNumber";
	public static final String FIRSTNAME = "regFistname";
	public static final String LASTNAME = "regLastname";
	public static final String EMAIL = "regEmail";
	public static final String LOGIN = "regLogin";
	public static final String PASSWORD = "regPassword";
	public static final String RP_PASSWORD = "RPregPassword";
    }

    public static class LoginParams {
	public static final String LOGIN = "login";
	public static final String PASSWORD = "password";
    }

    public static class ToUpBalanceParams {
	public static final String TO_UP_BALANCE = "upBalance";
    }

    public static class ChangeProfileParams {
	public static final String FIRSTNAME = "changeFistname";
	public static final String LASTNAME = "changeLastname";
	public static final String EMAIL = "changeEmail";
    }

    public static class ChangePasswordParams {
	public static final String OLD_PASSWORD = "changeOldPassword";
	public static final String NEW_PASSWORD = "changeNewPassword";
	public static final String RP_NEW_PASSWORD = "changeNewPasswordRP";
    }

    public static class CustomOrderParams {
	public static final String TEXT = "orderText";
	public static final String SRC_TEXT = "srcText";
	public static final String ORDER_ID = "orderId";
    }

    public static class UpdateOrderStatusParams {
	public static final String STATE = "state";
	public static final String ORDER_ID = "requestId";
    }

    public static class PayOrderParams {
	public static final String PAYMENT = "payment";
	public static final String ORDER_ID = "requestId";
    }

    public static class FeedbackParams {
	public static final String MASTER_ID = "id";
	public static final String FEEDBACK_FLAG = "flagReview";
	public static final String FEEDBACK_LIST = "feedbackList";
	
	public static final String REQUEST_ID = "requestId";
	public static final String REVIEW_TEXT = "reviewText";
	public static final String GRADE = "grade";

    }
    
    public static class ChangePriceParams {
	public static final String ORDER_ID = "orderId";
	public static final String SUM = "sum";
    }
    
    public static class ChangeMasterParams {
	public static final String ORDER_ID = "orderId";
	public static final String MASTER_ID = "masterId";
	public static final String CURRENT_MASTER_ID = "currentMasterId";
	public static final String CHANGE_FLAG = "changeFlag";
    }
    
    public static class TopUpBalanceManagerParams {
	public static final String USER_ID = "userId";
	public static final String SUM = "sum";
    }
    
    public static class UpdateStatusParams {
	public static final String ORDER_ID = "orderId";
	public static final String STATUS_ID_NEW = "statusIdNew";
	public static final String CURRENT_STATUS_ID = "currentStatusId";
    }
}
