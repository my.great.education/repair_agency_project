package com.repairagency2.filter;

import java.io.IOException;
import java.net.http.HttpRequest;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.*;

import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.AccountService;
import com.repairagency2.entity.Account;

@WebFilter("/AuthenticationFilter")
public class AuthenticationFilter implements Filter {
    private FilterConfig config;
    private ServletContext context;

    public void init(FilterConfig config) throws ServletException {
	this.config = config;
	this.context = config.getServletContext();
	this.context.log("AuthenticationFilter initialized");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
	    throws IOException, ServletException {
	boolean isActive = config.getInitParameter("active").equalsIgnoreCase("true");
	if (!isActive) {
	    chain.doFilter(request, response);
	    return;
	}
	HttpServletRequest req = (HttpServletRequest) request;
	HttpServletResponse res = (HttpServletResponse) response;
	HttpSession session = req.getSession(false);

	String uri = req.getRequestURI();
	String startPageURI = req.getContextPath() + ConfManager.getProperty("path.page.index");
	String controllerURI = req.getContextPath() + ConfManager.getProperty("path.controller");
	String noticeServletURI = req.getContextPath() + ConfManager.getProperty("path.notice");
	String noticeServletURL = ConfManager.getProperty("path.notice");
	String noticePageURI = req.getContextPath() + ConfManager.getProperty("path.page.notice");
	String logoPageURI = req.getContextPath() + ConfManager.getProperty("path.logo");

//	if (session != null && !uri.equals(noticeServletURI) && !uri.equals(noticePageURI)
//		&& !uri.equals(logoPageURI)) {
//	    clearSessionFromNoticeAttr(session);
//	}

	this.context.log("Requested Resource::" + uri);

	if (session != null && req.getParameter("logout") != null) {
	    this.context.log("Logout user::" + ((Account) session.getAttribute("user")).getLogin());
//	    clearSessionFromLoginAttr(session);
	    session.invalidate();
	    res.sendRedirect(startPageURI);
	    return;
	}
	
	boolean isLoggedIn = (session != null) && (session.getAttribute("user") != null);
	boolean isStartPageRequest = uri.equals(startPageURI) || uri.equals(req.getContextPath() + "/");
	boolean isControllerRequest = uri.equals(controllerURI);
	boolean isNoticeRequest = uri.equals(noticeServletURI);
	boolean isImg = uri.endsWith(".jpeg") || uri.endsWith(".png") || uri.endsWith(".jpg") || uri.endsWith(".svg");
	boolean isJSPF = uri.endsWith(".jspf");
	boolean isJS = uri.endsWith(".js") || uri.endsWith(".css");
	
	if (!isLoggedIn && !isControllerRequest && !isNoticeRequest
		&& !(isImg || isJSPF || isStartPageRequest || isJS)) {
	    this.context.log("Unauthorized access request");
	    request.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
	    request.getRequestDispatcher(noticeServletURL).forward(request, response);
	} else {
	    chain.doFilter(request, response);
	}
    }

//    private void clearSessionFromNoticeAttr(HttpSession session) {
//	session.removeAttribute("error");
//	session.removeAttribute("success");
//    }
}
