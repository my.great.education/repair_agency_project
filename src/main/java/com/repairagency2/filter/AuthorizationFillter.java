package com.repairagency2.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.entity.Account;

@WebFilter("/AuthorizationFillter")
public class AuthorizationFillter implements Filter {
    private FilterConfig config;
    private ServletContext context;

    public void init(FilterConfig config) throws ServletException {
	this.config = config;
	this.context = config.getServletContext();
	this.context.log("AuthorizationFillter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
	    throws IOException, ServletException {
	boolean isActive = config.getInitParameter("active").equalsIgnoreCase("true");
	if (!isActive) {
	    chain.doFilter(request, response);
	    return;
	}

	HttpServletRequest req = (HttpServletRequest) request;
	HttpServletResponse res = (HttpServletResponse) response;
	HttpSession session = req.getSession(false);
	if (session == null) {
	    chain.doFilter(request, response);
	    return;
	}
	
	Account account = (Account) session.getAttribute("user");
	String uri = req.getRequestURI();
	String contPath = req.getContextPath();
	boolean isImg = uri.endsWith(".jpeg") || uri.endsWith(".png") || uri.endsWith(".jpg") || uri.endsWith(".svg");
	boolean isJSPF = uri.endsWith(".jspf");
	boolean isJS = uri.endsWith(".js") || uri.endsWith(".css");
	int roleId = 0;

	if (account != null) {
	    roleId = account.getRoleId();
	    switch (roleId) {
	    case 2:
		if (!isShare(uri, contPath) && !uri.startsWith(contPath + "/page/manager/")
			&& !(isImg || isJSPF || isJS)) {
		    String page = ConfManager.getProperty("path.notice");
		    req.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
		    req.getRequestDispatcher(page).forward(req, res);
		    return;
		}
		break;
	    case 4:
		if (!isShare(uri, contPath) && !uri.startsWith(contPath + "/page/account/")
			&& !(isImg || isJSPF || isJS)) {
		    String page = ConfManager.getProperty("path.notice");
		    req.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
		    req.getRequestDispatcher(page).forward(req, res);
		    return;
		}
		break;
	    }
	}
	chain.doFilter(request, response);
    }

    private boolean isShare(String uri, String contPath) {
	return uri.equals(contPath + ConfManager.getProperty("path.page.index")) 
		|| uri.equals(contPath + ConfManager.getProperty("path.notice")) 
		|| uri.equals(contPath + ConfManager.getProperty("path.page.notice"))
		|| uri.equals(contPath + "/")
		|| uri.contains(contPath + "/controller");
    }
}
