package com.repairagency2.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EncodingFilter extends HttpFilter {
	private static final long serialVersionUID = 1L;
	private String encoding;
	private FilterConfig config;
	
	@Override
	public void init(FilterConfig config) throws ServletException {
		this.config = config;
		encoding = config.getInitParameter("encoding");
	}
	
	@Override
	public void destroy() {
		encoding = null;
		config = null;
	}
	
	@Override
	protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		boolean isActive = config.getInitParameter("active").equalsIgnoreCase("true");
		if (isActive) {
			req.setCharacterEncoding(encoding);
			res.setCharacterEncoding(encoding);
		}
		chain.doFilter(req, res);
	}
}
