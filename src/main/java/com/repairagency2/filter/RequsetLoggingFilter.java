package com.repairagency2.filter;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.*;
import javax.servlet.http.*;

public class RequsetLoggingFilter implements Filter {
    private FilterConfig config;
    private ServletContext context;

    @Override
    public void init(FilterConfig config) throws ServletException {
	this.config = config;
	this.context = config.getServletContext();
	this.context.log("RequestLoggingFilter initialized");

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
	    throws ServletException, IOException {
	boolean isActive = config.getInitParameter("active").equalsIgnoreCase("true");
	if (!isActive) {
	    chain.doFilter(request, response);
	    return;
	}

	HttpServletRequest req = (HttpServletRequest) request;
	Enumeration<String> params = req.getParameterNames();
	
	while (params.hasMoreElements()) {
	    String name = params.nextElement();
	    String value = request.getParameter(name);
	    this.context.log(req.getRemoteAddr() + "::Request Params::{" + name + "=" + value + "}");
	}

	Cookie[] cookies = req.getCookies();
	if (cookies != null) {
	    for (Cookie cookie : cookies) {
		this.context
			.log(req.getRemoteAddr() + "::Cookie::{" + cookie.getName() + "," + cookie.getValue() + "}");
	    }
	}
	chain.doFilter(request, response);
    }
}
