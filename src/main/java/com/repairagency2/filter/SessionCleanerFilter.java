package com.repairagency2.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.entity.field.RequestParamsUtil;

@WebFilter("/SessionCleanerFilter")
public class SessionCleanerFilter implements Filter {
    private FilterConfig config;
    private ServletContext context;

    public void init(FilterConfig config) throws ServletException {
	this.config = config;
	this.context = config.getServletContext();
	this.context.log("SessionCleanerFilter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
	    throws IOException, ServletException {
	boolean isActive = config.getInitParameter("active").equalsIgnoreCase("true");
	if (!isActive) {
	    chain.doFilter(request, response);
	    return;
	}
	
	HttpServletRequest req = (HttpServletRequest) request;
	HttpServletResponse res = (HttpServletResponse) response;
	HttpSession session = req.getSession(false);

	String uri = req.getRequestURI();
	String startPageURI = req.getContextPath() + ConfManager.getProperty("path.page.index");
	String controllerURI = req.getContextPath() + ConfManager.getProperty("path.controller");
	String noticeServletURI = req.getContextPath() + ConfManager.getProperty("path.notice");
	String noticeServletURL = ConfManager.getProperty("path.notice");
	String noticePageURI = req.getContextPath() + ConfManager.getProperty("path.page.notice");
	String logoPageURI = req.getContextPath() + ConfManager.getProperty("path.logo");
	String feedbackPageURI = req.getContextPath() + ConfManager.getProperty("path.page.feedback");
	String feedbackCommandURI = req.getContextPath() + ConfManager.getProperty("path.controller.orderHistory");
	
	if (session != null && !uri.equals(noticeServletURI) && !uri.equals(noticePageURI)
		&& !uri.equals(logoPageURI)) {
	    clearNoticeAttr(session);
	}
		
	chain.doFilter(request, response);
    }
    
    private void clearNoticeAttr(HttpSession session) {
	session.removeAttribute(RequestParamsUtil.NoticeParams.ERROR);
	session.removeAttribute(RequestParamsUtil.NoticeParams.SUCCESS);
    }
    
    private void clearFeedbackAttr(HttpSession session) {
	session.removeAttribute(RequestParamsUtil.FeedbackParams.FEEDBACK_FLAG);
    }
}
