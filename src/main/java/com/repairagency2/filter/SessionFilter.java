package com.repairagency2.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.repairagency2.command.manager.ConfManager;
import com.repairagency2.command.manager.MessageManager;
import com.repairagency2.db.service.AccountService;
import com.repairagency2.entity.Account;
import com.repairagency2.entity.AccountOrderHistory;
import com.repairagency2.exception.DAOException;
import com.repairagency2.exception.DBException;

@WebFilter("/sessionFilter")
public class SessionFilter implements Filter {
    private FilterConfig config;
    private ServletContext context;
    private static Set<String> userPages = new HashSet<>();    
    
    static {
	userPages.add("/RepairAgency" + ConfManager.getProperty("path.page.orders"));
	userPages.add("/RepairAgency" + ConfManager.getProperty("path.page.acc.paymentHistory"));
    }

    public void init(FilterConfig config) throws ServletException {
	this.config = config;
	this.context = config.getServletContext();
	this.context.log("AuthenticationFilter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
	    throws IOException, ServletException {
	boolean isActive = config.getInitParameter("active").equalsIgnoreCase("true");
	if (!isActive) {
	    chain.doFilter(request, response);
	    return;
	}

	HttpServletRequest req = (HttpServletRequest) request;
	HttpServletResponse resp = (HttpServletResponse) response;
	HttpSession session = req.getSession(false);
	String noticeServletURL = ConfManager.getProperty("path.notice");
	boolean isLoggedIn = (session != null) && (session.getAttribute("user") != null);
	
	
	if (isLoggedIn) {
	    AccountService as = new AccountService();
	    Account currentAcc = (Account) session.getAttribute("user");
	    
	    try {
		Account actualAcc = as.find(currentAcc.getId());
		session.setAttribute("user", actualAcc);
	    } catch (DAOException e) {
		// log
		e.printStackTrace();
		if (session != null) {
		    session.invalidate();
		}
		req.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
		req.getRequestDispatcher(noticeServletURL).forward(request, response);
	    } catch (DBException e) {
		// log
		e.printStackTrace();
		if (session != null) {
		    session.invalidate();
		}
		req.setAttribute("error", MessageManager.getProperty("message.error.nullpage"));
		req.getRequestDispatcher(noticeServletURL).forward(request, response);
	    }
	}

	chain.doFilter(request, response);
    }
}
