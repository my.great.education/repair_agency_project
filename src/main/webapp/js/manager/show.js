var cash = [];

function showOrderInfo(i) {	
	var edit = document.getElementById("orderInfo" + i);
	
	if (cash.length == 1) {
		var first = cash[0];
		first.style.display = "none";
		cash.push(edit);
		cash.shift();
	} else {
		cash.push(edit);
	}
	
	edit.style.display = "";
}

function unShowOrderInfo(i) {
	var edit = document.getElementById("orderInfo" + i);

	edit.style.display = "none";

}

function showTopUp(i) {	
	var input = document.getElementById("topUp" + i);

	input.style.display = "";
}

function unShowTopUp(i) {
	var input = document.getElementById("topUp" + i);
	
	document.getElementById("topUpMsg" + i).innerHTML = "";
	input.style.display = "none";
}

function showChangePrice(i) {
	var form = document.getElementById("changePrice" + i);
	var price = document.getElementById("price" + i);
	
	form.style.display = "";
	price.style.display = "none";
}

function unShowChangePrice(i, inputPrice) {
	var form = document.getElementById("changePrice" + i);
	var price = document.getElementById("price" + i);
	var mess = document.getElementById("changePriceMsg" + i);
	
	$("#sumValue" + i).val(inputPrice);
	mess.innerHTML = "";
	form.style.display = "none";
	price.style.display = "";
}

function showChangeMaster(i) {
	var form = document.getElementById("changeMaster" + i);
	var master = document.getElementById("master" + i);
	
	form.style.display = "";
	master.style.display = "none";
}

function unShowChangeMaster(i, currentId) {
	var form = document.getElementById("changeMaster" + i);
	var master = document.getElementById("master" + i);
	var selected = document.getElementById("masterValue" + i);
	
	selected.value = currentId;
	form.style.display = "none";
	master.style.display = "";
}

function showStatus(i) {
	var form = document.getElementById("changeStatus" + i);
	var status = document.getElementById("status" + i);
	
	form.style.display = "";
	status.style.display = "none";
}

function unShowChangeStatus(i, inputStat) {
	var val = inputStat;
	var form = document.getElementById("changeStatus" + i);
	var status = document.getElementById("status" + i);
	var mess = document.getElementById("changeStatusMsg" + i);
	
	$("#statusVal" + i).val(val);
	mess.innerHTML = "";
	form.style.display = "none";
	status.style.display = "";
}