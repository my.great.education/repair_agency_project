function validateChangePrice(i, current) {
	const value = $("#sumValue" + i).val();
	document.getElementById("changePriceMsg" + i).innerHTML = "";

	if (value == "") {
		document.getElementById("changePriceMsg" + i).innerHTML = "**Enter payment!";
		return false;
	} else {
		document.getElementById("changePriceMsg" + i).innerHTML = "";
	}
	if (parseFloat(value) <= 1 || Number.isNaN(parseFloat(value))) {
		document.getElementById("changePriceMsg" + i).innerHTML = "**Fill correct payment!";
		return false;
	} 
	if (value == current) {
		unShowChangePrice(i);
		document.getElementById("changePriceMsg" + i).innerHTML = "";
		$("#sumValue" + i).val(current);
		return false;
	} else {
		document.getElementById("changePriceMsg" + i).innerHTML = "";
		return true;
	}
}

function validateTopUp(i) {
	const value = $("#topUpVal" + i).val();

	if (value == "") {
		document.getElementById("topUpMsg" + i).innerHTML = "**Enter replenishment amount!";
		return false;
	} else {
		document.getElementById("topUpMsg" + i).innerHTML = "";
	}
	if (parseFloat(value) <= 0 || Number.isNaN(parseFloat(value))) {
		document.getElementById("topUpMsg" + i).innerHTML = "**Fill correct value!";
		return false;
	} else {
		document.getElementById("topUpMsg" + i).innerHTML = "";
		return true;
	}
}

function changeMaster(i, cMasterId) {
	const value = $("#masterValue" + i).val();
	
	if (value == 0 && value == cMasterId) {
		return false;
	}
	if (cMasterId == value) {
		unShowChangeMaster(i, cMasterId);
		document.getElementById("changeMasterMsg" + i).innerHTML = "";
		return false;
	} else {
		document.getElementById("changeMasterMsg" + i).innerHTML = "";
		return true;
	}
}

function changeStatus(i, cStatusId) {
	const value = $("#statusVal" + i).val();
	
	if (cStatusId == value) {
		unShowChangeStatus(i, cStatusId);
		document.getElementById("changeStatusMsg" + i).innerHTML = "";
		return false;
	} else {
		document.getElementById("changeStatusMsg" + i).innerHTML = "";
		return true;
	}
}