function show(i) {
	var edit = document.getElementById("editDescription" + i);
	var desc = document.getElementById("description" + i);
	var editBtn = document.getElementById("editBtn" + i);
	const length = $("#textArea" + i).val().length; 
	const text = $("#textArea" + i).val();
	
	$("#textArea" + i).val(text);
	document.getElementById("textArea" + i + "a").innerHTML = length;
	
	edit.style.display = "block";
	desc.style.display = "none";
	editBtn.style.display = "none";

}

function unShow(i, text) {
	var edit = document.getElementById("editDescription" + i);
	var desc = document.getElementById("description" + i);
	var editBtn = document.getElementById("editBtn" + i);

	$("#textArea" + i).val(text);
	edit.style.display = "none";
	desc.style.display = "block";
	editBtn.style.display = "block";
}