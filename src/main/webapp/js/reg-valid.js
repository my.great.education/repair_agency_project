$('input').on('input invalid', function() {
	this.setCustomValidity('')
	if (this.validity.valueMissing) {
		this.setCustomValidity("Required field")
	}
	if (this.validity.typeMismatch) {
		this.setCustomValidity("Doesn't match type")
	}
	if (this.validity.patternMismatch) {
		this.setCustomValidity("Incorrect data format")
	}
})

$('textarea').on('input', function() {
	const currentLength = $(this).val().length;
	const id = $(this).attr(`id`);

	document.getElementById(id + "a").innerHTML = currentLength;
});

function resetTextArea() {
	document.getElementById("textArea0a").innerHTML = 0;
};

function validateRegister() {
	var phone = $("#validationRegPhoneNumber").val();
	var name1 = $("#validationRegFirstname").val();
	var name2 = $("#validationRegLastname").val();
	var pw1 = $("#validationRegPassword").val();
	var pw2 = $("#RPvalidationRegPassword").val();
	var email = $("#validationRegEmail").val();
	var login = $("#validationRegLogin").val();

	if (phone == "") {
		document.getElementById("phoneMsg").innerHTML = "**Fill the first name";
		return false;
	} else {
		document.getElementById("phoneMsg").innerHTML = "";
	}
	if (name1 == "") {
		document.getElementById("blankMsg").innerHTML = "**Fill the first name";
		return false;
	} else {
		document.getElementById("blankMsg").innerHTML = "";
	}
	if (name2 == "") {
		document.getElementById("blankMsg2").innerHTML = "**Fill the last name";
		return false;
	} else {
		document.getElementById("blankMsg2").innerHTML = "";
	}
	if (email == "") {
		document.getElementById("messageEmail").innerHTML = "**Fill the email please!";
		return false;
	} else {
		document.getElementById("messageEmail").innerHTML = "";
	}
	if (login == "") {
		document.getElementById("messageLogin").innerHTML = "**Fill the login please!";
		return false;
	} else {
		document.getElementById("messageLogin").innerHTML = "";
	}
	if (pw1 == "") {
		document.getElementById("message1").innerHTML = "**Fill the password please!";
		return false;
	} else {
		document.getElementById("message1").innerHTML = "";
	}
	if (pw2 == "") {
		document.getElementById("message2").innerHTML = "**Enter the password please!";
		return false;
	} else {
		document.getElementById("message2").innerHTML = "";
	}
	if (pw1.length < 5) {
		document.getElementById("message1").innerHTML = "**Password length must be atleast 5 characters";
		return false;
	} else {
		document.getElementById("message1").innerHTML = "";
	}
	if (pw1.length > 8) {
		document.getElementById("message1").innerHTML = "**Password length must not exceed 8 characters";
		return false;
	} else {
		document.getElementById("message1").innerHTML = "";
	}
	if (pw1 != pw2) {
		document.getElementById("message2").innerHTML = "**Passwords are not same";
		return false;
	} else {
		return true;
	}
}

function validateLogin() {
	const login = $("#validationLogin").val();
	const pw = $("#validationPassword").val();
	const check = document.querySelector('#invalidCheck');

	if (login == "") {
		document.getElementById("loginMsg").innerHTML = "**Fill the login please!";
		return false;
	} else {
		document.getElementById("loginMsg").innerHTML = "";
	}
	if (pw == "") {
		document.getElementById("pwMsg").innerHTML = "**Fill the password please!";
		return false;
	} else {
		document.getElementById("pwMsg").innerHTML = "";
	}
	if (check.checked) {
		document.getElementById("cMsg").innerHTML = "";
		return true;
	} else {
		document.getElementById("cMsg").innerHTML = "**Check please!";
		return false;
	}
}

function validateChangeProfile() {
	const login = $("#validationRegFirstname").val();
	const pw = $("#validationRegLastname").val();
	const email = $("#validationRegEmail").val();

	if (login == "") {
		document.getElementById("blankMsg").innerHTML = "**Fill the first name";
		return false;
	} else {
		document.getElementById("blankMsg").innerHTML = "";
	}
	if (pw == "") {
		document.getElementById("blankMsg2").innerHTML = "**Fill the last name";
		return false;
	} else {
		document.getElementById("blankMsg2").innerHTML = "";
	}
	if (email == "") {
		document.getElementById("messageEmail").innerHTML = "**Fill the email please!";
		return false;
	} else {
		document.getElementById("messageEmail").innerHTML = "";
	}
}

function validateChangePassword() {
	const pw = $("#validationChangeOldPass").val();
	const pw1 = $("#validationChangeNewPass").val();
	const pw2 = $("#validationChangeNewPassRP").val();

	if (pw == "") {
		document.getElementById("messageOldPw").innerHTML = "**Fill the password please!";
		return false;
	} else {
		document.getElementById("messageOldPw").innerHTML = "";
	}
	if (pw1 == "") {
		document.getElementById("messageNewPw").innerHTML = "**Fill the new password please!";
		return false;
	} else {
		document.getElementById("messageNewPw").innerHTML = "";
	}
	if (pw2 == "") {
		document.getElementById("messageNewPwRP").innerHTML = "**Enter the new password please!";
		return false;
	} else {
		document.getElementById("messageNewPwRP").innerHTML = "";
	}
	if (pw1.length < 5) {
		document.getElementById("messageNewPw").innerHTML = "**New password length must be atleast 5 characters";
		return false;
	}
	if (pw1.length > 8) {
		document.getElementById("messageNewPw").innerHTML = "**New password length must not exceed 8 characters";
		return false;
	}
	if (pw1 != pw2) {
		document.getElementById("messageNewPwRP").innerHTML = "**Passwords are not same";
		return false;
	} else {
		return true;
	}
}

function validateToUpbalance() {
	var value = $("#topUpBalance").val();

	if (value == "") {
		document.getElementById("messageBalance").innerHTML = "**Enter payment!";
		return false;
	} else {
		document.getElementById("messageBalance").innerHTML = "";
	}
	if (value <= 0) {
		document.getElementById("messageBalance").innerHTML = "**Fill correct payment!";
		return false;
	} else {
		return true;
	}
}

function validateCustomOrder(i, srcText) {
	var text = $("#textArea" + i);
	var len = text.val().trim().length;

	if (len < 20 || len > 300) {
		document.getElementById("messageTextArea" + i).innerHTML = "**Enter the correct order text! (from 20 to 300 symbols)";
		return false;
	}
	if (srcText == text.val()) {
		document.getElementById("messageTextArea" + i).innerHTML = "**Texts are the same";
		return false;
	}
	document.getElementById("messageTextArea" + i).innerHTML = "";
	return true;
}

function validatePayOrder(balance) {
	var payment = $("#paymentValue").val();
	var requestId = $("#requestIdValue").val();

	if (balance < payment || payment < 1) {
		document.getElementById("messagePay" + requestId).innerHTML = "**Not enough money to pay for the order. Please top up your account.";
		return false;
	}
	document.getElementById("messageTextArea" + requestId).innerHTML = "";
	return true;
}

function validateFeedback() {
	var text = $("#textArea0");
	var radios = document.getElementsByName("grade");
	var len = text.val().trim().length;
	var formValid = false;

	if (len < 3 || len > 120) {
		document.getElementById("messageFeedback").innerHTML = "**Feedback text must not be empty (3-120 symbols)!";
		return false;
	}
	
    var i = 0;
    while (!formValid && i < radios.length) {
        if (radios[i].checked) formValid = true;
        i++;        
    }

    if (!formValid) {
		document.getElementById("messageFeedback").innerHTML = "**Select raiting please";
		return false;
	}
	
	document.getElementById("messageFeedback").innerHTML = "";
	return true;
}