$(document).ready(function() {
	$('#historyTable').DataTable({
		order: [],
		bFilter: false
	});
});

$(document).ready(function() {
	dataTable = $("#orderTable").DataTable(
		{
			"dom": 'lrtip',
			"ordering": false,
			"order": [],
			"columnDefs": [
				{
					"targets": [1],
					"visible": false
				}
			]
		});

	$('.status-dropdown').on('change', function(e) {
		var status = $(this).val();
		$('.status-dropdown').val(status)
		console.log(status)
		dataTable.column(1).search(status).draw();
	})
});