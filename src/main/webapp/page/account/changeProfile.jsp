<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="user" scope="session" value="${user}" />
<c:set var="login" scope="session" value="${user.login}" />
<c:set var="roleId" scope="session" value="${user.roleId}" />
<c:set var="balance" scope="session" value="${user.balance}" />
<c:set var="email" scope="session" value="${user.email}" />

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Change profile</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../css/bootstrap-formhelpers.min.css" rel="stylesheet" type="text/css">
<link href="../../css/style.css" rel="stylesheet" type="text/css">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Space+Grotesk&display=swap"
	rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.6.1.min.js"
	integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
	crossorigin="anonymous" defer></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
	crossorigin="anonymous" defer>
	
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap-formhelpers.min.js"
	defer>
	
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/reg-valid.js" defer>	
</script>
</head>

<body>
	<div class="page bg-light">
		<%@include file="/WEB-INF/jspf/navbar_index.jspf"%>
		<div class="container-fluid">
			<div class="container rounded bg-white mt-5 mb-5">
				<div class="row">
					<div class="col-md-3 border-right">
						<div
							class="d-flex flex-column align-items-center text-center p-3 py-5">
							<img class="rounded-circle mt-5" width="150px"
								src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
							<span class="font-weight-bold">${login}</span> 
							<span class="text-black-50">+380${user.phoneNumber}</span>
						</div>
					</div>
					<form class="col-md-5 border-right" method="post" action="${pageContext.request.contextPath}/controller" onsubmit ="return validatePayOrder(${requestId})">
						<input type="hidden" name="command" value="change_profile" />
						<div class="p-3 py-5">
							<div
								class="d-flex justify-content-between align-items-center mb-3">
								<h4 class="text-right">Change Profile</h4>
							</div>
							<div class="row mt-2">
								<div class="col-md-6">
									<label class="labels fw-semibold">Firstname<span style="color:red">*</span></label> <br /> 
									<input
										type="text" name="changeFistname" class="form-control text-secondary"
										id="validationRegFirstname"
										pattern="^[a-zA-Zа-яА-ЯЇїЄєІі0-9]{3,16}$"
										value="${user.firstname}" >
										<span id="blankMsg" style="color:red"> </span>
								</div>					
								<div class="col-md-6">
									<label class="labels fw-semibold">Lastname<span style="color:red">*</span></label> <br /> 
									<input
										type="text" name="changeLastname" class="form-control text-secondary"
										id="validationRegLastname"
										pattern="^[a-zA-Zа-яА-ЯЇїЄєІі0-9]{3,16}$"
										value="${user.lastname}" >
										<span id="blankMsg2" style="color:red"> </span>
								</div>
							</div>
							<div class="row mt-3">
								<div class="col-md-12">
									<label class="labels fw-semibold">Email<span style="color:red">*</span></label> <br /> 
									<input
										type="email" name="changeEmail" class="form-control text-secondary"
										id="validationRegEmail" value="${email}"
										pattern="(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})" >
										<span id="messageEmail" style="color:red"> </span>
								</div>
							</div>
							<div class="mt-5">
								<button class="btn btn-primary profile-button" type="submit">Apply changes</button>
								<a href="javascript:history.back()" class="btn btn-secondary profile-button" type="button">Back</a>
							</div>
						</div>
					</form>
					<div class="col-md-4">
						<div class="p-3 py-5">
							<div
								class="d-flex justify-content-between align-items-center experience">
								<span class="fw-semibold">Your balance</span> <span
									class="border px-3 p-1 add-experience font-monospace"><b>&nbsp;${balance} UAH</b></span>
							</div>
							<br>
							<div class="col-md-12">
								<button class="btn btn-primary profile-button" type="button"
									data-bs-toggle="modal" data-bs-target="#topUpBalanceForm">
									<i class="fa fa-plus"></i>&nbsp;Top up balance</button>
								<div class="modal fade" id="topUpBalanceForm"
									data-bs-keyboard="false" tabindex="-1"
									aria-labelledby="topUpBalanceFormLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header bg-light">
												<button type="button" class="btn-close"
													data-bs-dismiss="modal" aria-label="Close"></button>
											</div>
											<div class="modal-body bg-light">
												<%@include file="/WEB-INF/jspf/top_up_balance_form.jspf"%>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<%@include file="/WEB-INF/jspf/footer_form.jspf"%>
	</div>
</body>
</html>