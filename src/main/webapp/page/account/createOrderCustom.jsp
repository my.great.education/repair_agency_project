<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="user" scope="session" value="${user}" />
<c:set var="login" scope="session" value="${user.login}" />
<c:set var="roleId" scope="session" value="${user.roleId}" />
<c:set var="email" scope="session" value="${user.email}" />

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Orders</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../css/bootstrap-formhelpers.min.css" rel="stylesheet" type="text/css">
<link href="../../css/style.css" rel="stylesheet" type="text/css">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Space+Grotesk&display=swap"
	rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.6.1.min.js"
	integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
	crossorigin="anonymous" defer></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
	crossorigin="anonymous" defer>	
</script>
<!-- <script type="text/javascript" -->
<%-- 	src="${pageContext.request.contextPath}/js/bootstrap-formhelpers.min.js" --%>
<!-- 	defer>	 -->
<!-- </script> -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/reg-valid.js"
	defer>	
</script>
</head>

<body>
	<div class="page bg-light">
		<%@include file="/WEB-INF/jspf/navbar_index.jspf"%>
		<div class="container-fluid bg-white mb-2 mt-5" style="margin-left: 0px;">
			<div class="row">
				<ul class="nav nav-tabs">
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/page/account/profile.jsp">Profile</a></li>
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/controller?command=order_history">Orders</a></li>
					<li class="nav-item"><a class="nav-link active"
						aria-current="page">Create order</a></li>
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/controller?command=payment_history">Payment
							history</a></li>
				</ul>
				<div class="col-md-3"></div>
				<div class="col-md-9 text-center fs-4">Create order</div>
				<hr class="my-0" />
			</div>
			<div class="row">
				<div class="col-md-3 border-right">
					<div
						class="d-flex flex-column align-items-center text-center p-3 py-5">
						<img class="rounded-circle mt-5" width="150px"
							src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
						<span class="font-weight-bold">${login}</span> <span
							class="text-black-50">${email}</span>
					</div>
				</div>
				<div class="col-md-9 border-right bg-light">
					<div class="p-3">
						<ul class="nav nav-tabs">
							<li class="nav-item"><a class="nav-link"
								href="${pageContext.request.contextPath}/page/account/createOrder.jsp">Service</a></li>
							<li class="nav-item"><a class="nav-link active"
								aria-current="page">Custom</a></li>
						</ul>
					</div>
					<div class="col-md-9">
						<form action="${pageContext.request.contextPath}/controller"
							method="post" onsubmit="return validateCustomOrder(0);">
							<input type="hidden" name="command" value="create_order_custom">
							<label for="textArea" class="form-label fs-4">Create
								custom order</label> <br /> <br /> <span class="text-black-60">Please
								describe your order in as much detail as possible.</span> 
							<br />
							<textarea class="form-control" name="orderText" id="textArea0"
								rows="3" style="height: 150px" maxlength="300" placeholder="min 20 characters"></textarea>
							<div class="row">
								<div class="col-12 text-end">
									<span class="chars-current" id="textArea0a">0</span> 
									/ 
									<span class="chars-total" id="b">300</span>
								</div>
							</div>
							<div class="row">
								<span id="messageTextArea0" style="color: red"> </span> <span
									class="fst-italic">*The cost of the application will be
									determined by the manager. You will be contacted to confirm your order.</span>
							</div>
							<br /> 
							<button class="btn btn-primary profile-button" type="submit">Send
								an order</button>
							<button type="reset" class="btn btn-secondary" id="hulk" onclick="resetTextArea()">Reset</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@include file="/WEB-INF/jspf/footer_form.jspf"%>
	</div>
</body>
</html>