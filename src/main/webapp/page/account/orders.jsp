<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="user" scope="session" value="${user}" />
<c:set var="orders" scope="session" value="${ordersHistory}" />
<c:set var="login" scope="session" value="${user.login}" />
<c:set var="roleId" scope="session" value="${user.roleId}" />
<c:set var="balance" scope="session" value="${user.balance}" />
<c:set var="email" scope="session" value="${user.email}" />
<c:set var="status_canceled" value="canceled" />
<c:set var="status_completed" value="completed" />
<c:set var="status_pending_payment" value="pending payment" />
<c:set var="status_inProgress" value="in progress" />
<c:set var="status_awaiting" value="awaiting processing" />
<c:set var="master_0" value="Not assigned" />

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Orders</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../css/dataTables.bootstrap5.min.css" rel="stylesheet" type="text/css">
<link href="../../css/style.css" rel="stylesheet" type="text/css">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Space+Grotesk&display=swap"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,100,0,0" />

<script src="https://code.jquery.com/jquery-3.6.1.min.js"
	integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"
	defer></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
	crossorigin="anonymous" defer>	
</script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js" defer>	
</script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js" defer>	
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/reg-valid.js"
	defer>
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/table.js" defer>
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/order.js" defer>
</script>
</head>

<body>
	<div class="page bg-light">
		<%@include file="/WEB-INF/jspf/navbar_index.jspf"%>
		<div class="container-fluid bg-white mb-2 mt-5" style="margin-left: 0px;">
			<div class="row">
				<ul class="nav nav-tabs">
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/page/account/profile.jsp">Profile</a></li>
					<li class="nav-item"><a class="nav-link active" aria-current="page">Orders</a></li>
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/page/account/createOrderCustom.jsp">Create
							order</a></li>
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/controller?command=payment_history">Payment
							history</a></li>
				</ul>
				<div class="col-md-3"></div>
				<div class="col-md-9 text-center fs-4">Order page</div>
				<hr class="my-0" />
			</div>
			<div class="row">
				<div class="col-md-3 border-right">
					<div class="d-lg-flex flex-column align-items-center text-center p-3 py-5">
						<img class="rounded-circle mt-5" width="150px"
							src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
						<span class="font-weight-bold">${login}</span> <span class="text-black-50">${email}</span>
					</div>
				</div>
				<div class="col-md-9 border-right bg-light">
					<div class="p-3 py-5">
						<div class="row text-center">
							<font color="green"><b>${sessionScope.successUpdate}</b></font> <font color="red"><b>${sessionScope.errorUpdate}</b></font>
							${sessionScope.successUpdate = null} ${sessionScope.errorUpdate = null}
						</div>
						<table id="orderTable" class="table table-striped table-hover caption-top">
							<caption class="d-flex justify-content-between">
								<a type="button" class="btn btn-primary"
									href="${pageContext.request.contextPath}/controller?command=order_history">
									<i class="fa fa-thin fa-arrows-rotate"></i>&nbsp;Update order status </a>
								<div class="btn-group submitter-group float-right">
									<div class="input-group-prepend">
										<div class="input-group-text">Status</div>
									</div>
									<select class="form-control status-dropdown">
										<option value="">All</option>
										<option value="awaiting processing">Awaiting processing</option>
										<option value="pending payment">Pending payment</option>
										<option value="paid">Paid</option>
										<option value="in progress">In progress</option>
										<option value="completed">Completed</option>
										<option value="canceled">Canceled</option>
									</select>
								</div>
							</caption>
							<thead>
								<tr>
									<th class="th-sm" scope="col"></th>
									<th class="th-sm" scope="col"></th>
								</tr>
							</thead>
							<tbody>
								<c:if test="${ (orders ne null) or (not empty orders) }">
									<c:forEach var="order" items="${orders}">

										<c:set var="requestId" value="${order.requestId}"></c:set>
										<c:set var="payment" value="${order.payment.setScale(2, 5)}"></c:set>
										<c:set var="isPayment" value="${payment > 1}"></c:set>
										<c:set var="status" value="${order.status}"></c:set>
										<c:set var="master" value="${order.master}"></c:set>
										<c:set var="isThereReview" value="${order.thereReview}"></c:set>

										<tr class="accordion accordion-flush" id="accordion${requestId}">
											<td class="accordion-item">
												<h2 class="accordion-header" id="flush-heading${requestId}">
													<button class="accordion-button collapsed" type="button"
														data-bs-toggle="collapse" id="accordionBtn"
														data-bs-target="#flush-collapse${requestId}" aria-expanded="false"
														aria-controls="flush-collapse${requestId}">
														<div class="container">
															<div class="row">
																<div class="col text-start">
																	<div class="col">
																		<span class="link-primary fw-bold text-decoration-underline"> №
																			${requestId}</span>
																	</div>
																	<div class="col text-start">
																		<span class="s-date">${order.date.toLocalDate()}</span>
																	</div>
																</div>
																<div class="col">
																	<span class="s-date">Service: ${order.category}</span>
																</div>
																<div class="col text-center b-price">
																	<c:if test="${not isPayment}">
																		Not assigned
																	</c:if>
																	<c:if test="${isPayment}">
																		${payment}<i class="i-price"> UAH</i>
																	</c:if>
																</div>
																<div class="col text-end text-uppercase">
																	<c:choose>
																		<c:when test="${status eq status_completed}">
																			<span class="b-status-comleted">${status}</span>
																		</c:when>
																		<c:when test="${status eq status_canceled}">
																			<span class="b-status-cancel">${status}</span>
																		</c:when>
																		<c:when test="${status eq status_inProgress}">
																			<span class="b-status-inProgress">${status}</span>
																		</c:when>
																		<c:when test="${status eq status_pending_payment}">
																			<span class="b-status-panding">${status}</span>
																		</c:when>
																		<c:otherwise>
																			<span class="b-status-mid">${status}</span>
																		</c:otherwise>
																	</c:choose>
																</div>
															</div>
														</div>
													</button>
												</h2>
												<div id="flush-collapse${requestId}" class="accordion-collapse collapse"
													aria-labelledby="flush-heading${requestId}"
													data-bs-parent="#accordion${requestId}" style="">
													<div class="accordion-body">
														<div class="row">
															<div class="col-6">
																<dl class="b-lmo-full-info-list">
																	<dt>Descriprion</dt>
																	<dd>
																		<p class="text-break" id="description${requestId}"
																			style="display: block;">${order.description}</p>
																		<div id="editDescription${requestId}" style="display: none;">
																			<form action="${pageContext.request.contextPath}/controller"
																				method="post" onsubmit="return validateCustomOrder(${requestId}, '${order.description}');">
																				<input type="hidden" name="command" value="update_order_custom_text">
																				<input type="hidden" name="orderId" value="${requestId}">
																				<input type="hidden" name="srcText" value="${order.description}">
																				<textarea class="form-control" name="orderText"
																					id="textArea${requestId}" rows="3" style="height: 150px;"
																					maxlength="300">${order.description}</textarea>
																				<div class="row">
																					<div class="col-12 text-end">
																						<span class="chars-current" id="textArea${requestId}a">${order.description.length()}</span>
																						/ <span class="chars-total" id="b">300</span>
																					</div>
																					<span id="messageTextArea${requestId}" style="color: red"></span>
																					<div class="col-12 text-end">
																						<button type="submit" class="btn btn-primary">Confirm</button>
																						<button type="button" class="btn btn-secondary"
																							id="cancelBth${requestId}"
																							onclick="unShow(${requestId}, '${order.description}');">Cancel</button>
																					</div>
																				</div>
																			</form>
																		</div>
																	</dd>
																</dl>
															</div>
															<div class="col-6">
																<dl class="b-lmo-full-info-list">
																	<dt>Order date</dt>
																	<dd>${order.date.toLocalDate()}&nbsp;at&nbsp;${order.date.toLocalTime()}</dd>
																	<dt>Amount payable</dt>
																	<dd>
																		<c:if test="${not isPayment}">
																			<span style="color: red">Not assigned</span>
																		</c:if>
																		<c:if test="${isPayment}">
																			<span class="b-lmo-price">${payment} ₴</span>
																		</c:if>
																	</dd>
																	<dt>Master</dt>
																	<dd>
																		<c:choose>
																			<c:when test="${master eq master_0}">
																				<span style="color: red">${order.master}</span>
																			</c:when>
																			<c:otherwise>
																				${order.master} <a
																					href="${pageContext.request.contextPath}/controller?command=about_master&id=${order.masterId}"
																					class="link-primary">(info)</a>
																			</c:otherwise>
																		</c:choose>
																	</dd>
																	<dt>Phone number</dt>
																	<dd>+380${user.phoneNumber}</dd>
																</dl>
															</div>
														</div>
														<div class="row justify-content-end">
															<div class="col-md">
																<c:if test="${status eq status_completed && master ne master_0}">
																	<c:choose>
																		<c:when test="${isThereReview}">
																			<button type="button" class="btn btn-outline-success">Feedback done</button>
																		</c:when>
																		<c:otherwise>
																			<form method="get" action="${pageContext.request.contextPath}/controller">
																				<input type="hidden" name="command" value="about_master"> 
																				<input type="hidden" name="id" value="${order.masterId}">
																				<input type="hidden" name="requestId" value="${requestId}">
																				<input type="hidden" name="flagReview">
																				<button type="submit" class="btn btn-outline-primary">Feedback
																					on work</button>
																			</form>
																		</c:otherwise>
																	</c:choose>
																</c:if>
															</div>
															<div class="col-md-auto">
																<c:if test="${status eq status_awaiting}">
																	<button type="button" id="editBtn${requestId}"
																		class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown"
																		aria-expanded="false" style="width: 100px; display: block;">Edit</button>
																	<ul class="dropdown-menu">
																		<li><button class="dropdown-item" onclick="show(${requestId})">Edit
																				order</button></li>
																		<li><button class="dropdown-item" type="button"
																				data-bs-toggle="modal" data-bs-target="#cancelDialog${requestId}">Cancel
																				order</button></li>
																	</ul>
																	<div class="modal fade" id="cancelDialog${requestId}"
																		data-bs-keyboard="false" tabindex="-1"
																		aria-labelledby="cancelDialogLabel${requestId}" aria-hidden="true">
																		<div class="modal-dialog">
																			<div class="modal-content">
																				<div class="modal-header bg-light">
																					<button type="button" class="btn-close" data-bs-dismiss="modal"
																						aria-label="Close"></button>
																				</div>
																				<div class="modal-body bg-light text-center">
																					<form action="${pageContext.request.contextPath}/controller"
																						method="post">
																						<div class="mb-3 input-group" id="form-head">
																							<label class="form-label fs-5" for="form-head">Are you sure
																								you want to cancel order <b>№ ${requestId}</b>
																							</label>
																						</div>
																						<input type="hidden" name="command" value="update_order_status">
																						<input type="hidden" name="requestId" value="${requestId}">
																						<input type="hidden" name="state" value="6">
																						<button class="btn btn-outline-danger" type="submit">Cancel
																							order</button>
																						<button type="button" class="btn btn-secondary"
																							data-bs-dismiss="modal" aria-label="Close">Back</button>
																					</form>
																				</div>
																			</div>
																		</div>
																	</div>
																</c:if>
																<c:if test="${(status eq status_pending_payment) && isPayment}">
																	<button type="button" class="btn btn-warning" data-bs-toggle="modal"
																		data-bs-target="#payDialog${requestId}" style="width: 150px">Pay</button>
																	<div class="modal fade" id="payDialog${requestId}"
																		data-bs-keyboard="false" tabindex="-1"
																		aria-labelledby="payDialogLabel${requestId}" aria-hidden="true">
																		<div class="modal-dialog">
																			<div class="modal-content">
																				<div class="modal-header bg-light">
																					<button type="button" class="btn-close" data-bs-dismiss="modal"
																						aria-label="Close"></button>
																				</div>
																				<div class="modal-body bg-light">
																					<form action="${pageContext.request.contextPath}/controller"
																						method="post" onsubmit="return validatePayOrder(${balance})">
																						<div class="mb-3 input-group" id="form-head">
																							<label class="form-label fs-5" for="form-head">You are
																								going to pay for order <b>№ ${requestId}</b>
																							</label>
																							<dl class="b-lmo-full-info-list">
																								<dt>Order date</dt>
																								<dd>${order.date.toLocalDate()}at${order.date.toLocalTime()}</dd>
																								<dt>Amount payable</dt>
																								<dd>
																									<c:if test="${not isPayment}">
																										<span style="color: red">Not assigned</span>
																									</c:if>
																									<c:if test="${isPayment}">
																										<span class="b-lmo-price">${payment} ₴</span>
																									</c:if>
																								</dd>
																								<dt>Master</dt>
																								<dd>
																									<c:choose>
																										<c:when test="${master eq master_0}">
																											<span style="color: red">${order.master}</span>
																										</c:when>
																										<c:otherwise>
																											${order.master}
																										</c:otherwise>
																									</c:choose>
																								</dd>
																								<dt>Phone number</dt>
																								<dd>+380${user.phoneNumber}</dd>
																							</dl>
																						</div>
																						<input type="hidden" name="command" value="pay_order"> <input
																							type="hidden" name="requestId" value="${requestId}"
																							id="requestIdValue"> <input type="hidden" name="payment"
																							value="${payment}" id="paymentValue">
																						<div class="row">
																							<span id="messagePay${requestId}" style="color: red"></span>
																						</div>
																						<button class="btn btn-warning" type="submit">Pay order</button>
																						<button type="button" class="btn btn-secondary"
																							data-bs-dismiss="modal" aria-label="Close">Back</button>
																					</form>
																				</div>
																			</div>
																		</div>
																	</div>
																</c:if>
															</div>
														</div>
													</div>
												</div>
											</td>
											<td>${status}</td>
										</tr>
									</c:forEach>
								</c:if>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<%@include file="/WEB-INF/jspf/footer_form.jspf"%>
	</div>
</body>
</html>