<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="user" scope="session" value="${user}" />
<c:set var="login" scope="session" value="${user.login}" />
<c:set var="roleId" scope="session" value="${user.roleId}" />
<c:set var="ordersList" scope="session" value="${orders}" />
<c:set var="mastersList" scope="session" value="${masters}" />
<c:set var="status_canceled" value="canceled" />
<c:set var="status_completed" value="completed" />
<c:set var="status_pending_payment" value="pending payment" />
<c:set var="status_inProgress" value="in progress" />
<c:set var="status_awaiting" value="awaiting processing" />
<c:set var="status_paid" value="paid" />
<c:set var="master_0" value="Not assigned" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta name="description" content="" />
<meta name="author" content="" />
<title>Manager page</title>
<link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css"
	rel="stylesheet" />
<link href="https://nightly.datatables.net/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<link href="../../css/styles-manager.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.6.1.min.js"
	integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
	crossorigin="anonymous">
</script>
<script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js"
	crossorigin="anonymous">
</script>
<script src="https://kit.fontawesome.com/29d515ab5f.js" crossorigin="anonymous" defer></script>
<script src="../../js/manager/show.js" defer></script>
<script src="../../js/manager/validation.js" defer></script>

</head>
<body class="sb-nav-fixed">
	<%@include file="/WEB-INF/jspf/manager/navbar_index.jspf"%>
	<div id="layoutSidenav">
		<div id="layoutSidenav_nav">
			<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
				<div class="sb-sidenav-menu">
					<div class="nav">
						<div class="sb-sidenav-menu-heading text-center">Core</div>
						<a class="nav-link active" href="index.jsp">
							<div class="sb-nav-link-icon">
								<i class="fas fa-tachometer-alt"></i>
							</div> Orders
						</a> <a class="nav-link" href="users.jsp">
							<div class="sb-nav-link-icon"></div>
						</a>
					</div>
				</div>
			</nav>
		</div>
		<div id="layoutSidenav_content">
			<main>
				<div class="container-fluid px-4">
					<h1 class="mt-4">Dashboard</h1>
					<ol class="breadcrumb mb-4">
						<li class="breadcrumb-item active">Dashboard</li>
					</ol>
					<div class="text-start p-2 fs-5">
						<span style="color: red">${sessionScope.errorManager}</span>
						<c:if test="${sessionScope.errorManager ne null}">
							<b>${sessionScope.orderIdCurrent}</b>
							<b>${sessionScope.userIdCurrent}</b>
							${sessionScope.errorManager = null}
							${sessionScope.userIdCurrent = null}
						</c:if>
						<span style="color: green">${sessionScope.successManager}</span>
						<c:if test="${sessionScope.successManager ne null}">
							<b>${sessionScope.orderIdCurrent}</b>
							<b>${sessionScope.userIdCurrent}</b>
							${sessionScope.successManager = null}
							${sessionScope.orderIdCurrent = null}
							${sessionScope.userIdCurrent = null}
						</c:if>
					</div>
					<div class="container-fluid d-flex">
						<div class="col-lg-8 col-md-12 me-1 flex-lg-grow-1">
							<div class="card-header">
								<i class="fas fa-table me-1"></i>Orders
							</div>
							<div class="card-body" id="cardTableManager">
								<table class="text-start caption-top table" id="datatablesSimple">
									<caption>
										<a type="button" class="btn btn-primary"
											href="${pageContext.request.contextPath}/controller?command=order_full"> <i
											class="fas fa-arrows-rotate"></i>&nbsp;Update orders
										</a>
									</caption>
									<thead>
										<tr>
											<th>ID</th>
											<th>User name</th>
											<th>Price / Balance, ₴</th>
											<th>Status</th>
											<th>Master</th>
											<th>Date</th>
											<th>Details</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th><input type="text" name="search_platform" value="Search ID" class="search_init"></th>
											<th><input type="text" name="search_platform" value="Search username" class="search_init"></th>
											<th><input type="hidden" name="search_platform" value="Search Price" class="search_init"></th>
											<th><input type="text" name="search_platform" value="Search Status" class="search_init"></th>
											<th><input type="text" name="search_platform" value="Search Master" class="search_init"></th>
											<th><input type="hidden" name="search_platform" value="Search Date" class="search_init"></th>
											<th><input type="hidden" name="search_platform" value="Search Details" class="search_init"></th>
									</tfoot>
									<c:if test="${ (ordersList ne null) or (not empty ordersList) }">
										<tbody class="fs-6">
											<c:forEach var="order" items="${ordersList}">
												<tr>
													<td>${order.orderId}</td>
													<td>${order.userName}</td>
													<td><c:choose>
															<c:when test="${order.price.doubleValue() == 1}">
																<span style="color: red;">Not assigned&nbsp;/&nbsp;</span>
															</c:when>
															<c:otherwise>
																${order.price}&nbsp;/&nbsp;
															</c:otherwise>
														</c:choose> 
														<c:choose>
															<c:when test="${order.price > order.userBalance}">
																<span style="color: red;">${order.userBalance.setScale(2, 5)}</span>
															</c:when>
															<c:otherwise>
																<span style="color: green;">${order.userBalance.setScale(2, 5)}</span>
															</c:otherwise>
														</c:choose>
													</td>
													<td>									
													<c:choose>
														<c:when test="${order.status eq status_awaiting}">
															<i class="fas fa-exclamation" style="background-color:yellow;  width: 15px;">
															</i> ${order.status}
														</c:when>
														<c:when test="${order.status eq status_completed}">
															<i class="fas fa-check" style="background-color: green; width: 15px;">
															</i> ${order.status}
														</c:when>
														<c:when test="${order.status eq status_canceled}">
															<i class="fas fa-xmark" style="background-color: red; width: 15px;">
															</i> ${order.status}
														</c:when>
														<c:otherwise>
															${order.status}
														</c:otherwise>
													</c:choose>
													</td>
													<td><c:if test="${order.masterId eq 0}">
															<span style="color: red;">Not assigned</span>
														</c:if> 
														<c:if test="${order.masterId ne 0}">
															<span style="color: green;">${order.masterName} ${order.masterLastname}</span>
														</c:if>
													</td>
													<td>${order.date.toLocalDate()}</td>
													<td>
														<a type="button" class="link-primary" onclick="showOrderInfo(${order.orderId});">show</a></td>
												</tr>
											</c:forEach>
										</tbody>
									</c:if>
								</table>
							</div>
						</div>
						<!--------------------------------------------->
						<c:if test="${info eq null}">
							<c:forEach var="order" items="${ordersList}">
								<div class="col-lg-4 card mb-4 info-order" style="display: none;"
									id="orderInfo${order.orderId}">
									<div class="card-header d-flex justify-content-between">
										<div>
											<i class="fas fa-circle-info"></i> Info
										</div>
										<button type="button" class="btn-close" aria-label="Close"
											onclick="unShowOrderInfo(${order.orderId});"></button>
									</div>
									<div class="d-flex card-body" style="background-color: #f7f7f7;">
										<div class="col-5">
											<dl class="b-lmo-full-info-list">
												<dt>Order ID</dt>
												<dd>№ ${order.orderId}</dd>
												<hr class="my-0" />
												<dt>Order date</dt>
												<dd>
													${order.date.toLocalDate()}
													<br/>
													at ${order.date.toLocalTime()}
												</dd>
												<hr class="my-0" />
												<dt>Service</dt>
												<dd>${order.service}</dd>
												<hr class="my-0" />
												<dt>Amount</dt>
												<dd>${order.amount}</dd>
												<hr class="my-0" />
												<dt>Order status</dt>
												<dd>
													<c:choose>
														<c:when test="${(order.status eq status_completed) or (order.status eq status_canceled) or order.price.doubleValue() <= 1}">
															<div class="d-flex justify-content-between">
																${order.status}
															</div>
														</c:when>
														<c:otherwise>
															<c:choose>
																<c:when test="${order.status eq status_paid and order.masterId eq 0}">
																	<div class="d-flex justify-content-between">
																		${order.status}
																	</div>
																</c:when>
																<c:otherwise>
																	<div id="status${order.orderId}">
																		<div class="d-flex justify-content-between">
																			${order.status}
																			<a class="link-primary" type="button" onclick="showStatus(${order.orderId});">edit</a>
																		</div>
																	</div>
																	<div id="changeStatus${order.orderId}" style="display: none;">
																		<div class="row d-flex justify-content-between">
																			<form action="${pageContext.request.contextPath}/controller" method="post" onsubmit="return changeStatus(${order.orderId}, ${order.statusId});">
																				<input type="hidden" name="command" value="update_status"> 
																				<input type="hidden" name="orderId" value="${order.orderId}"> 
																				<input type="hidden" name="currentStatusId" value="${order.statusId}"> 
																				<select class="form-select" aria-label="Status select" style="width: 100%;" 
																					id="statusVal${order.orderId}" name="statusIdNew">	
																					<option selected value="${order.statusId}">${order.status}</option>
																					<option value="${order.statusNextId}">${order.statusNext}</option>
																					<option value="6" style="font-weight: bold;">cancel</option>
																				</select>
																				<div class="col-12 text-end">
																					<div class="col text-end">
																						<span id="changeStatusMsg${order.orderId}" style="color:red; width: 100%;"></span>
																					</div>
																					<div class="col text-end" style="margin-top: 5px;">
																						<button type="button" class="btn btn-secondary"
																							id="cancelPriceBth${order.orderId}" 
																							onclick="unShowChangeStatus(${order.orderId}, ${order.statusId});">Cancel</button>
																						<button type="submit" class="btn btn-primary">Confirm</button>
																					</div>
																				</div>
																			</form>
																		</div>
																	</div>
																</c:otherwise>
															</c:choose>
														</c:otherwise>
													</c:choose>
												</dd>
												<hr class="my-0" />
												<dt>Amount payable</dt>
												<dd>
													<c:choose>
														<c:when test="${order.price.doubleValue() <= 1 and (order.status ne status_canceled)}">
														<div class="row d-flex justify-content-between">
															<form action="${pageContext.request.contextPath}/controller" method="post" onsubmit="return validateChangePrice(${order.orderId})">
																<input type="hidden" name="command" value="change_price"> 
																<input type="hidden" name="orderId" value="${order.orderId}"> 
																<input id="sumValue${order.orderId}" type="text" name="sum" placeholder="Not assigned" style="width: 100%;">
																<div class="col-12 text-end">
																	<div class="col text-end">
																		<span id="changePriceMsg${order.orderId}" style="color:red; width: 100%;"></span>
																	</div>
																	<div class="col text-end" style="margin-top: 5px;">
																		<button type="submit" class="btn btn-primary">Confirm</button>
																	</div>
																</div>
															</form>
														</div>
														</c:when>
														<c:when test="${order.price.doubleValue() > 1 and ((order.status eq status_awaiting) or (order.status eq status_pending_payment))}">
															<div id="price${order.orderId}">
																<div class="d-flex justify-content-between">			
																	${order.price}
																	<a type="button" class="link-primary" onclick="showChangePrice(${order.orderId});">edit</a>
																</div>
															</div>
															<div id="changePrice${order.orderId}" style="display: none;">												
																<div class="row d-flex justify-content-between">
																	<form action="${pageContext.request.contextPath}/controller"
																		method="post" onsubmit="return validateChangePrice(${order.orderId}, ${order.price})">
																		<input type="hidden" name="command" value="change_price">
																		<input type="hidden" name="orderId" value="${order.orderId}"> 
																		<input id="sumValue${order.orderId}" type="text" name="sum" value="${order.price}" style="width: 100%;">
																		<div class="col-12 text-end">
																			<div class="col text-end">
																				<span id="changePriceMsg${order.orderId}" style="color: red"></span>
																			</div>
																			<div class="col text-end" style="margin-top: 5px; width: 100%;">
																				<button type="button" class="btn btn-secondary"
																					id="cancelPriceBth${order.orderId}" 
																					onclick="unShowChangePrice(${order.orderId}, ${order.price});">Cancel</button>
																				<button type="submit" class="btn btn-primary">Confirm</button>
																			</div>
																		</div>
																	</form>
																</div>
															</div>
														</c:when>	
														<c:when test="${order.status eq status_canceled}">
															<span style="color: red">Not assign</span>
														</c:when>													
														<c:otherwise>
															${order.price}
														</c:otherwise>
													</c:choose>
												</dd>
												<hr class="my-0" />
												<dt>Master</dt>
												<dd>
													<c:choose >
														<c:when test="${order.masterId eq 0 and (order.status ne status_canceled)}">
															<div class="row d-flex justify-content-between">
																<form action="${pageContext.request.contextPath}/controller" method="post" onsubmit="return changeMaster(${order.orderId}, ${order.masterId});">
																	<input type="hidden" name="command" value="change_master"> 
																	<input type="hidden" name="orderId" value="${order.orderId}"> 
																	<input type="hidden" name="currentMasterId" value="${order.masterId}"> 
																	<input type="hidden" name="changeFlag" value="1"> 
																	<select class="form-select" aria-label="Master select" style="width: 100%;" 
																		id="masterValue${order.orderId}" name="masterId">
																		<option selected value="0">Not assigned</option>
																		<c:forEach items="${mastersList}" var="master">
																			<option value="${master.id}">${master.firstname} ${master.lastname}</option>
																		</c:forEach>
																	</select>
																	<div class="col-12 text-end">
																		<div class="col text-end">
																			<span id="changeMasterMsg${order.orderId}" style="color:red; width: 100%;"></span>
																		</div>
																		<div class="col text-end" style="margin-top: 5px;">
																			<button type="submit" class="btn btn-primary">Confirm</button>
																		</div>
																	</div>
																</form>
															</div>
														</c:when>
														<c:when test="${order.masterId ne 0 and ((order.status eq status_awaiting) or (order.status eq status_pending_payment) or (order.status eq status_paid))}">
															<div id="master${order.orderId}">
																<div class="d-flex justify-content-between">
																	<span>${order.masterName} ${order.masterLastname}</span>
																	<a type="button" class="link-primary" onclick="showChangeMaster(${order.orderId});">edit</a>
																</div>
															</div>
															<div id="changeMaster${order.orderId}" style="display: none;">
																<c:set var="master_current_id" value="${order.masterId}"/>
																<c:set var="master_current_name" value="${order.masterName}"/>
																<c:set var="master_current_lname" value="${order.masterLastname}"/>
																<div class="row d-flex justify-content-between">
																	<form action="${pageContext.request.contextPath}/controller" method="post" onsubmit="return changeMaster(${order.orderId}, ${order.masterId});">
																		<input type="hidden" name="command" value="change_master"> 
																		<input type="hidden" name="orderId" value="${order.orderId}"> 
																		<input type="hidden" name="currentMasterId" value="${order.masterId}"> 
																		<input type="hidden" name="changeFlag" value="0">
																		<select class="form-select" aria-label="Master select" style="width: 100%;" 
																			id="masterValue${order.orderId}" name="masterId">	
																			<option value="0">Not assigned</option>
																			<c:forEach items="${mastersList}" var="master">
																				<c:if test="${master_current_id eq master.id}">
																					<option selected value="${master.id}">${master.firstname} ${master.lastname}</option>
																				</c:if>
																				<c:if test="${master_current_id ne master.id}">
																					<option value="${master.id}">${master.firstname} ${master.lastname}</option>
																				</c:if>															
																			</c:forEach>
																		</select>
																		<div class="col-12 text-end">
																			<div class="col text-end">
																				<span id="changeMasterMsg${order.orderId}" style="color:red; width: 100%;"></span>
																			</div>
																			<div class="col text-end" style="margin-top: 5px;">
																				<button type="button" class="btn btn-secondary"
																					id="cancelPriceBth${order.orderId}" 
																					onclick="unShowChangeMaster(${order.orderId}, ${master_current_id});">Cancel</button>
																				<button type="submit" class="btn btn-primary">Confirm</button>
																			</div>
																		</div>
																	</form>
																</div>
															</div>
														</c:when>
															<c:when test="${order.status eq status_canceled}">
																<span style="color: red">Not assign</span>
															</c:when>	
														<c:otherwise>
															<span>${order.masterName} ${order.masterLastname}</span>
														</c:otherwise>
													</c:choose>
												</dd>
												<hr class="my-0" />
											</dl>
										</div>
										<div class="col-7 d-flex justify-content-center ps-5">
											<dl class="container b-lmo-full-info-list">
												<dt>User name</dt>
												<dd>${order.userName}</dd>
												<hr class="my-0" />
												<dt>User balance, UAH</dt>	
												<dd>
													<div>
														<div class="d-flex justify-content-between">
															${order.userBalance.setScale(2, 5)}
															<a class="link-primary" type="button" onclick="showTopUp(${order.orderId});">top up</a>
														</div>
													</div>
													<div id="topUp${order.orderId}" style="display: none;">
														<div class="row d-flex justify-content-start">
															<form action="${pageContext.request.contextPath}/controller" method="post" onsubmit="return validateTopUp(${order.orderId});" style="">
																<input type="hidden" name="command" value="tubalance_manager">
																<input type="hidden" name="userId" value="${order.userId}"> 
																<input id="topUpVal${order.orderId}" type="text" name="sum" style="width: 100%;">
																<div class="col-12 text-end">
																	<div class="col text-end">
																		<span id="topUpMsg${order.orderId}" style="color: red"></span>
																	</div>
																	<div class="col text-end" style="width: 100%; padding-top: 5px;">
																		<button type="button" class="btn btn-secondary" id="cancelBth${order.orderId}" 
																			onclick="unShowTopUp(${order.orderId});">Cancel</button>
																		<button type="submit" class="btn btn-primary">Confirm</button>
																	</div>
																</div>
															</form>
														</div>
													</div>
												</dd>
												<hr class="my-0" />
												<dt>Phone number</dt>
												<dd>+380${order.userPhone}</dd>
												<hr class="my-0" />
												<dt>Order body</dt>
												<dd>
													<div>
														${order.description}
													</div>
												</dd>
												<hr class="my-0" />
											</dl>
										</div>
									</div>
								</div>
							</c:forEach>
						</c:if>
					</div>
					<!--------------------------------------------->
				</div>
			</main>
			<%@include file="/WEB-INF/jspf/manager/footer.jspf"%>
		</div>
	</div>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
	crossorigin="anonymous">
</script>
		
<script
	src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"
	crossorigin="anonymous">
</script>

<script
	src="https://cdn.datatables.net/fixedheader/3.2.3/js/dataTables.fixedHeader.min.js"
	crossorigin="anonymous">
</script>
<script src="../../js/manager/scripts.js"></script>
<script src="../../assets/demo/datatables-demo.js"></script>
</body>
</html>
