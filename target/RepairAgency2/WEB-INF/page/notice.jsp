<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>

<html>
<head>
<title>Notice page</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<style><%@include file="../../css/bootstrap.min.css"%></style>
<style><%@include file="../../css/bootstrap-formhelpers.min.css"%></style>
<style><%@include file="../../css/style.css"%></style>

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Space+Grotesk&display=swap"
	rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.6.1.min.js"
	integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
	crossorigin="anonymous" defer></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
	crossorigin="anonymous" defer></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/reg-valid.js" defer>	</script>

</head>

<body>
	<div class="page bg-light">
		<%@include file="/WEB-INF/jspf/navbar_index.jspf"%>
		<div class="container text-center fs-3">
			<br/> ${error}
			<br/> ${success}
			<br/> 	
			<c:choose>
				<c:when test="${user eq null}">
					<a class="btn btn-secondary fs-5" href="${pageContext.request.contextPath}/index.jsp">MAIN</a>
				</c:when>
				<c:when test="${user ne null}">
					<c:if test="${roleId eq 4}">
						<a class="btn btn-secondary fs-5" href="${pageContext.request.contextPath}/index.jsp">MAIN</a>
					</c:if>
					<c:if test="${roleId eq 2}">
						<a class="btn btn-secondary fs-5" href="${pageContext.request.contextPath}/page/manager/index.jsp">MAIN</a>
					</c:if>
				</c:when>
			</c:choose>	
		</div>
		<%@include file="/WEB-INF/jspf/footer_form.jspf"%>
	</div>
</body>
</html>