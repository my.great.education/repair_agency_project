<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>

<html>
<head>
<title>Hello page</title>
<meta name="viewport" content="width=device-width, initial-scale=1" charset="UTF-8">

<%-- <style><%@include file="css/bootstrap.min.css"%></style> --%>
<%-- <style><%@include file="css/bootstrap-formhelpers.min.css"%></style> --%>
<%-- <style><%@include file="css/style.css"%></style> --%>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap-formhelpers.min.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Space+Grotesk&display=swap"
	rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.6.1.min.js"
	integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
	crossorigin="anonymous" defer></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
	crossorigin="anonymous" defer>	
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap-formhelpers.min.js"
	defer>	
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/reg-valid.js" defer>	
</script>

</head>
<body>
	<div class="page bg-light">
		<%@include file="/WEB-INF/jspf/navbar_index.jspf"%>
		<div class="container mb-5 mt-5">
			<div class="row row-cols-1 row-cols-md-3 g-4">
				<div class="col">
					<div class="card h-100">
						<div class="card-b">
							<div class="back">
								<div class="back-content center">
									<ul class="list-group fs-6 text-center">
										<li class="list-group-item">Replacing the heating element
											- 260 UAH</li>
										<li class="list-group-item">Repair of the board - 450 UAH</li>
										<li class="list-group-item">Installing a new drum - 800
											UAH</li>
										<li class="list-group-item">Replacing the bearing block -
											320 UAH</li>
									</ul>
								</div>
							</div>
							<img src="${pageContext.request.contextPath}/img/w.jpeg"
								class="card-img-top img-fluid" width="120" height="150">
							<div class="card-body">
								<h5 class="card-title">Repair of washing machines</h5>
								<p class="card-text">The cost of services from 260 UAH.</p>
							</div>
							<div class="card-footer">
								<a class="btn btn-primary w-100">Show price</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card h-100">
						<div class="card-b">
							<div class="back">
								<div class="back-content center">
									<ul class="list-group">
										<li class="list-group-item">Элемент</li>
										<li class="list-group-item">Второй элемент</li>
										<li class="list-group-item">Третий элемент</li>
										<li class="list-group-item">Четвертый элемент</li>
										<li class="list-group-item">И пятый</li>
									</ul>
								</div>
							</div>
							<img src="${pageContext.request.contextPath}/img/r.jpeg"
								class="card-img-top img-fluid" width="120" height="150">
							<div class="card-body">
								<h5 class="card-title">Refrigerator repair</h5>
								<p class="card-text">The cost of services from 180 UAH.</p>
							</div>
							<div class="card-footer">
								<a class="btn btn-primary w-100">Show price</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card h-100">
						<div class="card-b">
							<div class="back">
								<div class="back-content center">
									<ul class="list-group">
										<li class="list-group-item">Элемент</li>
										<li class="list-group-item">Второй элемент</li>
										<li class="list-group-item">Третий элемент</li>
										<li class="list-group-item">Четвертый элемент</li>
										<li class="list-group-item">И пятый</li>
									</ul>
								</div>
							</div>
							<img src="${pageContext.request.contextPath}/img/gs.jpeg"
								class="card-img-top img-fluid" width="120" height="150">
							<div class="card-body">
								<h5 class="card-title">Gas stove repair</h5>
								<p class="card-text">The cost of services from 160 UAH.</p>
							</div>
							<div class="card-footer">
								<a class="btn btn-primary w-100">Show price</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card h-100">
						<div class="card-b">
							<div class="back">
								<div class="back-content center">
									<ul class="list-group">
										<li class="list-group-item">Элемент</li>
										<li class="list-group-item">Второй элемент</li>
										<li class="list-group-item">Третий элемент</li>
										<li class="list-group-item">Четвертый элемент</li>
										<li class="list-group-item">И пятый</li>
									</ul>
								</div>
							</div>
							<img src="${pageContext.request.contextPath}/img/ep.jpeg"
								class="card-img-top img-fluid" width="115" height="150">
							<div class="card-body">
								<h5 class="card-title">Repair of electric stoves</h5>
								<p class="card-text">The cost of services from 215 UAH.</p>
							</div>
							<div class="card-footer">
								<a class="btn btn-primary w-100">Show price</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card h-100">
						<img src="${pageContext.request.contextPath}/img/key.png"
							class="card-img-top img-fluid" width="115" height="150">
						<div class="card-body">
							<h5 class="card-title">Custom</h5>
							<p class="card-text">The cost is determined by the manager.</p>
						</div>
						<div class="card-footer">
						<c:choose>
							<c:when test="${user eq null}">
								<button type="button" class="btn btn-warning w-100"
									data-bs-toggle="modal" data-bs-target="#customOrderForm">Make
									an order</button>
								<div class="modal fade" id="customOrderForm"
									data-bs-keyboard="false" tabindex="-1"
									aria-labelledby="#customOrderFormLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header bg-light">
												<button type="button" class="btn-close"
													data-bs-dismiss="modal" aria-label="Close"></button>
											</div>
											<div class="modal-body bg-light">
												<%@include file="/WEB-INF/jspf/login_form.jspf"%>
											</div>
										</div>
									</div>
								</div>
							</c:when>
							<c:when test="${user ne null}">
								<c:if test="${roleId eq 4}">
									<a type="button" class="btn btn-warning w-100"
									href="${pageContext.request.contextPath}/page/account/createOrderCustom.jsp">Make an order</a>
								</c:if>
							</c:when>
							</c:choose>	
						</div>								
					</div>
				</div>
			</div>
		</div>
		<%@include file="/WEB-INF/jspf/footer_form.jspf"%>
	</div>
</body>
</html>