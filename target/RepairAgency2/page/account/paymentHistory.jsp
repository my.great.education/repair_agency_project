<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="user" scope="session" value="${user}" />
<c:set var="login" scope="session" value="${user.login}" />
<c:set var="email" scope="session" value="${user.email}" />
<c:set var="history" scope="session" value="${balanceHistory}" />

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Payment history</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css"> -->
<!-- <link href="../../css/dataTables.bootstrap5.min.css" rel="stylesheet" type="text/css"> -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.2/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.bootstrap5.min.css">
<link href="../../css/style.css" rel="stylesheet" type="text/css">


<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Space+Grotesk&display=swap"
	rel="stylesheet">
	
<!-- JS -->
<script  type="text/javascript" src="https://code.jquery.com/jquery-3.6.1.min.js"
	integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
	crossorigin="anonymous" defer></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js" defer></script>	
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"
	defer>	
</script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js" defer></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.bootstrap5.min.js" defer></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js" defer></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js" defer></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.colVis.min.js" defer></script>
<script type="text/javascript"
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
	crossorigin="anonymous" defer>	
</script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/table.js"
	defer>	
</script>
</head>

<body>
	<div class="page bg-light">
		<%@include file="/WEB-INF/jspf/navbar_index.jspf"%>
		<div class="container-fluid bg-white mt-1 mb-2 mt-5" style="margin-left: 0px;">
			<div class="row">
				<ul class="nav nav-tabs">
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/page/account/profile.jsp">Profile</a></li>
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/controller?command=order_history">Orders</a></li>
					<li class="nav-item"><a class="nav-link "
						href="${pageContext.request.contextPath}/page/account/createOrderCustom.jsp">Create
							order</a></li>
					<li class="nav-item"><a class="nav-link active"
						aria-current="page">Payment history</a></li>
				</ul>
				<div class="col-md-3"></div>
				<div class="col-md-9 text-center fs-4">Payment history</div>
				<hr class="my-0" />
			</div>
			<div class="row">
				<div class="col-md-3 border-right">
					<div
						class="d-flex flex-column align-items-center text-center p-3 py-5">
						<img class="rounded-circle mt-5" width="150px"
							src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
						<span class="font-weight-bold">${login}</span> 
						<span class="text-black-50">${email}</span>
					</div>
				</div>
				<div class="col-md-9 border-right bg-light">
					<div class="p-3 py-5">
						<table id="historyTable" class="table table-striped table-hover caption-top datatable-custom">
							<caption>
							<hr class="my-0 mb-2" />
								<a type="button" class="btn btn-primary"
									href="${pageContext.request.contextPath}/controller?command=payment_history">
									<i class="fa fa-thin fa-arrows-rotate"></i>&nbsp;Update payments</a>
							</caption>
							<thead>
								<tr>
									<th class="th-sm" scope="col">Payment id</th>
									<th class="th-sm" scope="col">Payment, UAH</th>
									<th class="th-sm" scope="col">Date</th>
									<th class="th-sm" scope="col">Time</th>
								</tr>
							</thead>
							<tbody>
								<c:if test="${ (balanceHistory ne null) or (not empty balanceHistory) }">
									<c:forEach var="el" items="${history}">
									<c:set var="payment" value="${el.payment.setScale(2, 5)}"></c:set>
										<tr>
											<th scope="row">${el.id}</th>
											<c:if test="${payment.signum() > 0}"><td><span style="color:green">${el.payment.setScale(2, 5)} ₴</span></td></c:if>
											<c:if test="${payment.signum() < 0}"><td><span style="color:red">${el.payment.setScale(2, 5)} ₴</span></td></c:if>	
											<c:if test="${payment.signum() == 0}"><td><span style="color:yellow;">${el.payment.setScale(2, 5)} ₴</span></td></c:if>							
											<td>${el.time.toLocalDate()}</td>
											<td>${el.time.toLocalTime()}</td>
										</tr>
									</c:forEach>
								</c:if>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<%@include file="/WEB-INF/jspf/footer_form.jspf"%>
	</div>
</body>
</html>