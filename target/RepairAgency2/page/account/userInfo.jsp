<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="user" scope="session" value="${user}" />
<c:set var="feedbackList" scope="session" value="${feedbackList}" />
<c:set var="master" scope="session" value="${masterAccount}" />
<c:set var="flagReview" scope="session" value="${flagReview}" />
<c:set var="login" scope="session" value="${user.login}" />
<c:set var="roleId" scope="session" value="${user.roleId}" />

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Info</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../css/dataTables.bootstrap5.min.css" rel="stylesheet" type="text/css">
<link href="../../css/style.css" rel="stylesheet" type="text/css">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Space+Grotesk&display=swap"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,100,0,0" />

<!-- <script src="https://kit.fontawesome.com/29d515ab5f.js" crossorigin="anonymous"></script> -->
<script src="https://code.jquery.com/jquery-3.6.1.min.js"
	integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"
	defer></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
	crossorigin="anonymous" defer>
	
</script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js" defer>
	
</script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js" defer>
	
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/reg-valid.js"
	defer>
	
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/table.js" defer>
	
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/order.js" defer>
	
</script>
</head>

<body>
	<div class="page bg-light">
		<%@include file="/WEB-INF/jspf/navbar_index.jspf"%>
		<div class="container-fluid mt-5 mb-2" style="margin-left: 0px;">
			<div class="row bg-white">
				<ul class="nav nav-tabs">
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/page/account/profile.jsp">Profile</a></li>
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/controller?command=order_history">Orders</a></li>
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/page/account/createOrder.jsp">Create
							order</a></li>
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/controller?command=payment_history">Payment
							history</a></li>
					<li class="nav-item"><a class="nav-link active" aria-current="page">About
							the master</a></li>
				</ul>
			</div>
			<div class="row">
				<div class="col-md-4">
					<section>
						<div class="container my-5 py-5">
							<div class="row d-flex justify-content-center">
								<div class="col-md-12 col-lg-10">
									<div class="card text-dark bg-white">
										<h4 class="mb-0 text-center">Master info</h4>
										<hr class="my-0" />
										<div class="row">
											<div class="d-flex flex-column align-items-center text-center p-3 py-5">
												<img class="rounded-circle shadow-1-strong me-3" width="200px"
													src="https://drive.google.com/uc?export=view&id=1D1kI6YY7Mhdorr2WohxU169JpO9jSp0R">
												<span class="font-weight-bold fs-5">${master.lastname}
													${master.firstname}</span> <span class="text-black-50 fs-6">${master.email}</span> <span
													class="text-black-50 fs-6">+380${master.phoneNumber}</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				<div class="col-md-8">
					<section>
						<div class="container my-5 py-5">
							<div class="row d-flex justify-content-center">
								<div class="col-md-12 col-lg-10">
								<div class="text-center fs-6">
									<span id = "messageFeedback" style="color:red"> </span>
									<span style="color:red">${sessionScope.feedbackError}</span>
									<c:if test="${sessionScope.feedbackError ne null}">
										${sessionScope.feedbackError = null}
									</c:if>
								</div>
									<c:if test="${ (feedbackList eq null) || (empty feedbackList)}">
										<div class="card text-dark">
											<h4 class="mb-0 text-center">Reviews</h4>
											<hr class="my-0" />
											<c:if test="${(flagReview ne null) && (flagReview == 1)}">
												<%@include file="/WEB-INF/jspf/feedback_form.jspf"%>
											</c:if>
											<br />
											<div class="card-body p-4 text-center">no reviews</div>
										</div>
									</c:if>
									<c:if test="${ (feedbackList ne null) || (not empty feedbackList)}">
										<div class="card text-dark">
											<div id="card_feedback">
												<h4 class="mb-0 text-center">Reviews</h4>
												<hr class="my-0" />
												<c:if test="${(flagReview ne null) && (flagReview == 1)}">
													<%@include file="/WEB-INF/jspf/feedback_form.jspf" %>
												</c:if>
												<br />
												<c:forEach var="feedback" items="${feedbackList}">
													<c:set var="firstnameUser" scope="session" value="${feedback.firstnameUser}"></c:set>
													<c:set var="reviewDate" scope="session" value="${feedback.reviewDate}"></c:set>
													<c:set var="review" scope="session" value="${feedback.review}"></c:set>
													<c:set var="grade" scope="session" value="${feedback.grade}"></c:set>
													
													<div class="card-body p-4">
														<div class="d-flex flex-start">
															<img class="rounded-circle shadow-1-strong me-3"
																src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg"
																alt="avatar" width="60" height="70" />
															<div>
																<div class="d-flex">
																	<h6 class="fw-bold mb-1 me-2">${feedback.firstnameUser}</h6>
																	<c:choose>
																		<c:when test="${grade eq true}">
																			<i class="fa-solid fa-thumbs-up" id="thumb-up"></i>
																		</c:when>
																		<c:otherwise>
																			<i class="fa-solid fa-thumbs-down"></i>
																		</c:otherwise>
																	</c:choose>
																</div>
																<div class="d-flex align-items-center mb-3">
																	<p class="mb-0 small">${feedback.reviewDate.toLocalDate()}</p>
																</div>
																<p class="mb-0">${feedback.review}</p>
																<p class="mb-0"></p>
															</div>
														</div>
													</div>

													<hr class="my-0" />
												</c:forEach>
											</div>
										</div>
									</c:if>

								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
		<%@include file="/WEB-INF/jspf/footer_form.jspf"%>
	</div>
</body>
</html>