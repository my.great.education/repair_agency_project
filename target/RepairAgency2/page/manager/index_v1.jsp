<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="user" scope="session" value="${user}" />
<c:set var="login" scope="session" value="${user.login}" />
<c:set var="roleId" scope="session" value="${user.roleId}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta name="description" content="" />
<meta name="author" content="" />
<title>Manager page</title>
<link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css"
	rel="stylesheet" />
<link href="../../css/styles-manager.css" rel="stylesheet" />
<script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js"
	crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/29d515ab5f.js" crossorigin="anonymous" defer></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/table.js" defer></script>

</head>
<body class="sb-nav-fixed">
	<%@include file="/WEB-INF/jspf/manager/navbar_index.jspf"%>
	<div id="layoutSidenav">
		<div id="layoutSidenav_nav">
			<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
				<div class="sb-sidenav-menu">
					<div class="nav">
						<div class="sb-sidenav-menu-heading text-center">Core</div>
						<a class="nav-link active" href="index.jsp">
							<div class="sb-nav-link-icon">
								<i class="fas fa-tachometer-alt"></i>
							</div> Orders
						</a> <a class="nav-link" href="users.jsp">
							<div class="sb-nav-link-icon">
								<i class="fa-solid fa-user"></i>
							</div> Users
						</a>
						<div class="sb-sidenav-menu-heading text-center">Interface</div>
						<a class="nav-link collapsed" href="#" data-bs-toggle="collapse"
							data-bs-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
							<div class="sb-nav-link-icon">
								<i class="fas fa-book-open"></i>
							</div> Pages
							<div class="sb-sidenav-collapse-arrow">
								<i class="fas fa-angle-down"></i>
							</div>
						</a>
						<div class="collapse" id="collapsePages" aria-labelledby="headingTwo"
							data-bs-parent="#sidenavAccordion">
							<nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
								<a class="nav-link collapsed" href="#" data-bs-toggle="collapse"
									data-bs-target="#pagesCollapseAuth" aria-expanded="false"
									aria-controls="pagesCollapseAuth"> Authentication
									<div class="sb-sidenav-collapse-arrow">
										<i class="fas fa-angle-down"></i>
									</div>
								</a>
								<div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne"
									data-bs-parent="#sidenavAccordionPages">
									<nav class="sb-sidenav-menu-nested nav">
										<a class="nav-link" href="login.html">Login</a> <a class="nav-link"
											href="register.html">Register</a> <a class="nav-link" href="password.html">Forgot
											Password</a>
									</nav>
								</div>
								<a class="nav-link collapsed" href="#" data-bs-toggle="collapse"
									data-bs-target="#pagesCollapseError" aria-expanded="false"
									aria-controls="pagesCollapseError"> Error
									<div class="sb-sidenav-collapse-arrow">
										<i class="fas fa-angle-down"></i>
									</div>
								</a>
								<div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne"
									data-bs-parent="#sidenavAccordionPages">
									<nav class="sb-sidenav-menu-nested nav">
										<a class="nav-link" href="401.html">401 Page</a> <a class="nav-link"
											href="404.html">404 Page</a> <a class="nav-link" href="500.html">500 Page</a>
									</nav>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</nav>
		</div>
		<div id="layoutSidenav_content">
			<main>
				<div class="container-fluid px-4">
					<h1 class="mt-4">Dashboard</h1>
					<ol class="breadcrumb mb-4">
						<li class="breadcrumb-item active">Dashboard</li>
					</ol>
					<div class="card mb-4">
						<div class="card-header">
							<i class="fas fa-table me-1"></i>Orders
						</div>
						<div class="card-body">
							<table class="text-start" id="datatablesSimple">
								<thead>
									<tr>
										<th>ID</th>
										<th>User name</th>
										<th>Price / Balance, ₴</th>
										<th>Status</th>
										<th>Master</th>
										<th>Service</th>
										<th>Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody class="fs-6">
									<tr>
										<td>7</td>
										<td>user1</td>
										<td style="width: 200 px">
											<div class="d-flex">
												<div class="col-8">
													350 / <span style="color: red;">80</span>
												</div>
												<div class="col-4 text-end">
												<button type="button" class="btn btn-primary"><i class="fas fa-circle-plus"></i></button>
												</div>
											</div>
										</td>
										<td style="width: 270px">
<!-- 											<i class="fas fa-exclamation"></i> Awaiting processing -->
										<div class="d-flex">
											<div class="col order-1">
												<select class="form-control status-dropdown fs-6" style="width: auto;" name="newStatusId">
													<option value="1">Awaiting processing</option>
													<option value="2">Pending payment</option>
													<option value="3">Paid</option>
													<option value="6">Canceled</option>
												</select>
											</div>
											<div class="col text-end order-2 ps-2">
												<button type="button" id="" class="btn btn-primary">Apply</button>
											</div>
										</div>
										</td>
										<td>Anton Antonov</td>
										<td>Custom</td>
										<td>2022/11/01</td>
										<td>-</td>
									</tr>
									<tr>
										<td>8</td>
										<td>user2</td>
										<td class="d-flex">
											<div class="col-8">
												750 / <span style="color: green;">1200</span>
											</div>
											<div class="col-4 text-end">
												<i class="fas fa-circle-plus"></i>
											</div>
										</td>
										<td>Paid</td>
										<td>Anton Antonov</td>
										<td>Custom</td>
										<td>2022/11/01</td>
										<td>-</td>
									</tr>
									<tr>
										<td>12</td>
										<td>user3</td>
										<td class="d-flex">
											<div class="col-8">
												180 / <span style="color: green;">2000</span>
											</div>
											<div class="col-4 text-end">
												<i class="fas fa-circle-plus"></i>
											</div>
										</td>
										<td>Paid</td>
										<td>Anton Antonov</td>
										<td>Custom</td>
										<td>2022/11/01</td>
										<td>-</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</main>
			<%@include file="/WEB-INF/jspf/manager/footer.jspf"%>
		</div>
	</div>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
		crossorigin="anonymous"></script>
	<script src="../../js/manager/scripts.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"
		crossorigin="anonymous"></script>
	<script src="../../assets/demo/chart-area-demo.js"></script>
	<script src="../../assets/demo/chart-bar-demo.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest"
		crossorigin="anonymous"></script>
	<script src="../../js/manager/datatables-simple-demo.js"></script>
</body>
</html>
